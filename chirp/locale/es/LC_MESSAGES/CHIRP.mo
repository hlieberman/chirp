��    �     <  	  \      �   
  �   -   �!  9   �!     �!  ;   
"  +   F"     r"     y"    �"  �   �#  �  <$  �   �%  �   l&    P'  �   g(    \)  �   b*  �   \+  �   T,  �   P-    @.  o  L/  q  �0  �   .2  �   
3  p  �3  e  Q5  �   �6  �   �7  �   |8  �   S9  �   C:  �   0;  �   <  �   =  	  >  �   ?  �   �?  �   �@  �   hA  �   @B  �   !C  #  �C    #E  M  /F  �   }G  �   8H  �   �H  �   �I    �J    �K  �   �L  �   uM  �   UN  m   �N  �   DO  �   #P  �   Q  �   �Q  �   �R    �S  �   �T  �   �U  ^   �V     �V     �V     �V  	   �V     W     W      W     6W     HW     ZW     cW     iW     oW     sW     {W     �W  ]   �W  J   �W     BX     TX     dX     xX     �X     �X  (   �X     �X    �X  ,   �Y     !Z     )Z     <Z     MZ  
   SZ  %   ^Z     �Z     �Z     �Z  k   �Z     [     &[     +[  
   3[     >[  	   J[     T[     X[     ][     k[  	   w[     �[     �[     �[     �[  A   �[     �[     �[     	\     \     )\     2\     ;\      V\     w\     �\     �\     �\     �\  C   �\     ]     ]     <]     W]     n]     v]     �]     �]     �]     �]     �]     �]     ^     +^     ?^     _^     m^     ~^     �^  u   �^     _      _     7_     @_     X_     ^_  1   e_     �_  	   �_     �_     �_  �   �_  �   �`  �   [a    b  �   .c  �   �c  �   �d  �   'e  �   �e  �   �f  8  ng  �   �h  �   Li  �   j  �   �j  �   Ck  �   �k  �   �l  .   0m  	   _m     im     nm     m     �m     �m     �m  
   �m     �m     �m  7   �m      n     n     n     2n     8n     =n     In     Zn     pn  '   �n     �n     �n     �n  "   �n      o     o      o     %o     .o     @o     Lo     Xo  4   eo     �o     �o     �o  y  �o     Tq     eq     �q  	   �q     �q     �q  %   �q     �q     r     r      r     &r     -r     ;r     Vr  	   sr     }r  
   �r     �r     �r     �r     �r  
   �r     �r      s     s     s     !s      4s     Us     ls     qs     }s     �s     �s     �s     �s     �s     �s     �s     	t      t     <t  �   Pt     �t     �t  3   �t  *   u  (   Du  &   mu  ?   �u  �  �u  (  �w    �x     �y  $   �y     z     z  !   z     4z     Bz  
   Kz     Vz     _z     lz     tz     zz     �z  �   �z  ;   P{  t   �{     |     |     &|     4|     K|  W   W|     �|  �   �|     z}     �}     �}     �}     �}  	   �}     �}  	   �}  	   �}     ~     ~     %~     2~     ?~     Q~     Y~     b~     r~     �~  .   �~     �~     �~  3     5   ?     u     �     �     �     �     �     �  �  �  �   ��  �   ��  ^  !�     ��  �   ��  G   G�  #   ��  =   ��  <  �  �   .�     �     0�  &  O�  �   v�  ,   e�     ��  	   ��     ��     ��     ��  L   ʋ  ,   �     D�     c�     ��  �   ��  "   :�     ]�     }�  /   ��     ��     ύ     ߍ     �     �     �     *�     H�     d�  '   �     ��     Ŏ     ̎     ӎ     ؎     �     �     ��  7   ��  !   5�     W�     \�  
   b�     m�     v�     ~�  {  ��  A  	�  ,   K�  R   x�     ˒  V   �  2   9�     l�     s�  g  ��  �   �  �  ��  �   ��  0  �  D  ��    ��  %  �  "  2�    U�    [�  �   a�  *  [�  �  ��  �  2�  �   ̥  �   Ȧ  �  ��  �  5�    ʪ  �   ӫ  �   ά    ��  �   ��  
  ��  �   ��    ��  %  ²  �   �  �   ʴ  �   ��  �   ��     ��  �   ��  ^  n�  &  ͺ  �  ��  �   ��  �   w�  �   D�  �   :�  V  �  M  n�    ��    ��  �   ��     ��  -  �  (  <�  �   e�    9�  &  V�  C  }�  
  ��     ��  {   ��  	   I�     S�     c�     h�     {�  
   ��     ��     ��     ��     ��     ��     ��     ��  	   �     �     /�  ]   6�  N   ��     ��     ��     �     '�     6�     J�  /   Y�     ��  E  ��  2   ��     	�     �     '�     8�     ?�  &   N�  
   u�     ��  
   ��  p   ��     �     $�     +�     1�     >�     S�     d�     k�     p�     �  
   ��     ��     ��     ��     ��  V   ��     )�     G�     W�     g�     }�  	   ��     ��  '   ��  	   ��     ��     ��     �     ,�  R   I�     ��      ��     ��      ��  
   �     �     %�     C�  )   `�     ��     ��     ��  !   ��     ��  )   �     9�     H�     Z�     `�  �   i�  "   ��     �     ;�     K�     d�     m�  :   u�     ��     ��  	   ��     ��  �   ��  �   ��  �   ��    z�  �   ��  �   G�  �   �  �   ��  �   ��  �   V�  N  �  �   i�  �   �  �   ��  �   �  �   .�  �   ��  �   w�  9   �  
   U�     `�     e�     x�     ��     ��     ��     ��     ��     ��  M   ��      �     )�     C�     _�     g�     t�     ��     ��     ��  *   ��     ��     �     (�  +   ?�     k�     �     ��     ��     ��     ��     ��     ��  :   ��     �     .�      L�  �  m�     ��  %   
�  %   0�     V�     _�     h�  2   ��  )   ��     ��     ��     ��     ��     ��     �      (�     I�     U�     b�     p�     ��     ��  )   ��     ��      �     �     �     +�     ?�  0   R�     ��     ��     ��     ��     ��     ��     ��     �  +   *�     V�     j�     x�  !   ��     ��  �   ��  
   b�     m�  ;   s�  0   ��  2   ��  .   �  M   B�    ��  \  ��  B  �     F�  ,   W�     ��     ��  .   ��     ��     ��     ��     ��     �     �     �  #   #�     G�  �   ]�  R   (�  �   {�     ��     �     -�     B�     a�  c   q�     ��  �   ��     ��  ,   ��     ��     �     �     +�     ;�     M�     _�     k�     �     ��     ��     ��     ��     ��     ��  .   �     4�  1   O�  !   ��  &   ��  N   ��  P   �     j�     �  	   ��     ��     ��     ��     ��  Q  ��  �   # �    �  �    W �   d N   P .   � M   � h     �	 *   �
 ,   �
 h  �
   K ;   c    �    �    �    �    � V   � =   K 0   � !   �    � �   � &   � )   �     D       d    y    �    �    �    �    �    �      0   6    g    �    �    �    �    �    � 
   � >   �         1    6 
   <    G 
   U    `    L       \   �   0     �   U      h   �   �                   �   =  �   .  4      �         �     8           D   r    �   s   T   }   �           z       �   �   B       ^   �   ]  �   <   h          w           4         t      %       [   B      �   g       -      i    u  /  |  �       }              �   K      �      x   l  �      �       �   �           *   a   U   y  &   ;  )   �   �       n  �   M   '     >   7   j  p          K       �                      �   �   6  �       �           c   T  �          k      C   N  �   �       H  �   G   �   �       v  (     O  �   g      �   ~      z  �             $       C  '      *      �   �   7  �   3  �    I      ,   �             �   Y   Z   f   y   	       �           �   �   �      r   R     {   -  d  <  /         l   o   �   8  =   s  �   m               �   
  W         �   6   H      ^  x      5  m  :   w  �       �   >  F   ~              
           �   Q       W          �   �   �   �   ?          �          �   �   :  �   �  e  �         �      3   p   $          "   `  �       J  .   �   Y      v   �      P               �   �   �       �          P     �   \  0               9      �   O   J        #  +  �     2       _           b   �   A    �      	  �   �       1   �   �   �   �     u   �  �         �   A       M  "     �   R       �       t       !   �   @   �   �     D      S   %  �       �   X       n   �       @  L  �   �   �      I   �       �   �   �   �   �       [  �   S     2  �                     f            E  �   �   b  �           1      o  q  �      !  _  j   |                  c      a  �          �             �   �   N   G  k   E   ,  �   5      `   q   �   �   �   �   ?      �   �           Q     d           �         �  �   e              �   �   9         )  V   �          V      i                           �   (  X  &  ]   #   F  �   ;   �   Z  {      �       �   +     1. Turn radio off.
 2. Connect cable to DATA terminal.
 3. Press and hold in [DISP] key while turning on radio
      ("CLONE" will appear on radio LCD).
 4. Press [RECEIVE] screen button
      ("-WAIT-" will appear on radio LCD).
5. Finally, press OK button below.
 %(value)s must be between %(min)i and %(max)i %i Memories and shift all up %i Memories and shift all up %i Memory %i Memories %i Memory and shift block up %i Memories and shift block up %s has not been saved. Save before closing? (none) ...and %i more 1. Ensure your firmware version is 4_10 or higher
2. Turn radio off
3. Connect your interface cable
4. Turn radio on
5. Press and release PTT 3 times while holding MONI key
6. Supported baud rates: 57600 (default) and 19200
   (rotate dial while holding MONI to change)
7. Click OK
 1. Turn Radio off.
2. Connect data cable.
3. While holding "A/N LOW" button, turn radio on.
4. <b>After clicking OK</b>, press "SET MHz" to send image.
 1. Turn Radio off.
2. Connect data cable.
3. While holding "A/N LOW" button, turn radio on.
4. Press "MW D/MR" to receive image.
5. Make sure display says "-WAIT-" (see note below if not)
6. Click OK to dismiss this dialog and start transfer.
Note: if you don't see "-WAIT-" at step 5, try cycling
      power and pressing and holding red "*L" button to unlock
      radio, then start back at step 1.
 1. Turn Radio off.
2. Connect data cable.
3. While holding "TONE" and "REV" buttons, turn radio on.
4. <b>After clicking OK</b>, press "TONE" to send image.
 1. Turn Radio off.
2. Connect data cable.
3. While holding "TONE" and "REV" buttons, turn radio on.
4. Press "REV" to receive image.
5. Make sure display says "CLONE RX" and green led is blinking
6. Click OK to start transfer.
 1. Turn radio off.
2. Connect cable
3. Press and hold in the MHz, Low, and D/MR keys on the radio while turning it on
4. Radio is in clone mode when TX/RX is flashing
5. <b>After clicking OK</b>, press the MHz key on the radio to send image.
    ("TX" will appear on the LCD). 
 1. Turn radio off.
2. Connect cable
3. Press and hold in the MHz, Low, and D/MR keys on the radio while turning it on
4. Radio is in clone mode when TX/RX is flashing
5. Press the Low key on the radio ("RX" will appear on the LCD).
6. Click OK. 1. Turn radio off.
2. Connect cable to ACC jack.
3. Press and hold in the [MODE &lt;] and [MODE &gt;] keys while
     turning the radio on ("CLONE MODE" will appear on the
     display).
4. <b>After clicking OK</b> here, press the [C.S.] key to
    send image.
 1. Turn radio off.
2. Connect cable to ACC jack.
3. Press and hold in the [MODE &lt;] and [MODE &gt;] keys while
     turning the radio on ("CLONE MODE" will appear on the
     display).
4. <b>After clicking OK</b>, press the [A] key to send image.
 1. Turn radio off.
2. Connect cable to ACC jack.
3. Press and hold in the [MODE &lt;] and [MODE &gt;] keys while
     turning the radio on ("CLONE MODE" will appear on the
     display).
4. Click OK here.
    ("Receiving" will appear on the LCD).
 1. Turn radio off.
2. Connect cable to ACC jack.
3. Press and hold in the [MODE &lt;] and [MODE &gt;] keys while
     turning the radio on ("CLONE MODE" will appear on the
     display).
4. Press the [A](RCV) key ("receiving" will appear on the LCD).
 1. Turn radio off.
2. Connect cable to ACC jack.
3. Press and hold in the [MODE &lt;] and [MODE &gt;] keys while
     turning the radio on ("CLONE MODE" will appear on the
     display).
4. Press the [C] key ("RX" will appear on the LCD).
 1. Turn radio off.
2. Connect cable to CAT/LINEAR jack.
3. Press and hold in the [MODE &lt;] and [MODE &gt;] keys while
     turning the radio on ("CLONE MODE" will appear on the
     display).
4. <b>After clicking OK</b>,
     press the [C](SEND) key to send image.
 1. Turn radio off.
2. Connect cable to DATA jack.
3. Press and hold in the "left" [V/M] key while turning the
     radio on.
4. Rotate the "right" DIAL knob to select "CLONE START".
5. Press the [SET] key. The display will disappear
     for a moment, then the "CLONE" notation will appear.
6. <b>After clicking OK</b>, press the "left" [V/M] key to
     send image.
 1. Turn radio off.
2. Connect cable to DATA jack.
3. Press and hold in the "left" [V/M] key while turning the
     radio on.
4. Rotate the "right" DIAL knob to select "CLONE START".
5. Press the [SET] key. The display will disappear
     for a moment, then the "CLONE" notation will appear.
6. Press the "left" [LOW] key ("CLONE -RX-" will appear on
     the display).
 1. Turn radio off.
2. Connect cable to DATA jack.
3. Press and hold in the [FW] key while turning the radio on
     ("CLONE" will appear on the display).
4. <b>After clicking OK</b>, press the [BAND] key to send image.
 1. Turn radio off.
2. Connect cable to DATA jack.
3. Press and hold in the [FW] key while turning the radio on
     ("CLONE" will appear on the display).
4. Press the [MODE] key ("-WAIT-" will appear on the LCD).
 1. Turn radio off.
2. Connect cable to DATA jack.
3. Press and hold in the [MHz(PRI)] key while turning the
     radio on.
4. Rotate the DIAL job to select "F-7 CLONE".
5. Press and hold in the [BAND(SET)] key. The display
     will disappear for a moment, then the "CLONE" notation
     will appear.
6. Press the [LOW(ACC)] key ("--RX--" will appear on the display).
 1. Turn radio off.
2. Connect cable to DATA jack.
3. Press and hold in the [MHz(PRI)] key while turning the
 radio on.
4. Rotate the DIAL job to select "F-7 CLONE".
5. Press and hold in the [BAND(SET)] key. The display
 will disappear for a moment, then the "CLONE" notation
 will appear.
6. <b>After clicking OK</b>, press the [V/M(MW)] key to send image.
 1. Turn radio off.
2. Connect cable to DATA terminal.
3. Press and hold [DISP] key while turning on radio
     ("CLONE" will appear on the display).
4. <b>After clicking OK here in chirp</b>,
     press the [Send] screen button.
 1. Turn radio off.
2. Connect cable to DATA terminal.
3. Press and hold in the [F] key while turning the radio on
     ("CLONE" will appear on the display).
4. <b>After clicking OK</b>, press the [BAND] key to send image.
 1. Turn radio off.
2. Connect cable to DATA terminal.
3. Press and hold in the [F] key while turning the radio on
     ("CLONE" will appear on the display).
4. Press the [Dx] key ("-WAIT-" will appear on the LCD).
 1. Turn radio off.
2. Connect cable to DATA terminal.
3. Press and hold in the [MHz(SETUP)] key while turning the radio
     on ("CLONE" will appear on the display).
4. <b>After clicking OK</b>, press the [REV(DW)] key
     to send image.
 1. Turn radio off.
2. Connect cable to DATA terminal.
3. Press and hold in the [MHz(SETUP)] key while turning the radio
     on ("CLONE" will appear on the display).
4. Press the [MHz(SETUP)] key
     ("-WAIT-" will appear on the LCD).
 1. Turn radio off.
2. Connect cable to MIC Jack.
3. Press and hold in the [MHz(SETUP)] key while turning the radio
     on ("CLONE" will appear on the display).
4. <b>After clicking OK</b>, press the [GM(AMS)] key
     to send image.
 1. Turn radio off.
2. Connect cable to MIC Jack.
3. Press and hold in the [MHz(SETUP)] key while turning the radio
     on ("CLONE" will appear on the display).
4. Press the [MHz(SETUP)] key
     ("-WAIT-" will appear on the LCD).
 1. Turn radio off.
2. Connect cable to MIC/EAR jack.
3. Press and hold in the [F/W] key while turning the radio on
    ("CLONE" will appear on the display).
4. <b>After clicking OK</b>, press the [VFO(DW)SC] key to receive
    the image from the radio.
 1. Turn radio off.
2. Connect cable to MIC/EAR jack.
3. Press and hold in the [F/W] key while turning the radio on
    ("CLONE" will appear on the display).
4. Press the [MR(SKP)SC] key ("CLONE WAIT" will appear
    on the LCD).
5. Click OK to send image to radio.
 1. Turn radio off.
2. Connect cable to MIC/SP jack.
3. Press and hold [PTT] &amp; Knob while turning the
     radio on.
4. <b>After clicking OK</b>, press the [PTT] switch to send image.
 1. Turn radio off.
2. Connect cable to MIC/SP jack.
3. Press and hold [PTT] &amp; Knob while turning the
     radio on.
4. Press the [MONI] switch ("WAIT" will appear on the LCD).
5. Press OK.
 1. Turn radio off.
2. Connect cable to MIC/SP jack.
3. Press and hold in the [F/W] key while turning the radio on
     ("CLONE" will appear on the display).
4. <b>After clicking OK</b>, press the [BAND] key to send image.
 1. Turn radio off.
2. Connect cable to MIC/SP jack.
3. Press and hold in the [F/W] key while turning the radio on
     ("CLONE" will appear on the display).
4. Press the [V/M] key ("-WAIT-" will appear on the LCD).
 1. Turn radio off.
2. Connect cable to MIC/SP jack.
3. Press and hold in the [MON-F] key while turning the radio on
     ("CLONE" will appear on the display).
4. <b>After clicking OK</b>, press the [BAND] key to send image.
 1. Turn radio off.
2. Connect cable to MIC/SP jack.
3. Press and hold in the [MON-F] key while turning the radio on
     ("CLONE" will appear on the display).
4. Press the [V/M] key ("CLONE WAIT" will appear on the LCD).
 1. Turn radio off.
2. Connect cable to MIC/SP jack.
3. Press and hold in the [MONI] switch while turning the
     radio on.
4. Rotate the DIAL job to select "F8 CLONE".
5. Press the [F/W] key momentarily.
6. <b>After clicking OK</b>, hold the [PTT] switch
     for one second to send image.
 1. Turn radio off.
2. Connect cable to MIC/SP jack.
3. Press and hold in the [MONI] switch while turning the
     radio on.
4. Rotate the DIAL job to select "F8 CLONE".
5. Press the [F/W] key momentarily.
6. Press the [MONI] switch ("--RX--" will appear on the LCD).
 1. Turn radio off.
2. Connect cable to MIC/SP jack.
3. Press and hold in the [moni] key while turning the radio on.
4. Select CLONE in menu, then press F. Radio restarts in clone mode.
     ("CLONE" will appear on the display).
5. <b>After clicking OK</b>, briefly hold [PTT] key to send image.
    ("-TX-" will appear on the LCD). 
 1. Turn radio off.
2. Connect cable to mic jack.
3. Press and hold in the [LOW(A/N)] key while turning the radio on.
4. <b>After clicking OK</b>, press the [MHz(SET)] key to send image.
 1. Turn radio off.
2. Connect cable to mic jack.
3. Press and hold in the [LOW(A/N)] key while turning the radio on.
4. Press the [D/MR(MW)] key ("--WAIT--" will appear on the LCD).
 1. Turn radio off.
2. Connect cable to mic jack.
3. Press and hold in the [MHz], [LOW], and [D/MR] keys
   while turning the radio on.
4. <b>After clicking OK</b>, press the [MHz(SET)] key to send image.
 1. Turn radio off.
2. Connect cable to mic jack.
3. Press and hold in the [MHz], [LOW], and [D/MR] keys
   while turning the radio on.
4. Press the [D/MR(MW)] key ("--WAIT--" will appear on the LCD).
 1. Turn radio off.
2. Connect cable to mic/spkr connector.
3. Make sure connector is firmly connected.
4. Turn radio on (volume may need to be set at 100%).
5. Ensure that the radio is tuned to channel with no activity.
6. Click OK to download image from device.
 1. Turn radio off.
2. Connect cable to mic/spkr connector.
3. Make sure connector is firmly connected.
4. Turn radio on (volume may need to be set at 100%).
5. Ensure that the radio is tuned to channel with no activity.
6. Click OK to upload image to device.
 1. Turn radio off.
2. Connect cable to mic/spkr connector.
3. Make sure connector is firmly connected.
4. Turn radio on.
5. Ensure that the radio is tuned to channel with no activity.
6. Click OK to download image from device.
 1. Turn radio off.
2. Connect cable to mic/spkr connector.
3. Make sure connector is firmly connected.
4. Turn radio on.
5. Ensure that the radio is tuned to channel with no activity.
6. Click OK to upload image to device.
 1. Turn radio off.
2. Connect data cable.
3. Prepare radio for clone.
4. <b>After clicking OK</b>, press the key to send image.
 1. Turn radio off.
2. Connect data cable.
3. Prepare radio for clone.
4. Press the key to receive the image.
 1. Turn radio off.
2. Connect mic and hold [ACC] on mic while powering on.
    ("CLONE" will appear on the display)
3. Replace mic with PC programming cable.
4. <b>After clicking OK</b>, press the [SET] key to send image.
 1. Turn radio off.
2. Connect mic and hold [ACC] on mic while powering on.
    ("CLONE" will appear on the display)
3. Replace mic with PC programming cable.
4. Press the [DISP/SS] key
    ("R" will appear on the lower left of LCD).
 1. Turn radio off.
2. Remove front head.
3. Connect data cable to radio, use the same connector where
     head was connected to, <b>not the mic connector</b>.
4. Click OK.
 1. Turn radio off.
3. Press and hold in the [moni] key while turning the radio on.
4. Select CLONE in menu, then press F. Radio restarts in clone mode.
     ("CLONE" will appear on the display).
5. Press the [moni] key ("-RX-" will appear on the LCD).
 1. Turn radio on.
2. Connect cable to DATA terminal.
3. Unclip battery.
4. Press and hold in the [AMS] key and power key while clipping 
 in back battery the("ADMS" will appear on the display).
5. <b>After clicking OK</b>, press the [BAND] key.
 1. Turn radio on.
2. Connect cable to DATA terminal.
3. Unclip battery.
4. Press and hold in the [AMS] key and power key while clipping 
 in back battery the("ADMS" will appear on the display).
5. Press the [MODE] key ("-WAIT-" will appear on the LCD). OK</b>
 click <b>Then 1. Turn radio on.
2. Connect cable to mic/spkr connector.
3. Make sure connector is firmly connected.
4. Click OK to download image from device.

It will may not work if you turn on the radio with the cable already attached
 1. Turn radio on.
2. Connect cable to mic/spkr connector.
3. Make sure connector is firmly connected.
4. Click OK to upload the image to device.

It will may not work if you turn on the radio with the cable already attached A new CHIRP version is available. Please visit the website as soon as possible to download it! About About CHIRP All All Files All supported formats| Amateur An error has occurred Applying settings Available modules Bandplan Bands Banks Bin Browser Building Radio Browser Canada Changing this setting requires refreshing the settings from the image, which will happen now. Channels with equivalent TX and RX %s are represented by tone mode of "%s" Chirp Image Files Choice Required Choose %s DTCS Code Choose %s Tone Choose Cross Mode Choose duplex Choose the module to load from issue %i: City Click on the "Special Channels" toggle-button of the memory
editor to see/set the EXT channels. P-VFO channels 100-109
are considered Settings.
Only a subset of the over 200 available radio settings
are supported in this release.
Ignore the beeps from the radio on upload and download.
 Clone completed, checking for spurious bytes Cloning Cloning from radio Cloning to radio Close Close file Cluster %i memory Cluster %i memories Comment Communicate with radio Complete Connect your interface cable to the PC Port on the
back of the 'TX/RX' unit. NOT the Com Port on the head.
 Convert to FM Copy Country Cross mode Custom Port Custom... Cut DTCS DTCS
Polarity DTMF decode DV Memory Danger Ahead Dec Delete Developer Mode Developer state is now %s. CHIRP must be restarted to take effect Diff Raw Memories Digital Code Digital Modes Disable reporting Disabled Distance Do not prompt again for %s Double-click to change bank name Download Download from radio Download from radio... Download instructions Driver information Dual-mode digital repeaters that support analog will be shown as FM Duplex Edit details for %i memories Edit details for memory %i Enable Automatic Edits Enabled Enter Frequency Enter Offset (MHz) Enter TX Frequency (MHz) Enter a new name for bank %s: Enter custom port: Erased memory %s Error applying settings Error communicating with radio Experimental driver Export can only write CSV files Export to CSV Export to CSV... Extra FM Radio FREE repeater database, which provides most up-to-date
information about repeaters in Europe. No account is
required. Failed to load radio browser Failed to parse result Features File does not exist: %s Files Filter Filter results with location matching this string Find Find Next Find... Finished radio job %s Follow these instructions to download the radio memory:
1 - Connect your interface cable
2 - Radio > Download from radio: DO NOT mess with the radio
during download!
3 - Disconnect your interface cable
 Follow these instructions to download the radio memory:
1 - Connect your interface cable
2 - Radio > Download from radio: Don't adjust any settings
on the radio head!
3 - Disconnect your interface cable
 Follow these instructions to download the radio memory:
1 - Turn off your radio
2 - Connect your interface cable
3 - Turn on your radio, volume @ 50%
4 - CHIRP Menu - Radio - Download from radio
 Follow these instructions to download your config:
1 - Turn off your radio
2 - Connect your interface cable to the Speaker-2 jack
3 - Turn on your radio
4 - Radio > Download from radio
5 - Disconnect the interface cable! Otherwise there will be
    no right-side audio!
 Follow these instructions to download your info:
1 - Turn off your radio
2 - Connect your interface cable
3 - Turn on your radio
4 - Do the download of your radio data
 Follow these instructions to download your radio:
1 - Turn off your radio
2 - Connect your interface cable
3 - Turn on your radio (unlock it if password protected)
4 - Click OK to start
 Follow these instructions to read your radio:
1 - Turn off your radio
2 - Connect your interface cable
3 - Turn on your radio
4 - Click OK to start
 Follow these instructions to upload the radio memory:
1 - Connect your interface cable
2 - Radio > Upload to radio: DO NOT mess with the radio
during upload!
3 - Disconnect your interface cable
 Follow these instructions to upload the radio memory:
1 - Connect your interface cable
2 - Radio > Upload to radio: Don't adjust any settings
on the radio head!
3 - Disconnect your interface cable
 Follow these instructions to upload the radio memory:
1 - Turn off your radio
2 - Connect your interface cable
3 - Turn on your radio, volume @ 50%
4 - CHIRP Menu - Radio - Upload to radio
 Follow these instructions to upload your config:
1 - Turn off your radio
2 - Connect your interface cable to the Speaker-2 jack
3 - Turn on your radio
4 - Radio > Upload to radio
5 - Disconnect the interface cable, otherwise there will be
    no right-side audio!
6 - Cycle power on the radio to exit clone mode
 Follow these instructions to upload your info:
1 - Turn off your radio
2 - Connect your interface cable
3 - Turn on your radio
4 - Do the upload of your radio data
 Follow these instructions to upload your radio:
1 - Turn off your radio
2 - Connect your interface cable
3 - Turn on your radio (unlock it if password protected)
4 - Click OK to start
 Follow these instructions to write your radio:
1 - Turn off your radio
2 - Connect your interface cable
3 - Turn on your radio
4 - Click OK to start
 Follow this instructions to download your info:
1 - Turn off your radio
2 - Connect your interface cable
3 - Turn on your radio
4 - Do the download of your radio data
 Follow this instructions to read your radio:
1 - Turn off your radio
2 - Connect your interface cable
3 - Turn on your radio
4 - Do the download of your radio data
 Follow this instructions to upload your info:
1 - Turn off your radio
2 - Connect your interface cable
3 - Turn on your radio
4 - Do the upload of your radio data
 Follow this instructions to write your radio:
1 - Turn off your radio
2 - Connect your interface cable
3 - Turn on your radio
4 - Do the upload of your radio data
 Found empty list value for %(name)s: %(value)r Frequency GMRS Getting settings Goto Memory Goto Memory: Goto... Help Help Me... Hex Hide empty memories If set, sort results by distance from these coordinates Import Import from file... Import not recommended Index Info Information Insert Row Above Install desktop icon? Interact with driver Invalid %(value)s (use decimal degrees) Invalid Entry Invalid ZIP code Invalid edit: %s Invalid or unsupported module file Invalid value: %r Issue number: LIVE Latitude Length must be %i Limit Bands Limit Modes Limit Status Limit results to this distance (km) from coordinates Load Module... Load module from issue Load module from issue... Loading modules can be extremely dangerous, leading to damage to your computer, radio, or both. NEVER load a module from a source you do not trust, and especially not from anywhere other than the main CHIRP website (chirp.danplanet.com). Loading a module from another source is akin to giving them direct access to your computer and everything on it! Proceed despite this risk? Loading settings Logo string 1 (12 characters) Logo string 2 (12 characters) Longitude Memories Memory %i is not deletable Memory must be in a bank to be edited Memory {num} not in bank {bank} Mode Model Modes Module Module Loaded Module loaded successfully More than one port found: %s Move Down Move Up New Window New version available No empty rows below! No modules found No modules found in issue %i No results No results! Number Offset Only certain bands Only certain modes Only memory tabs may be exported Only working repeaters Open Open Recent Open Stock Config Open a file Open a module Open debug log Open in new window Open stock config directory Optional: -122.0000 Optional: 100 Optional: 45.0000 Optional: County, Hospital, etc. Overwrite memories? P-VFO channels 100-109 are considered Settings.
Only a subset of the over 130 available radio settings
are supported in this release.
 Parsing Paste Pasted memories will overwrite %s existing memories Pasted memories will overwrite memories %s Pasted memories will overwrite memory %s Pasted memory will overwrite memory %s Please be sure to quit CHIRP before installing the new version! Please follow this steps carefully:
1 - Turn on your radio
2 - Connect the interface cable to your radio
3 - Click the button on this window to start download
    (you may see another dialog, click ok)
4 - Radio will beep and led will flash
5 - You will get a 10 seconds timeout to press "MON" before
    data upload start
6 - If all goes right radio will beep at end.
After cloning remove the cable and power cycle your radio to
get into normal mode.
 Please follow this steps carefully:
1 - Turn on your radio
2 - Connect the interface cable to your radio.
3 - Click the button on this window to start download
    (Radio will beep and led will flash)
4 - Then press the "A" button in your radio to start cloning.
    (At the end radio will beep)
 Please note that developer mode is intended for use by developers of the CHIRP project, or under the direction of a developer. It enables behaviors and functions that can damage your computer and your radio if not used with EXTREME care. You have been warned! Proceed anyway? Please wait Plug in your cable and then click OK Port Power Press enter to set this in memory Print Preview Printing Properties Query %s Query Source RX DTCS Radio Radio did not ack block %i Radio information Radio sent data after the last awaited block, this happens when the selected model is a non-US but the radio is a US one. Please choose the correct model and try again. RadioReference Canada requires a login before you can query RadioReference.com is the world's largest
radio communications data provider
<small>Premium account required</small> Refresh required Refreshed memory %s Reload Driver Reload Driver and File Rename bank RepeaterBook is Amateur Radio's most comprehensive,
worldwide, FREE repeater directory. Reporting enabled Reporting helps the CHIRP project know which radio models and OS platforms to spend our limited efforts on. We would really appreciate if you left it enabled. Really disable reporting? Restart Required Restore %i tab Restore %i tabs Retrieved settings Save Save before closing? Save file Saved settings Scanlists Scrambler Security Risk Select Bandplan... Select Bands Select Modes Select a bandplan Service Settings Show Raw Memory Show debug log location Show extra fields Some memories are incompatible with this radio Some memories are not deletable Sort %i memory Sort %i memories Sort %i memory ascending Sort %i memories ascending Sort %i memory descending Sort %i memories descending Sort by column: Sort memories Sorting State State/Province Success The DMR-MARC Worldwide Network The FT-450D radio driver loads the 'Special Channels' tab
with the PMS scanning range memories (group 11), 60meter
channels (group 12), the QMB (STO/RCL) memory, the HF and
50m HOME memories and all the A and B VFO memories.
There are VFO memories for the last frequency dialed in
each band. The last mem-tune config is also stored.
These Special Channels allow limited field editing.
This driver also populates the 'Other' tab in the channel
memory Properties window. This tab contains values for
those channel memory settings that don't fall under the
standard Chirp display columns. With FT450 support, the gui now
uses the mode 'DIG' to represent data modes and a new column
'DATA MODE' to select USER-U, USER-L and RTTY 
 The X3Plus driver is currently experimental.
There are no known issues but you should proceed with caution.
Please save an unedited copy of your first successful
download to a CHIRP Radio Images (*.img) file.
 The author of this module is not a recognized CHIRP developer. It is recommended that you not load this module as it could pose a security risk. Proceed anyway? The recommended procedure for importing memories is to open the source file and copy/paste memories from it into your target image. If you continue with this import function, CHIRP will replace all memories in your currently-open file with those in %(file)s. Would you like to open this file to copy/paste memories across, or proceed with the import? This Memory This driver has been tested with v3 of the ID-5100. If your radio is not fully updated please help by opening a bug report with a debug log so we can add support for the other revisions. This driver is in development and should be considered as experimental. This is an early stage beta driver
 This is an early stage beta driver - upload at your own risk
 This is an experimental driver for the Quansheng UV-K5. It may harm your radio, or worse. Use at your own risk.

Before attempting to do any changes please download the memory image from the radio with chirp and keep it. This can be later used to recover the original settings. 

some details are not yet implemented This is experimental support for BJ-9900 which is still under development.
Please ensure you have a good backup with OEM software.
Also please send in bug and enhancement requests!
You have been warned. Proceed at your own risk! This memory and shift all up This memory and shift block up This radio has a tricky way of enter into program mode,
even the original software has a few tries to get inside.
I will try 8 times (most of the time ~3 will doit) and this
can take a few seconds, if don't work, try again a few times.
If you can get into it, please check the radio and cable.
 This should only be enabled if you are using modified firmware that supports wider frequency coverage. Enabling this will cause CHIRP not to enforce OEM restrictions and may lead to undefined or unregulated behavior. Use at your own risk! This will load a module from a website issue Tone Tone Mode Tone Squelch Tuning Step USB Port Finder Unable to determine port for your cable. Check your drivers and connections. Unable to edit memory before radio is loaded Unable to find stock config %r Unable to open the clipboard Unable to query Unable to read last block. This often happens when the selected model is US but the radio is a non-US one (or widebanded). Please choose the correct model and try again. Unable to reveal %s on this system Unable to set %s on this memory United States Unplug your cable (if needed) and then click OK Upload instructions Upload to radio Upload to radio... Uploaded memory %s Use fixed-width font Use larger font Value does not fit in %i bits Value must be at least %.4f Value must be at most %.4f Value must be exactly %i decimal digits Value must be zero or greater Values Vendor View WARNING! Warning Warning: %s Welcome Would you like CHIRP to install a desktop icon for you? Your cable appears to be on port: bits bytes bytes each disabled enabled {bank} is full Project-Id-Version: CHIRP
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-01-17 21:19-0300
Last-Translator: MELERIX
Language-Team: 
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 3.4.2
X-Poedit-SourceCharset: UTF-8
X-Poedit-Basepath: ..
X-Poedit-SearchPath-0: .
  1. Apaga la radio.
 2. Conecta el cable al terminal DATA.
 3. Presiona y mantén la tecla [DISP] mientras enciendes la radio
      ("CLONE" aparecerá en la LCD de la radio).
 4. Presiona el botón de pantalla [RECEIVE]
      ("-WAIT-" aparecerá en la LCD de la radio).
5. Finalmente, presiona el botón Aceptar abajo.
 %(value)s debe estar entre %(min)i y %(max)i %i Memoria y desplazar todo hacia arriba %i Memorias y desplazar todo hacia arriba %i Memoria %i Memorias %i Memoria y desplazar bloque hacia arriba %i Memorias y desplazar bloque hacia arriba %s no ha sido guardado. ¿Guardar antes de cerrar? (nada) ...y %i más 1. Asegúrate de que tu versión de firmware es 4_10 o superior
2. Apaga la radio
3. Conecta tu cable de interfaz
4. Enciende la radio
5. Presiona y suelta PTT 3 veces mientras mantienes presionada la tecla MONI
6. Tasas de baudios admitidas: 57600 (predeterminado) y 19200
   (gira el dial mientras mantienes presionado MONI para cambiar)
7. Cliquea Aceptar
 1. Apaga la radio.
2. Conecta el cable de datos.
3. Mientras mantienes presionado el botón "A/N LOW", enciende la radio.
4. <b>Después de cliquear Aceptar</b>, presiona "SET MHz" para enviar imagen.
 1. Apaga la radio.
2. Conecta el cable de datos.
3. Mientras mantienes presionado el botón "A/N LOW", enciende la radio.
4. Presiona "MW D/MR" para recibir la imagen.
5. Asegúrate de que la pantalla dice "-WAIT-" (mira la nota abajo si no)
6. Cliquea Aceptar para descartar este dialogo e iniciar la transferencia.
Nota: si no ves "-WAIT-" en el paso 5, intenta apagar y
      encender y manteniendo presionado el botón rojo "*L" para desbloquear
      la radio, entonces vuelve a iniciar en el paso 1.
 1. Apaga la radio.
2. Conecta el cable de datos.
3. Mientras mantienes presionados los botones "TONE" y "REV", enciende la radio.
4. <b>Después de cliquear Aceptar</b>, presiona "TONE" para enviar la imagen.
 1. Apaga la radio.
2. Conecta el cable de datos.
3. Mientras mantienes presionados los botones "TONE" y "REV", enciende la radio.
4. Presiona "REV" para recibir la imagen.
5. Asegúrate de que la pantalla diga "CLONE RX" y el led verde esté parpadeando
6. Cliquea Aceptar para iniciar la transferencia.
 1. Apaga la radio.
2. Conecta el cable
3. Presiona y mantén las teclas MHz, Low, y D/MR en la radio mientras la enciendes
4. La radio estará en modo clonación cuando TX/RX esté parpadeando
5. <b>Después de cliquear Aceptar</b>, presiona la tecla MHz en la radio para enviar la imagen.
    ("TX" aparecerá en la LCD). 
 1. Apaga la radio.
2. Conecta el cable
3. Presiona y mantén las teclas MHz, Low, y D/MR en la radio mientras la enciendes
4. La radio estará en modo clonación cuando TX/RX esté parpadeando
5. Presiona la tecla Low en la radio ("RX" aparecerá en la LCD).
6. Cliquea Aceptar. 1. Apaga la radio.
2. Conecta el cable al conector ACC.
3. Presiona y mantén las teclas [MODE &lt;] y [MODE &gt;] mientras
     enciendes la radio ("CLONE MODE" aparecerá en la
     pantalla).
4. <b>Después de cliquear Aceptar</b> aquí, presiona la tecla [C.S.] para
    enviar la imagen.
 1. Apaga la radio.
2. Conecta el cable al conector ACC.
3. Presiona y mantén las teclas [MODE &lt;] y [MODE &gt;] mientras
     enciendes la radio ("CLONE MODE" aparecerá en la
     pantalla).
4. <b>Después de cliquear Aceptar</b> aquí, presiona la tecla [A] para
    enviar la imagen.
 1. Apaga la radio.
2. Conecta el cable al conector ACC.
3. Presiona y mantén las teclas [MODE &lt;] y [MODE &gt;] mientras
     enciendes la radio ("CLONE MODE" aparecerá en la
     pantalla).
4. Cliquea Aceptar aquí.
    ("Receiving" aparecerá en la LCD).
 1. Apaga la radio.
2. Conecta el cable al conector ACC.
3. Presiona y mantén las teclas [MODE &lt;] y [MODE &gt;] mientras
     enciendes la radio ("CLONE MODE" aparecerá en la
     pantalla).
4. Presiona la tecla [A](RCV) ("receiving" aparecerá en la LCD).
 1. Apaga la radio.
2. Conecta el cable al conector ACC.
3. Presiona y mantén las teclas [MODE &lt;] y [MODE &gt;] mientras
     enciendes la radio ("CLONE MODE" aparecerá en la
     pantalla).
4. Presiona la tecla [C] ("RX" aparecerá en la LCD).
 1. Apaga la radio.
2. Conecta el cable al conector CAT/LINEAR.
3. Presiona y mantén las teclas [MODE &lt;] y [MODE &gt;] mientras
     enciendes la radio ("CLONE MODE" aparecerá en la
     pantalla).
4. <b>Después de cliquear Aceptar</b>,
     presiona la tecla [C](SEND) para enviar la imagen.
 1. Apaga la radio.
2. Conecta el cable al conector DATA.
3. Presiona y mantén la tecla "izquierda" [V/M] mientras enciendes la
     radio.
4. Gira la perilla "derecha" DIAL para seleccionar "CLONE START".
5. Presiona la tecla [SET]. La pantalla desaparecerá
     por un momento, entonces la notación "CLONE" aparecerá.
6. <b>Después de cliquear Aceptar</b>, presiona la tecla "izquierda" [V/M] para
     enviar la imagen.
 1. Apaga la radio.
2. Conecta el cable al conector DATA.
3. Presiona y mantén la tecla "izquierda" [V/M] mientras enciendes la
     radio.
4. Gira la perilla "derecha" DIAL para seleccionar "CLONE START".
5. Presiona la tecla [SET]. La pantalla desaparecerá
     por un momento, entonces la notación "CLONE" aparecerá.
6. Presiona la tecla "izquierda" [LOW] ("CLONE -RX-" aparecerá en
     la pantalla).
 1. Apaga la radio.
2. Conecta el cable al terminal DATA.
3. Presiona y mantén la tecla [FW] mientras enciendes la radio
     ("CLONE" aparecerá en la pantalla).
4. <b>Después de cliquear Aceptar</b>, presiona la tecla [BAND] para enviar la imagen.
 1. Apaga la radio.
2. Conecta el cable al conector DATA.
3. Presiona y mantén la tecla [FW] mientras enciendes la radio
     ("CLONE" aparecerá en la pantalla).
4. Presiona la tecla [MODE] ("-WAIT-" aparecerá en la LCD).
 1. Apaga la radio.
2. Conecta el cable al conector DATA.
3. Presiona y mantén la tecla [MHz(PRI)] mientras enciendes la
     radio.
4. Gira la perilla DIAL para seleccionar "F-7 CLONE".
5. Presiona y mantén la tecla [BAND(SET)]. La pantalla
     desaparecerá por un momento, entonces la notación "CLONE"
     aparecerá.
6. Presiona la tecla [LOW(ACC)] ("--RX--" aparecerá en la pantalla).
 1. Apaga la radio.
2. Conecta el cable al conector DATA.
3. Presiona y mantén la tecla [MHz(PRI)] mientras enciendes la
 radio.
4. Gira la perilla DIAL para seleccionar "F-7 CLONE".
5. Presiona y mantén la tecla [BAND(SET)]. La pantalla
 desaparecerá por un momento, entonces la notación "CLONE"
 aparecerá.
6. <b>Después de cliquear Aceptar</b>, presiona la tecla [V/M(MW)] para enviar la imagen.
 1. Apaga la radio.
2. Conecta el cable al terminal DATA.
3. Presiona y mantén la tecla [DISP] mientras enciendes la radio
     ("CLONE" aparecerá en la pantalla).
4. <b>Después de cliquear Aceptar aquí en chirp</b>,
     presiona el botón de pantalla [Send].
 1. Apaga la radio.
2. Conecta el cable al terminal DATA.
3. Presiona y mantén la tecla [F] mientras enciendes la radio
     ("CLONE" aparecerá en la pantalla).
4. <b>Después de cliquear Aceptar</b>, presiona la tecla [BAND] para enviar la imagen.
 1. Apaga la radio.
2. Conecta el cable al terminal DATA.
3. Presiona y mantén la tecla [F] mientras enciendes la radio
     ("CLONE" aparecerá en la pantalla).
4. Presiona la tecla [Dx] ("-WAIT-" aparecerá en la LCD).
 1. Apaga la radio.
2. Conecta el cable al terminal DATA.
3. Presiona y mantén la tecla [MHz(SETUP)] mientras enciendes la radio
     ("CLONE" aparecerá en la pantalla).
4. <b>Después de cliquear Aceptar</b>, presiona la tecla [REV(DW)]
     para enviar la imagen.
 1. Apaga la radio.
2. Conecta el cable al terminal DATA.
3. Presiona y mantén la tecla [MHz(SETUP)] mientras enciendes la radio
     ("CLONE" aparecerá en la pantalla).
4. Presiona la tecla [MHz(SETUP)]
     ("-WAIT-" aparecerá en la LCD).
 1. Apaga la radio.
2. Conecta el cable al conector MIC.
3. Presiona y mantén la tecla [MHz(SETUP)] mientras enciendes la radio
     ("CLONE" aparecerá en la pantalla).
4. <b>Después de cliquear Aceptar</b>, presiona la tecla [GM(AMS)]
     para enviar la imagen.
 1. Apaga la radio.
2. Conecta el cable la conector MIC.
3. Presiona y mantén la tecla [MHz(SETUP)] mientras enciendes la radio
     ("CLONE" aparecerá en la pantalla).
4. Presiona la tecla [MHz(SETUP)]
     ("-WAIT-" aparecerá en la LCD).
 1. Apaga la radio.
2. Conecta el cable al conector MIC/EAR.
3. Presiona y mantén la tecla [F/W] mientras enciendes la radio
    ("CLONE" aparecerá en la pantalla).
4. <b>Después de cliquear Aceptar</b>, presiona la tecla [VFO(DW)SC] para recibir
    la imagen desde la radio.
 1. Apaga la radio.
2. Conecta el cable al conector MIC/EAR.
3. Presiona y mantén la tecla [F/W] mientras enciendes la radio
    ("CLONE" aparecerá en la pantalla).
4. Presiona la tecla [MR(SKP)SC] ("CLONE WAIT" aparecerá
    en la LCD).
5. Cliquea Aceptar para enviar la imagen a la radio.
 1. Apaga la radio.
2. Conecta el cable al conector MIC/SP.
3. Presiona y mantén la perilla [PTT] mientras enciendes la
     radio.
4. <b>Después de cliquear Aceptar</b>, presiona el interruptor [PTT] para enviar la imagen.
 1. Apaga la radio.
2. Conecta el cable al conector MIC/SP.
3. Presiona y mantén la perilla [PTT] mientras enciendes la
     radio.
4. Presiona el interruptor [MONI] ("WAIT" aparecerá en la LCD).
5. Presiona Aceptar.
 1. Apaga la radio.
2. Conecta el cable al conector MIC/SP.
3. Presiona y mantén la tecla [F/W] mientras enciendes la radio
     ("CLONE" aparecerá en la pantalla).
4. <b>Después de cliquear Aceptar</b>, presiona la tecla [BAND] para enviar la imagen.
 1. Apaga la radio.
2. Conecta el cable al conector MIC/SP.
3. Presiona y mantén la tecla [F/W] mientras enciendes la radio
     ("CLONE" aparecerá en pantalla).
4. Presiona la tecla [V/M] ("-WAIT-" aparecerá en la LCD).
 1. Apaga la radio.
2. Conecta el cable al conector MIC/SP.
3. Presiona y mantén la tecla [MON-F] mientras enciendes la radio
     ("CLONE" aparecerá en la pantalla).
4. <b>Después de cliquear Aceptar</b>, presiona la tecla [BAND] para enviar la imagen.
 1. Apaga la radio.
2. Conecta el cable al conector MIC/SP.
3. Presiona y mantén la tecla [MON-F] mientras enciendes la radio
     ("CLONE" aparecerá en la pantalla).
4. Presiona la tecla [V/M] ("CLONE WAIT" aparecerá en la LCD).
 1. Apaga la radio.
2. Conecta el cable al conector MIC/SP.
3. Presiona y mantén el interruptor [MONI] mientras enciendes la
     radio.
4. Gira el DIAL para seleccionar "F8 CLONE".
5. Presiona la tecla [F/W] momentáneamente.
6. <b>Después de cliquear Aceptar</b>, mantén presionado el interruptor [PTT]
     por un segundo para enviar la imagen.
 1. Apaga la radio.
2. Conecta el cable al conector MIC/SP.
3. Presiona y mantén el interruptor [MONI] mientras enciendes la
     radio.
4. Gira el DIAL para seleccionar "F8 CLONE".
5. Presiona la tecla [F/W] momentáneamente.
6. Presiona el interruptor [MONI] ("--RX--" aparecerá en la LCD).
 1. Apaga la radio.
2. Conecta el cable al conector MIC/SP.
3. Presiona y mantén la tecla [moni] mientras enciendes la radio.
4. Selecciona CLONE en el menú, entonces presiona F. La radio se reinicia en modo clonación.
     ("CLONE" aparecerá en la pantalla).
5. <b>Después de cliquear Aceptar</b>, mantén presionada brevemente la tecla [PTT] para enviar la imagen.
    ("-TX-" aparecerá en la pantalla). 
 1. Apaga la radio.
2. Conecta el cable al conector de micrófono.
3. Presiona y mantén la tecla [LOW(A/N)] mientras enciendes la radio.
4. <b>Después de cliquear Aceptar</b>, presiona la tecla [MHz(SET)] para enviar la imagen.
 1. Apaga la radio.
2. Conecta el cable al conector de micrófono.
3. Presiona y mantén la tecla [LOW(A/N)] mientras enciendes la radio.
4. Presiona la tecla [D/MR(MW)] ("--WAIT--" aparecerá en la LCD).
 1. Apaga la radio.
2. Conecta el cable al conector de micrófono.
3. Presiona y mantén las teclas [MHz], [LOW] y [D/MR]
   mientras enciendes la radio.
4. <b>Después de cliquear Aceptar</b>, presiona la tecla [MHz(SET)] para enviar la imagen.
 1. Apaga la radio.
2. Conecta el cable al conector de micrófono.
3. Presiona y mantén las teclas [MHz], [LOW] y [D/MR]
   mientras enciendes la radio.
4. Presiona la tecla [D/MR(MW)] ("--WAIT--" aparecerá en la LCD).
 1. Apaga la radio.
2. Conecta el cable al conector mic/spkr.
3. Asegúrate de que el conector esté firmemente conectado.
4. Enciende la radio (es posible que el volumen necesite establecerse al 100%).
5. Asegúrate de que la radio esté sintonizada a un canal sin actividad.
6. Cliquea Aceptar para descargar la imagen desde el dispositivo.
 1. Apaga la radio.
2. Conecta el cable al conector mic/spkr.
3. Asegúrate de que el conector esté firmemente conectado.
4. Enciende la radio (es posible que el volumen necesite establecerse al 100%).
5. Asegúrate de que la radio esté sintonizada a un canal sin actividad.
6. Cliquea Aceptar para cargar la imagen al dispositivo.
 1. Apaga la radio.
2. Conecta el cable al conector mic/spkr.
3. Asegúrate de que el conector esté firmemente conectado.
4. Enciende la radio.
5. Asegúrate de que la radio esté sintonizada a un canal sin actividad.
6. Cliquea Aceptar para descargar la imagen desde dispositivo.
 1. Apaga la radio.
2. Conecta el cable al conector mic/spkr.
3. Asegúrate de que el conector esté firmemente conectado.
4. Enciende la radio.
5. Asegúrate de que la radio esté sintonizada a un canal sin actividad.
6. Cliquea Aceptar para cargar la imagen al dispositivo.
 1. Apaga la radio.
2. Conecta el cable de datos.
3. Prepara la radio para clonar.
4. <b>Después de cliquear Aceptar</b>, presiona la tecla para enviar la imagen.
 1. Apaga la radio.
2. Conecta el cable de datos.
3. Prepara la radio para clonar.
4. Presiona la tecla para recibir la imagen.
 1. Apaga la radio.
2. Conecta el micrófono y mantén presionado [ACC] en el micrófono mientras lo enciendes.
    ("CLONE" aparecerá en la pantalla)
3. Remplaza el micrófono con el cable de programación de PC.
4. <b>Después de cliquear Aceptar</b>, presiona la tecla [SET] para enviar la imagen.
 1. Apaga la radio.
2. Conecta micrófono y mantén presionado [ACC] en micrófono mientras enciendes.
    ("CLONE" aparecerá en la pantalla)
3. Remplaza micrófono con el cable de programación de PC.
4. Presiona la tecla [DISP/SS]
    ("R" aparecerá en la parte inferior izquierda de la LCD).
 1. Apaga la radio.
2. Remueve la cabeza frontal.
3. Conecta el cable de datos a la radio, usa el mismo conector en donde
     la cabeza estaba conectada, <b>no el conector de micrófono</b>.
4. Cliquea Aceptar.
 1. Apaga la radio.
3. Presiona y mantén la tecla [moni] mientras enciendes la radio.
4. Selecciona CLONE en el menú, entonces presiona F. La radio se reiniciara en modo clonación.
     ("CLONE" aparecerá en la pantalla).
5. Presiona la tecla [moni] ("-RX-" aparecerá en la LCD).
 1. Enciende la radio.
2. Conecta el cable al terminal DATA.
3. Desengancha la batería.
4. Presiona y mantén la tecla [AMS] y la tecla de encendido mientras enganchas 
 de vuelta la batería ("ADMS" aparecerá en la pantalla).
5. <b>Después de cliquear Aceptar</b>, presiona la tecla [BAND].
 1. Enciende la radio.
2. Conecta el cable al terminal DATA.
3. Desengancha la batería.
4. Presiona y mantén la tecla [AMS] y la tecla de encendido mientras enganchas 
 de vuelta la batería ("ADMS" aparecerá en la pantalla).
5. Presiona la tecla [MODE] ("-WAIT-" aparecerá en la LCD). Entonces cliquea </b>
 Aceptar <b> 1. Enciende la radio.
2. Conecta el cable al conector mic/spkr.
3. Asegúrate de que el conector esté firmemente conectado.
4. Cliquea Aceptar para descargar la imagen desde el dispositivo.

Esto podría no funcionar si enciendes la radio con el cable ya conectado
 1. Enciende la radio.
2. Conecta el cable al conector mic/spkr.
3. Asegúrate de que el conector esté firmemente conectado.
4. Cliquea Aceptar para cargar la imagen al dispositivo.

Esto podría no funcionar si enciendes la radio con el cable ya conectado Una nueva versión de CHIRP está disponible, ¡Por favor visita el sitio web tan pronto como sea posible para descargarla! Acerca de Acerca de CHIRP Todo Todos los archivos Todos los formatos soportados| Aficionado Ha ocurrido un error Aplicando ajustes Módulos disponibles Plan de banda Bandas Bancos Binario Navegador Construyendo navegador de radio Canada Cambiar este ajuste requiere actualizar los ajustes desde la imagen, lo cual ocurrirá ahora. Canales con TX y RX equivalentes %s son representados por modo de tono de "%s" Archivos de imagen Chirp Elección requerida Elegir %s código DTCS Elegir %s tono Elegir modo cruzado Elegir Dúplex Elige el módulo para cargar desde problema %i: Ciudad Cliquea en el botón para alternar "Canales Especiales" del editor de memoria
para ver/establecer los canales EXT. Los canales P-VFO 100-109
se consideran Ajustes.
Solo un subconjunto de las más de 200 ajustes de radio disponibles
están soportados en este lanzamiento.
Ignora los pitidos de la radio al cargar y descargar.
 Clonado completado, comprobando por bytes espurios Clonando Clonando desde radio Clonando a radio Cerrar Cerrar archivo Agrupar %i memoria Agrupar %i memorias Comentario Comunicarse con la radio Completado Conecta tu cable de interfaz al Puerto de PC en la
espalda de la unidad 'TX/RX'. NO el Puerto Com en la cabeza.
 Convertir a FM Copiar País Modo cruzado Puerto personalizado Personalizado... Cortar DTCS Polaridad
DTCS Decodificación DTMF Memoria DV Peligro adelante Decimal Borrar Modo desarrollador Estado de desarrollador ahora está %s. CHIRP debe ser reiniciado para que tome efecto Diferenciar memorias en bruto Código digital Modos digitales Deshabilitar reportes Desahbilitado Distancia No preguntar de nuevo para %s Doble clic para cambiar nombre de banco Descargar Descargar desde radio Descargar desde radio... Descargar instrucciones Información del controlador Los repetidores digitales de modo dual que soportan análogo se mostrarán como FM Dúplex Editar detalles para %i memorias Editar detalles para memoria %i Habilitar ediciones automáticas Habilitado Ingresar frecuencia Ingresar desplazamiento (MHz) Ingresar frecuencia TX (MHz) Ingresa un nuevo nombre para el banco %s: Ingresar puerto personalizado: Memoria borrada %s Error aplicando ajustes Error comunicándose con la radio Controlador experimental Exportar solo puede escribir archivos CSV Exportar a CSV Exportar a CSV... Extra Radio FM Base de datos de repetidores GRATUITA, la cual proporciona la
información más actualizada sobre de repetidores en Europa. No requiere
cuenta. Fallo al cargar navegador de radio Fallo al analizar el resultado Caracteristicas El archivo no existe: %s Archivos Filtrar Filtrar resultados con ubicación coincidiendo esta cadena Buscar Buscar siguiente Buscar... Trabajos de radio terminados %s Sigue estas instrucciones para descargar la memoria de la radio:
1 - Conecta tu cable de interfaz
2 - Radio > Descargar desde radio: ¡NO te metas con la radio
durante la descarga!
3 - Desconecta tu cable de interfaz
 Sigue estas instrucciones para descargar la memoria de la radio:
1 - Conecta tu cable de interfaz
2 - Radio > Descargar desde radio: ¡No ajustes ningún ajuste
en la cabeza de la radio!
3 - Desconecta tu cable de interfaz
 Sigue estas instrucciones para descargar la memoria de la radio:
1 - Apaga tu radio
2 - Conecta tu cable de interfaz
3 - Enciende tu radio, volumen @ 50%
4 - Menú de CHIRP - Radio > Descargar desde radio
 Sigue estas instrucciones para descargar tu configuración:
1 - Apaga tu radio
2 - Conecta tu cable de interfaz al conector Speaker-2
3 - Enciende tu radio
4 - Radio > Descargar desde radio
5 - ¡Desconecta el cable de interfaz! ¡De lo contrario no habrá
    audio del lado derecho!
 Sigue estas instrucciones para descargar tu información:
1 - Apaga tu radio
2 - Conecta tu cable de interfaz
3 - Enciende tu radio
4 - Haz la descarga de tus datos de radio
 Sigue estas instrucciones para descargar tu radio:
1 - Apaga tu radio
2 - Conecta tu cable de interfaz
3 - Enciende tu radio (desbloquéala si está protegida por contraseña)
4 - Cliquea Aceptar para iniciar
 Sigue estas instrucciones para leer tu radio:
1 - Apaga tu radio
2 - Conecta tu cable de interfaz
3 - Enciende tu radio
4 - Cliquea Aceptar para iniciar
 Sigue estas instrucciones para cargar la memoria de la radio:
1 - Conecta tu cable de interfaz
2 - Radio > Cargar a radio: ¡NO te metas con la radio
durante la carga!
3 - Desconecta tu cable de interfaz
 Sigue estas instrucciones para cargar la memoria de la radio:
1 - Conecta tu cable de interfaz
2 - Radio > Cargar a radio: ¡No ajustes ningún ajuste
en la cabeza de la radio!
3 - Desconecta tu cable de interfaz
 Sigue estas instrucciones para cargar la memoria de la radio:
1 - Apaga tu radio
2 - Conecta tu cable de interfaz
3 - Enciende tu radio, volumen @ 50%
4 - Menú de CHIRP - Radio > Cargar a radio
 Sigue estas instrucciones para cargar tu configuración:
1 - Apaga tu radio
2 - Conecta tu cable de interfaz al conector Speaker-2
3 - Enciende tu radio
4 - Radio > Cargar a radio
5 - ¡Desconecta el cable de interfaz, de otro modo no habrá
    audio del lado derecho!
6 - Apaga y enciende la radio para salir del modo de clonación
 Sigue estas instrucciones para cargar tu información:
1 - Apaga tu radio
2 - Conecta tu cable de interfaz
3 - Enciende tu radio
4 - Haz la carga de tus datos de radio
 Sigue estas instrucciones para cargar tu radio:
1 - Apaga tu radio
2 - Conecta tu cable de interfaz
3 - Enciende tu radio (desbloquéala si está protegida por contraseña)
4 - Cliquea Aceptar para iniciar
 Sigue estas instrucciones para escribir tu radio:
1 - Apaga tu radio
2 - Conecta tu cable de interfaz
3 - Enciende tu radio
4 - Cliquea Aceptar para iniciar
 Sigue estas instrucciones para descargar tu información:
1 - Apaga tu radio
2 - Conecta tu cable de interfaz
3 - Enciende tu radio
4 - Haz la descarga de tus datos de radio
 Sigue estas instrucciones para leer tu radio:
1 - Apaga tu radio
2 - Conecta tu cable de interfaz
3 - Enciende tu radio
4 - Haz la descarga de tus datos de radio
 Sigue estas instrucciones para cargar tu información:
1 - Apaga tu radio
2 - Conecta cable de interfaz
3 - Enciende tu radio
4 - Haz la carga de tus datos de radio
 Sigue estas instrucciones para escribir tu radio:
1 - Apaga tu radio
2 - Conecta tu cable de interfaz
3 - Enciende tu radio
4 - Haz la carga de tus datos de radio
 Encontrado valor de lista vacía para %(name)s: %(value)r Frecuencia GMRS Obteniendo ajustes Ir a memoria Ir a memoria: Ir a... Ayuda Ayúdame... Hexadecimal Ocultar memorias vacías Si se establece, ordenar los resultados por distancia desde estas coordenadas Importar Importar desde archivo... Importación no recomendada Índice Información Información Insertar fila arriba ¿Instalar icono de escritorio? Interactuar con controlador %(value)s inválido (usa grados decimales) Entrada inválida Código postal inválido Edición inválida: %s Archivo de módulo inválido o no soportado Valor inválido: %r Número de problema: EN VIVO Latitud Largo debe ser %i Bandas límite Modos límite Estado límite Limitar resultados a esta distancia (km) desde coordenadas Cargar modulo... Cargar módulo desde problema Cargar módulo desde problema... Cargar módulos puede ser extremadamente peligroso, pudiendo dañar tu computador, radio o ambos. NUNCA cargues un módulo desde una fuente que no confíes, y especialmente desde ningún sitio que no sea el sitio web principal de CHIRP (chirp.danplanet.com). ¡Cargar un módulo desde otra fuente es como darles acceso directo a tu computador y a todo en este! ¿Proceder a pesar de este riesgo? Cargando ajustes Cadena del logotipo 1 (12 caracteres) Cadena del logotipo 2 (12 caracteres) Longitud Memorias La memoria %i no es borrable La memoria debe estar en un banco para ser editada La memoria {num} no está en banco {bank} Modo Modelo Modos Modulo Módulo cargado Módulo cargado exitosamente Más de un puerto encontrado: %s Mover abajo Mover arriba Nueva ventana Nueva versión disponible ¡No hay filas vacías debajo! No se encontraron módulos No se encontraron módulos en problema %i No hay resultados ¡No hay resultados! Numero Desplazamiento Solo ciertas bandas Solo ciertos modos Solo pestañas de memorias pueden ser exportadas Sólo repetidores funcionando Abrir Abrir reciente Abrir configuración original Abrir un archivo Abrir un modulo Abrir registro de depuración Abrir en nueva ventana Abrir directorio de configuración original Opcional: -122.0000 Opcional: 100 Opcional: 45.0000 Opcional: Condado, Hospital, etc. ¿Sobrescribir memorias? Los canales P-VFO 100-109 se consideran Ajustes.
Solo un subconjunto de las más de 130 ajustes de radio disponibles
están soportados en este lanzamiento.
 Analizando Pegar Las memorias pegadas sobrescribirán %s memorias existentes Las memorias pegadas sobrescribirán memorias %s Las memorias pegadas sobrescribirán la memoria %s La memoria pegada sobrescribirá la memoria %s ¡Por favor asegúrate de salir de CHIRP antes de instalar la nueva versión! Por favor sigue estos pasos cuidadosamente:
1 - Enciende tu radio
2 - Conecta el cable de interfaz a tu radio
3 - Cliquea el botón en esta ventana para iniciar la descarga
    (puedes ver otro dialogo, cliquea aceptar)
4 - La radio emitirá un pitido y el led parpadeara
5 - Tendrás un tiempo de espera de 10 segundos para presionar "MON" antes
    de que la carga de datos inicie
6 - Si todo va bien la radio emitirá un pitido al final.
Después de clonar remueve el cable y apaga y enciende tu radio para
entrar en modo normal.
 Por favor sigue estos pasos cuidadosamente:
1 - Enciende tu radio
2 - Conecta el cable de interfaz a tu radio.
3 - Cliquea el botón en esta ventana para iniciar la descarga
    (La radio emitirá un pitido y el led parpadeará)
4 - Entonces presiona el botón "A" en tu radio para iniciar la clonación.
    (Al final la radio emitirá un pitido)
 Por favor te en cuenta que el modo desarrollador está pensado para uso por desarrolladores del proyecto CHIRP, o bajo la dirección de un desarrollador. Esto habilita comportamientos y funciones que pueden dañar tu computador y tu radio si no se usan con EXTREMO cuidado. ¡has sido advertido! ¿Proceder de todos modos? Por favor espera Conecta tu cable y luego haz clic en ACEPTAR Puerto Poder Presiona enter para configurar esto en memoria Previsualización de impresión Imprimiendo Propiedades Consulta %s Consultar fuente RX DTCS Radio La radio no reconoció el bloque %i Información de radio La radio envió datos después del último bloque esperado, esto sucede cuando el modelo seleccionado no es de EE.UU pero la radio es una de EE.UU. Por favor elije el modelo correcto e intenta de nuevo. RadioReference Canadá requiere un inicio de sesión antes de que puedas consultar RadioReference.com es el proveedor de datos
de comunicaciones de radio mas largo del mundo
<small>Requiere cuenta premium</small> Actualización requerida Memoria actualizada %s Recargar controlador Recargar controlador y archivo Renombrar banco RepeaterBook es el directorio GRATUITO de repetidores,
mundial, de Radio Aficionados más completo. Reportes habilitados Reportes ayudan al proyecto CHIRP a saber en cuales modelos de radio y plataformas de SO gastar nuestros limitados esfuerzos. Apreciamos mucho si lo dejas habilitado. ¿Realmente deshabilitar reportes? Reinicio requerido Restaurar %i pestaña Restaurar %i pestañas Ajustes recuperados Guardar ¿Guardar antes de cerrar? Guardar archivo Ajustes guardados Listas de escaneo Codificador Riesgo de seguridad Seleccionar plan de banda... Seleccionar bandas Seleccionar modos Seleccionar plan de banda Servicio Ajustes Mostrar memoria en bruto Mostrar ubicación del registro de depuración Mostrar campos adicionales Algunas memorias son incompatibles con esta radio Algunas memorias no son borrables Ordenar %i memoria Ordenar %i memorias Ordenar %i memoria de forma ascendente Ordenar %i memorias de forma ascendente Ordenar %i memoria de forma descendente Ordenar %i memorias de forma descendente Ordenar por columna: Ordenar memorias Ordenando Estado Estado/Provincia Éxito La red mundial DMR-MARC El controlador de la radio FT-450D carga la pestaña 'Canales Especiales'
con las memorias de rango de escaneo PMS (grupo 11), canales de 60
metros (grupo 12), la memoria QMB (STO/RCL), las HF y
memorias 50m HOME y todas las memorias VFO A y B.
Hay memorias de VFO para la última frecuencia marcada en
cada banda. La última configuración de mem-tune también se almacena.
Estos Canales Especiales permiten una edición de campo limitada.
Este controlador también llena la pestaña 'Otro' en la ventana
de Propiedades de la memoria del canal. Esta pestaña contiene valores para
esos ajustes de memoria de canal que no caen bajo el
estándar de columnas de visualización Chirp. Con el soporte de la FT450, la gui ahora
usa el modo 'DIG' para representar los modos de datos y una nueva columna
'DATA MODE' para seleccionar USER-U, USER-L y RTTY

 El controlador X3Plus es actualmente experimental.
No hay problemas conocidos pero debes proceder con precaución.
Por favor guarda una copia sin editar de tu primera descarga
exitosa a un archivo de imagen de Radio de CHIRP (*.img).
 El autor de este modulo no está reconocido por el desarrollador de CHIRP. Se recomienda que no cargues este modulo ya que podría poner un riesgo de seguridad. ¿Proceder de todos modos? El procedimiento recomendado para importar memorias es para abrir el archivo de origen y copiar/pegar memorias desde este en tu imagen objetivo. Si continúas con esta función de importación, CHIRP remplazara todas las memorias de tu archivo actualmente abierto por las de %(file)s. ¿Te gustaría abrir este archivo para copiar/pegar memorias a través de este, o proceder con la importación? Esta memoria Este controlador ha sido probado con v3 del ID-5100. Si tu radio no está completamente actualizada por favor ayuda abriendo un reporte de error con un registro de depuración para que podamos agregar soporte para las otras revisiones. Este controlador está en desarrollo y debe ser considerado como experimental. Este es un controlador beta de etapa temprana
 Este es un controlador beta de etapa temprana - cargar bajo tu propio riesgo
 Este es un controlador experimental para la Quansheng UV-K5. Este podría dañar tu radio, o algo peor. Úsalo bajo tu propio riesgo.

Antes de intentar hacer algún cambio por favor descarga la imagen de memoria de la radio con chirp y guárdala. Esta se puede usar después para recuperar los ajustes originales.

algunos detalles aun no están implementados Este es un soporte experimental para BJ-9900 que aún está en desarrollo.
Por favor asegúrate de tener un buen respaldo con el software OEM.
¡También por favor envía en solicitudes de errores y mejoras!
Has sido advertido. ¡Procede bajo tu propio riesgo! Esta memoria y desplazar todo hacia arriba Esta memoria y desplazar bloque hacia arriba Esta radio tiene una forma engorrosa de entrar en el modo de programación,
incluso el software original tiene algunos cuantos intentos de entrar.
Lo intentaré 8 veces (la mayoría de las veces ~3 lo harán) y esto
puede tardar unos segundos, si no funciona, inténtelo de nuevo unas cuantas veces.
Si puede ingresar, por favor comprueba la radio y el cable.
 Esto sólo debe habilitarse si utilizas un firmware modificado que soporte una cobertura de frecuencias más amplia. Habilitar esto hará que CHIRP no aplique las restricciones del OEM y puede conducir a un comportamiento indefinido o no regulado. ¡Úsalo bajo tu propio riesgo! Esto va a cargar un módulo desde un problema del sitio web Tono Modo de tono Silenciador de tono Paso de sintonización Buscador de puertos USB No se puede determinar puerto para tu cable. Comprueba tus controladores y conexiones. No se puede editar memoria antes de que la radio este cargada No se puede buscar la configuración original %r No se puede abrir el portapapeles No se puede consultar No se puede leer el último bloque. Esto suele suceder cuando el modelo seleccionado es de EE.UU pero la radio no es una de EE.UU (o de banda ancha). Por favor elije el modelo correcto e intenta de nuevo. No se puede revelar %s en este sistema No se puede establecer %s en esta memoria Estados Unidos Desconecta tu cable (si es necesario) y entonces haz clic en ACEPTAR Cargar instrucciones Cargar a radio Cargar a radio... Memoria cargada %s Usar fuente de ancho fijo Usar fuente más grande Valor no cabe en %i bits Valor debe ser al menos %.4f Valor debe ser como máximo %.4f Valor debe ser exactamente %i dígitos decimales Valor debe ser cero o superior Valores Vendedor Ver ¡ADVERTENCIA! Advertencia Advertencia: %s Bienvenido ¿Quieres que CHIRP instale un icono en el escritorio para ti? Tu cable parece estar en puerto: bits bytes bytes cada deshabilitado habilitado {bank} está lleno 