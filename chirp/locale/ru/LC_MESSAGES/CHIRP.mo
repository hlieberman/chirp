��    j     l  �  �      X  
  Y  -   d  +   �     �     �    �  �   �   �  �!  �   #  �   �#    �$  �   �%    �&  �   �'  �   �(  �   �)  �   �*    �+  o  �,  q  .  �   z/  �   V0  p  ,1  e  �2  �   4  �   �4  �   �5  �   �6  �   �7  �   |8  �   g9  �   O:  	  M;  �   W<  �   =  �   �=  �   �>  �   �?  �   m@  #  KA    oB  M  {C  �   �D  �   �E  �   ;F  �   G    �G    �H  �   �I  �   �J  �   �K  m   "L  �   �L  �   oM  �   YN  �   O  �   P    �P  �   R  �   �R  ^   �S     -T     3T     ?T  	   CT     MT     dT     lT     �T     �T     �T     �T     �T     �T     �T     �T     �T  ]   �T  J   CU     �U     �U     �U     �U     �U     �U  (   �U     V    !V  ,   @W     mW     uW     �W     �W  
   �W     �W     �W     �W  k   �W     >X     CX  
   KX     VX  	   bX     lX     pX     uX  	   �X     �X     �X     �X     �X  A   �X     �X     Y     Y     'Y     0Y     9Y      TY     uY     ~Y     �Y     �Y     �Y     �Y     �Y     �Y     Z     Z     -Z     @Z     YZ     wZ     �Z     �Z     �Z     �Z     �Z     [     [     %[  u   +[     �[     �[     �[     �[     �[     �[  1   \     5\  	   :\     D\     L\  �   b\  �   -]    �]  �   _  �   �_  �   l`  �   a  �   �a  8  �b  �   �c  �   hd  �   !e  �   �e  �   _f  �   g  �   �g  .   Lh  	   {h     �h     �h     �h     �h     �h     �h  
   �h     �h     �h  7   �h     i     #i     7i     Ni     Ti     Yi     ei     vi     �i  '   �i     �i     �i     �i  "   �i     j     .j     <j     Aj     Jj     \j     hj     tj  4   �j     �j     �j     �j  y  �j     pl  	   �l     �l     �l  %   �l     �l     �l     �l      m     m     m     m     6m  	   Sm     ]m  
   em     pm     �m     �m     �m  
   �m     �m     �m     �m     �m     n      n     5n     Ln     Qn     ]n     on     {n     �n     �n     �n     �n     �n     �n      �n     o  �   0o     �o     �o  3   �o  *   �o  (   $p  &   Mp  ?   tp  �  �p  (  yr    �s     �t  $   �t     �t     �t  !   �t     u     "u  
   +u     6u     ?u     Lu     Tu     Zu     uu  �   �u  ;   0v     lv     }v     �v     �v     �v     �v  �   �v     �w     �w     �w     �w  	   �w     �w     �w     �w     x     x     x     1x     9x     Bx     Rx     jx  .   |x     �x     �x     �x     �x     �x     �x     y  �   y  �   �y  ^  �z     �{  �   �{  #   �|  =   �|  �   	}     �}     ~  &  *~  �   Q  ,   @�     m�  	   r�     |�     ��     ��  L   ��  ,   �     �     >�     [�  �   k�  "   �     8�     X�  /   f�     ��     ��     ��     ͂     ��     ��     �     #�     ?�  '   Z�     ��     ��     ��     ��     ��     ��     ă     Ѓ  7   ؃  !   �     2�     7�  
   =�     H�     Q�     Y�  �  h�  �  &�  [   ��  k   Y�     ň     Έ  A  ވ  )   �  ~  J�  .  ɏ  �  ��    ��  �  Δ  �  ��  �  V�  �  �  �  {�  u  ��  �  s�  h  �  M  ��  �  Ԥ  b  Z�  H  ��  b  �  �  i�  �  ��  c  ��  �  �  t  |�  �  �  o  ��  �  �  �  ��  [  a�  W  ��  �  �  e  ��  �  ��  k  ��  *  ��  �  �  j  ��  n  ]�  N  ��  z  �  X  ��  	  ��    ��  �  �  �  ��  %  g�  �   ��  �  ��  �  R�  {  �  �  ��  �  W�  �  �  �  ��  �  ��  �   i�     ��     �     /�     6�  3   H�  -   |�     ��  )   ��     ��     �     �  
   7�     B�     S�     b�     ��  �   ��  s   E�     ��  .   ��     �     (�  &   C�     j�  K   ��  
   ��    ��  f   ��     a�  ,   z�  ,   ��     ��     ��     ��  /   �     B�  �   U�     �     #�     0�  )   F�  #   p�     ��     ��     ��     ��  !   ��     ��     �  #   �  �   >�  G   ��      �  2   8�     k�     ~�  0   ��  [   ��      �  &   3�  )   Z�  *   ��     ��  O   ��  M   �  J   \�     ��     ��  (   ��  )   ��  /   )�  9   Y�  )   ��  6   ��  >   ��  1   3�  a   e�     ��     ��     ��  �   �  D   ��  G   5�     }�  &   ��  
   ��     ��     ��  
   K�     V�     l�  3   z�  {  ��  l  *�  �  ��  [  ��  �  ��  M  �  {  � l  R �  � [  O �  � M  L
 [  � d  � [  [ b  � V       q    � '   � +   � ,   �            ' "   7 3   Z �   �        # ,   C    p    }    � &   � ;   � /   
 Y   : %   � .   � /   � V    %   p    �    �    � /   � .   � %   ) )   O h   y "   � 1    4   7 �  l %   7    ]    l 8   � y   � K   9 
   �    �    �    �    � ,   � 5       8 !   X    z (   � (   �     � 3       5    S 
   r    } 9   � ,   � P   � 2   F    y    � J   �    �     *   ' %   R Y   x !   �    �      >   0  3   o  �   �     �!    �!    �! f   )" d   �" d   �" |   Z# �  �# ]  �' \  
*    g, <   z,    �,    �, G   �,    -    2-    ?-    P-    `-    �-    �- 4   �- &   �- d  �- f   X/ '   �/ /   �/ )   0 5   A0 #   w0 0   �0 �  �0 '   �2 '   �2    �2 1   �2    3 )   33 %   ]3 -   �3 "   �3    �3 *   �3    4    &4 G   94 G   �4 6   �4 b    5 H   c5 +   �5 0   �5    	6    6    -6    I6 R  X6   �7 c  �8      ; �  A; ;   �< {    = �  |= F   B? H   �? ?  �? I  B d   \D    �D    �D    �D    �D    E �   'E z   �E ]   5F :   �F 3   �F n  G G   qH V   �H !   I X   2I %   �I $   �I '   �I 1   �I <   0J =   mJ 7   �J =   �J :   !K _   \K N   �K    L    L    5L    <L    ZL     wL    �L h   �L ?   !M    aM    hM    qM    �M    �M    �M    �   h  3   �     =       ?                 �       Q   �   �       "   �   L  $   z   �           u       �   m       �   <  �       +    3  +   p     J   �   �   �      �   D  .       �   @  X                               �   �       h   �   �       :  ]   s   d    l   q             5       �   H      �   �   B  �   �   b  �      �   �   k   r   o   }       �               �   8  �              �   �       *      �   	      �   �           [   E  f  ]    O   \         n      �   W  �   
      �   X  (   e   `   {   �       W       i           `  �   9  M     P  �             J       5      �       g  T   ~   �       �   t   A  �       2  N      K   �   �   F  \   0   �     U   Q  #      �                   v   �       a   �       [  C   |       �     1   �   @       �   �          G   H           #        j       �           c      T      �       !  �   �   e  �   �       V  �   �   P   �           �   '         K            �           �   '  M  �      �          �   �   f   4  S         �   )           �   �   �       %  x   U      �   L   /  �        R      g              �          <   �   �       �   
   (      a     Z   �   �   ;  �       *          �       �   >       -   �   �     ^  Y   �         �   R      y   �   �   �                   6   �   .  �   �   ,  :   	   7   �   D   _           9   !          �         �       �   �   �                  "  I  �   �   ?           %     d   )  w      �   S           �      �   4   O        >      F   Z            -  ,   7  �              �   I   1  �   ;   b   �          �          �       �   �         �   �   8   &     2   �          �         �   G  j  �   0  c           B   ^   �   A   6     =  $  i      Y  &   _  /   �   N     �   C      �       V       E       �           �   �            1. Turn radio off.
 2. Connect cable to DATA terminal.
 3. Press and hold in [DISP] key while turning on radio
      ("CLONE" will appear on radio LCD).
 4. Press [RECEIVE] screen button
      ("-WAIT-" will appear on radio LCD).
5. Finally, press OK button below.
 %(value)s must be between %(min)i and %(max)i %s has not been saved. Save before closing? (none) ...and %i more 1. Ensure your firmware version is 4_10 or higher
2. Turn radio off
3. Connect your interface cable
4. Turn radio on
5. Press and release PTT 3 times while holding MONI key
6. Supported baud rates: 57600 (default) and 19200
   (rotate dial while holding MONI to change)
7. Click OK
 1. Turn Radio off.
2. Connect data cable.
3. While holding "A/N LOW" button, turn radio on.
4. <b>After clicking OK</b>, press "SET MHz" to send image.
 1. Turn Radio off.
2. Connect data cable.
3. While holding "A/N LOW" button, turn radio on.
4. Press "MW D/MR" to receive image.
5. Make sure display says "-WAIT-" (see note below if not)
6. Click OK to dismiss this dialog and start transfer.
Note: if you don't see "-WAIT-" at step 5, try cycling
      power and pressing and holding red "*L" button to unlock
      radio, then start back at step 1.
 1. Turn Radio off.
2. Connect data cable.
3. While holding "TONE" and "REV" buttons, turn radio on.
4. <b>After clicking OK</b>, press "TONE" to send image.
 1. Turn Radio off.
2. Connect data cable.
3. While holding "TONE" and "REV" buttons, turn radio on.
4. Press "REV" to receive image.
5. Make sure display says "CLONE RX" and green led is blinking
6. Click OK to start transfer.
 1. Turn radio off.
2. Connect cable
3. Press and hold in the MHz, Low, and D/MR keys on the radio while turning it on
4. Radio is in clone mode when TX/RX is flashing
5. <b>After clicking OK</b>, press the MHz key on the radio to send image.
    ("TX" will appear on the LCD). 
 1. Turn radio off.
2. Connect cable
3. Press and hold in the MHz, Low, and D/MR keys on the radio while turning it on
4. Radio is in clone mode when TX/RX is flashing
5. Press the Low key on the radio ("RX" will appear on the LCD).
6. Click OK. 1. Turn radio off.
2. Connect cable to ACC jack.
3. Press and hold in the [MODE &lt;] and [MODE &gt;] keys while
     turning the radio on ("CLONE MODE" will appear on the
     display).
4. <b>After clicking OK</b> here, press the [C.S.] key to
    send image.
 1. Turn radio off.
2. Connect cable to ACC jack.
3. Press and hold in the [MODE &lt;] and [MODE &gt;] keys while
     turning the radio on ("CLONE MODE" will appear on the
     display).
4. <b>After clicking OK</b>, press the [A] key to send image.
 1. Turn radio off.
2. Connect cable to ACC jack.
3. Press and hold in the [MODE &lt;] and [MODE &gt;] keys while
     turning the radio on ("CLONE MODE" will appear on the
     display).
4. Click OK here.
    ("Receiving" will appear on the LCD).
 1. Turn radio off.
2. Connect cable to ACC jack.
3. Press and hold in the [MODE &lt;] and [MODE &gt;] keys while
     turning the radio on ("CLONE MODE" will appear on the
     display).
4. Press the [A](RCV) key ("receiving" will appear on the LCD).
 1. Turn radio off.
2. Connect cable to ACC jack.
3. Press and hold in the [MODE &lt;] and [MODE &gt;] keys while
     turning the radio on ("CLONE MODE" will appear on the
     display).
4. Press the [C] key ("RX" will appear on the LCD).
 1. Turn radio off.
2. Connect cable to CAT/LINEAR jack.
3. Press and hold in the [MODE &lt;] and [MODE &gt;] keys while
     turning the radio on ("CLONE MODE" will appear on the
     display).
4. <b>After clicking OK</b>,
     press the [C](SEND) key to send image.
 1. Turn radio off.
2. Connect cable to DATA jack.
3. Press and hold in the "left" [V/M] key while turning the
     radio on.
4. Rotate the "right" DIAL knob to select "CLONE START".
5. Press the [SET] key. The display will disappear
     for a moment, then the "CLONE" notation will appear.
6. <b>After clicking OK</b>, press the "left" [V/M] key to
     send image.
 1. Turn radio off.
2. Connect cable to DATA jack.
3. Press and hold in the "left" [V/M] key while turning the
     radio on.
4. Rotate the "right" DIAL knob to select "CLONE START".
5. Press the [SET] key. The display will disappear
     for a moment, then the "CLONE" notation will appear.
6. Press the "left" [LOW] key ("CLONE -RX-" will appear on
     the display).
 1. Turn radio off.
2. Connect cable to DATA jack.
3. Press and hold in the [FW] key while turning the radio on
     ("CLONE" will appear on the display).
4. <b>After clicking OK</b>, press the [BAND] key to send image.
 1. Turn radio off.
2. Connect cable to DATA jack.
3. Press and hold in the [FW] key while turning the radio on
     ("CLONE" will appear on the display).
4. Press the [MODE] key ("-WAIT-" will appear on the LCD).
 1. Turn radio off.
2. Connect cable to DATA jack.
3. Press and hold in the [MHz(PRI)] key while turning the
     radio on.
4. Rotate the DIAL job to select "F-7 CLONE".
5. Press and hold in the [BAND(SET)] key. The display
     will disappear for a moment, then the "CLONE" notation
     will appear.
6. Press the [LOW(ACC)] key ("--RX--" will appear on the display).
 1. Turn radio off.
2. Connect cable to DATA jack.
3. Press and hold in the [MHz(PRI)] key while turning the
 radio on.
4. Rotate the DIAL job to select "F-7 CLONE".
5. Press and hold in the [BAND(SET)] key. The display
 will disappear for a moment, then the "CLONE" notation
 will appear.
6. <b>After clicking OK</b>, press the [V/M(MW)] key to send image.
 1. Turn radio off.
2. Connect cable to DATA terminal.
3. Press and hold [DISP] key while turning on radio
     ("CLONE" will appear on the display).
4. <b>After clicking OK here in chirp</b>,
     press the [Send] screen button.
 1. Turn radio off.
2. Connect cable to DATA terminal.
3. Press and hold in the [F] key while turning the radio on
     ("CLONE" will appear on the display).
4. <b>After clicking OK</b>, press the [BAND] key to send image.
 1. Turn radio off.
2. Connect cable to DATA terminal.
3. Press and hold in the [F] key while turning the radio on
     ("CLONE" will appear on the display).
4. Press the [Dx] key ("-WAIT-" will appear on the LCD).
 1. Turn radio off.
2. Connect cable to DATA terminal.
3. Press and hold in the [MHz(SETUP)] key while turning the radio
     on ("CLONE" will appear on the display).
4. <b>After clicking OK</b>, press the [REV(DW)] key
     to send image.
 1. Turn radio off.
2. Connect cable to DATA terminal.
3. Press and hold in the [MHz(SETUP)] key while turning the radio
     on ("CLONE" will appear on the display).
4. Press the [MHz(SETUP)] key
     ("-WAIT-" will appear on the LCD).
 1. Turn radio off.
2. Connect cable to MIC Jack.
3. Press and hold in the [MHz(SETUP)] key while turning the radio
     on ("CLONE" will appear on the display).
4. <b>After clicking OK</b>, press the [GM(AMS)] key
     to send image.
 1. Turn radio off.
2. Connect cable to MIC Jack.
3. Press and hold in the [MHz(SETUP)] key while turning the radio
     on ("CLONE" will appear on the display).
4. Press the [MHz(SETUP)] key
     ("-WAIT-" will appear on the LCD).
 1. Turn radio off.
2. Connect cable to MIC/EAR jack.
3. Press and hold in the [F/W] key while turning the radio on
    ("CLONE" will appear on the display).
4. <b>After clicking OK</b>, press the [VFO(DW)SC] key to receive
    the image from the radio.
 1. Turn radio off.
2. Connect cable to MIC/EAR jack.
3. Press and hold in the [F/W] key while turning the radio on
    ("CLONE" will appear on the display).
4. Press the [MR(SKP)SC] key ("CLONE WAIT" will appear
    on the LCD).
5. Click OK to send image to radio.
 1. Turn radio off.
2. Connect cable to MIC/SP jack.
3. Press and hold [PTT] &amp; Knob while turning the
     radio on.
4. <b>After clicking OK</b>, press the [PTT] switch to send image.
 1. Turn radio off.
2. Connect cable to MIC/SP jack.
3. Press and hold [PTT] &amp; Knob while turning the
     radio on.
4. Press the [MONI] switch ("WAIT" will appear on the LCD).
5. Press OK.
 1. Turn radio off.
2. Connect cable to MIC/SP jack.
3. Press and hold in the [F/W] key while turning the radio on
     ("CLONE" will appear on the display).
4. <b>After clicking OK</b>, press the [BAND] key to send image.
 1. Turn radio off.
2. Connect cable to MIC/SP jack.
3. Press and hold in the [F/W] key while turning the radio on
     ("CLONE" will appear on the display).
4. Press the [V/M] key ("-WAIT-" will appear on the LCD).
 1. Turn radio off.
2. Connect cable to MIC/SP jack.
3. Press and hold in the [MON-F] key while turning the radio on
     ("CLONE" will appear on the display).
4. <b>After clicking OK</b>, press the [BAND] key to send image.
 1. Turn radio off.
2. Connect cable to MIC/SP jack.
3. Press and hold in the [MON-F] key while turning the radio on
     ("CLONE" will appear on the display).
4. Press the [V/M] key ("CLONE WAIT" will appear on the LCD).
 1. Turn radio off.
2. Connect cable to MIC/SP jack.
3. Press and hold in the [MONI] switch while turning the
     radio on.
4. Rotate the DIAL job to select "F8 CLONE".
5. Press the [F/W] key momentarily.
6. <b>After clicking OK</b>, hold the [PTT] switch
     for one second to send image.
 1. Turn radio off.
2. Connect cable to MIC/SP jack.
3. Press and hold in the [MONI] switch while turning the
     radio on.
4. Rotate the DIAL job to select "F8 CLONE".
5. Press the [F/W] key momentarily.
6. Press the [MONI] switch ("--RX--" will appear on the LCD).
 1. Turn radio off.
2. Connect cable to MIC/SP jack.
3. Press and hold in the [moni] key while turning the radio on.
4. Select CLONE in menu, then press F. Radio restarts in clone mode.
     ("CLONE" will appear on the display).
5. <b>After clicking OK</b>, briefly hold [PTT] key to send image.
    ("-TX-" will appear on the LCD). 
 1. Turn radio off.
2. Connect cable to mic jack.
3. Press and hold in the [LOW(A/N)] key while turning the radio on.
4. <b>After clicking OK</b>, press the [MHz(SET)] key to send image.
 1. Turn radio off.
2. Connect cable to mic jack.
3. Press and hold in the [LOW(A/N)] key while turning the radio on.
4. Press the [D/MR(MW)] key ("--WAIT--" will appear on the LCD).
 1. Turn radio off.
2. Connect cable to mic jack.
3. Press and hold in the [MHz], [LOW], and [D/MR] keys
   while turning the radio on.
4. <b>After clicking OK</b>, press the [MHz(SET)] key to send image.
 1. Turn radio off.
2. Connect cable to mic jack.
3. Press and hold in the [MHz], [LOW], and [D/MR] keys
   while turning the radio on.
4. Press the [D/MR(MW)] key ("--WAIT--" will appear on the LCD).
 1. Turn radio off.
2. Connect cable to mic/spkr connector.
3. Make sure connector is firmly connected.
4. Turn radio on (volume may need to be set at 100%).
5. Ensure that the radio is tuned to channel with no activity.
6. Click OK to download image from device.
 1. Turn radio off.
2. Connect cable to mic/spkr connector.
3. Make sure connector is firmly connected.
4. Turn radio on (volume may need to be set at 100%).
5. Ensure that the radio is tuned to channel with no activity.
6. Click OK to upload image to device.
 1. Turn radio off.
2. Connect cable to mic/spkr connector.
3. Make sure connector is firmly connected.
4. Turn radio on.
5. Ensure that the radio is tuned to channel with no activity.
6. Click OK to download image from device.
 1. Turn radio off.
2. Connect cable to mic/spkr connector.
3. Make sure connector is firmly connected.
4. Turn radio on.
5. Ensure that the radio is tuned to channel with no activity.
6. Click OK to upload image to device.
 1. Turn radio off.
2. Connect data cable.
3. Prepare radio for clone.
4. <b>After clicking OK</b>, press the key to send image.
 1. Turn radio off.
2. Connect data cable.
3. Prepare radio for clone.
4. Press the key to receive the image.
 1. Turn radio off.
2. Connect mic and hold [ACC] on mic while powering on.
    ("CLONE" will appear on the display)
3. Replace mic with PC programming cable.
4. <b>After clicking OK</b>, press the [SET] key to send image.
 1. Turn radio off.
2. Connect mic and hold [ACC] on mic while powering on.
    ("CLONE" will appear on the display)
3. Replace mic with PC programming cable.
4. Press the [DISP/SS] key
    ("R" will appear on the lower left of LCD).
 1. Turn radio off.
2. Remove front head.
3. Connect data cable to radio, use the same connector where
     head was connected to, <b>not the mic connector</b>.
4. Click OK.
 1. Turn radio off.
3. Press and hold in the [moni] key while turning the radio on.
4. Select CLONE in menu, then press F. Radio restarts in clone mode.
     ("CLONE" will appear on the display).
5. Press the [moni] key ("-RX-" will appear on the LCD).
 1. Turn radio on.
2. Connect cable to DATA terminal.
3. Unclip battery.
4. Press and hold in the [AMS] key and power key while clipping 
 in back battery the("ADMS" will appear on the display).
5. <b>After clicking OK</b>, press the [BAND] key.
 1. Turn radio on.
2. Connect cable to DATA terminal.
3. Unclip battery.
4. Press and hold in the [AMS] key and power key while clipping 
 in back battery the("ADMS" will appear on the display).
5. Press the [MODE] key ("-WAIT-" will appear on the LCD). OK</b>
 click <b>Then 1. Turn radio on.
2. Connect cable to mic/spkr connector.
3. Make sure connector is firmly connected.
4. Click OK to download image from device.

It will may not work if you turn on the radio with the cable already attached
 1. Turn radio on.
2. Connect cable to mic/spkr connector.
3. Make sure connector is firmly connected.
4. Click OK to upload the image to device.

It will may not work if you turn on the radio with the cable already attached A new CHIRP version is available. Please visit the website as soon as possible to download it! About About CHIRP All All Files All supported formats| Amateur An error has occurred Applying settings Available modules Bandplan Bands Banks Bin Browser Building Radio Browser Canada Changing this setting requires refreshing the settings from the image, which will happen now. Channels with equivalent TX and RX %s are represented by tone mode of "%s" Chirp Image Files Choice Required Choose %s DTCS Code Choose %s Tone Choose Cross Mode Choose duplex Choose the module to load from issue %i: City Click on the "Special Channels" toggle-button of the memory
editor to see/set the EXT channels. P-VFO channels 100-109
are considered Settings.
Only a subset of the over 200 available radio settings
are supported in this release.
Ignore the beeps from the radio on upload and download.
 Clone completed, checking for spurious bytes Cloning Cloning from radio Cloning to radio Close Close file Comment Communicate with radio Complete Connect your interface cable to the PC Port on the
back of the 'TX/RX' unit. NOT the Com Port on the head.
 Copy Country Cross mode Custom Port Custom... Cut DTCS DTCS
Polarity DV Memory Danger Ahead Dec Delete Developer Mode Developer state is now %s. CHIRP must be restarted to take effect Diff Raw Memories Digital Code Disable reporting Disabled Distance Do not prompt again for %s Double-click to change bank name Download Download from radio Download from radio... Download instructions Duplex Edit details for %i memories Edit details for memory %i Enable Automatic Edits Enabled Enter Frequency Enter Offset (MHz) Enter TX Frequency (MHz) Enter a new name for bank %s: Enter custom port: Erased memory %s Error applying settings Error communicating with radio Experimental driver Export can only write CSV files Export to CSV Export to CSV... Extra FREE repeater database, which provides most up-to-date
information about repeaters in Europe. No account is
required. Failed to load radio browser Failed to parse result Features File does not exist: %s Files Filter Filter results with location matching this string Find Find Next Find... Finished radio job %s Follow these instructions to download the radio memory:
1 - Connect your interface cable
2 - Radio > Download from radio: DO NOT mess with the radio
during download!
3 - Disconnect your interface cable
 Follow these instructions to download the radio memory:
1 - Connect your interface cable
2 - Radio > Download from radio: Don't adjust any settings
on the radio head!
3 - Disconnect your interface cable
 Follow these instructions to download your config:
1 - Turn off your radio
2 - Connect your interface cable to the Speaker-2 jack
3 - Turn on your radio
4 - Radio > Download from radio
5 - Disconnect the interface cable! Otherwise there will be
    no right-side audio!
 Follow these instructions to download your info:
1 - Turn off your radio
2 - Connect your interface cable
3 - Turn on your radio
4 - Do the download of your radio data
 Follow these instructions to download your radio:
1 - Turn off your radio
2 - Connect your interface cable
3 - Turn on your radio (unlock it if password protected)
4 - Click OK to start
 Follow these instructions to read your radio:
1 - Turn off your radio
2 - Connect your interface cable
3 - Turn on your radio
4 - Click OK to start
 Follow these instructions to upload the radio memory:
1 - Connect your interface cable
2 - Radio > Upload to radio: DO NOT mess with the radio
during upload!
3 - Disconnect your interface cable
 Follow these instructions to upload the radio memory:
1 - Connect your interface cable
2 - Radio > Upload to radio: Don't adjust any settings
on the radio head!
3 - Disconnect your interface cable
 Follow these instructions to upload your config:
1 - Turn off your radio
2 - Connect your interface cable to the Speaker-2 jack
3 - Turn on your radio
4 - Radio > Upload to radio
5 - Disconnect the interface cable, otherwise there will be
    no right-side audio!
6 - Cycle power on the radio to exit clone mode
 Follow these instructions to upload your info:
1 - Turn off your radio
2 - Connect your interface cable
3 - Turn on your radio
4 - Do the upload of your radio data
 Follow these instructions to upload your radio:
1 - Turn off your radio
2 - Connect your interface cable
3 - Turn on your radio (unlock it if password protected)
4 - Click OK to start
 Follow these instructions to write your radio:
1 - Turn off your radio
2 - Connect your interface cable
3 - Turn on your radio
4 - Click OK to start
 Follow this instructions to download your info:
1 - Turn off your radio
2 - Connect your interface cable
3 - Turn on your radio
4 - Do the download of your radio data
 Follow this instructions to read your radio:
1 - Turn off your radio
2 - Connect your interface cable
3 - Turn on your radio
4 - Do the download of your radio data
 Follow this instructions to upload your info:
1 - Turn off your radio
2 - Connect your interface cable
3 - Turn on your radio
4 - Do the upload of your radio data
 Follow this instructions to write your radio:
1 - Turn off your radio
2 - Connect your interface cable
3 - Turn on your radio
4 - Do the upload of your radio data
 Found empty list value for %(name)s: %(value)r Frequency GMRS Getting settings Goto Memory Goto Memory: Goto... Help Help Me... Hex Hide empty memories If set, sort results by distance from these coordinates Import Import from file... Import not recommended Index Info Information Insert Row Above Install desktop icon? Interact with driver Invalid %(value)s (use decimal degrees) Invalid Entry Invalid ZIP code Invalid edit: %s Invalid or unsupported module file Invalid value: %r Issue number: LIVE Latitude Length must be %i Limit Bands Limit Modes Limit Status Limit results to this distance (km) from coordinates Load Module... Load module from issue Load module from issue... Loading modules can be extremely dangerous, leading to damage to your computer, radio, or both. NEVER load a module from a source you do not trust, and especially not from anywhere other than the main CHIRP website (chirp.danplanet.com). Loading a module from another source is akin to giving them direct access to your computer and everything on it! Proceed despite this risk? Loading settings Longitude Memories Memory %i is not deletable Memory must be in a bank to be edited Memory {num} not in bank {bank} Mode Model Modes Module Module Loaded Module loaded successfully More than one port found: %s Move Down Move Up New Window New version available No empty rows below! No modules found No modules found in issue %i No results No results! Number Offset Only certain bands Only certain modes Only memory tabs may be exported Only working repeaters Open Open Recent Open Stock Config Open a file Open a module Open debug log Open in new window Open stock config directory Optional: -122.0000 Optional: 100 Optional: 45.0000 Optional: County, Hospital, etc. Overwrite memories? P-VFO channels 100-109 are considered Settings.
Only a subset of the over 130 available radio settings
are supported in this release.
 Parsing Paste Pasted memories will overwrite %s existing memories Pasted memories will overwrite memories %s Pasted memories will overwrite memory %s Pasted memory will overwrite memory %s Please be sure to quit CHIRP before installing the new version! Please follow this steps carefully:
1 - Turn on your radio
2 - Connect the interface cable to your radio
3 - Click the button on this window to start download
    (you may see another dialog, click ok)
4 - Radio will beep and led will flash
5 - You will get a 10 seconds timeout to press "MON" before
    data upload start
6 - If all goes right radio will beep at end.
After cloning remove the cable and power cycle your radio to
get into normal mode.
 Please follow this steps carefully:
1 - Turn on your radio
2 - Connect the interface cable to your radio.
3 - Click the button on this window to start download
    (Radio will beep and led will flash)
4 - Then press the "A" button in your radio to start cloning.
    (At the end radio will beep)
 Please note that developer mode is intended for use by developers of the CHIRP project, or under the direction of a developer. It enables behaviors and functions that can damage your computer and your radio if not used with EXTREME care. You have been warned! Proceed anyway? Please wait Plug in your cable and then click OK Port Power Press enter to set this in memory Print Preview Printing Properties Query %s Query Source RX DTCS Radio Radio did not ack block %i Radio information Radio sent data after the last awaited block, this happens when the selected model is a non-US but the radio is a US one. Please choose the correct model and try again. RadioReference Canada requires a login before you can query Refresh required Refreshed memory %s Reload Driver Reload Driver and File Rename bank Reporting enabled Reporting helps the CHIRP project know which radio models and OS platforms to spend our limited efforts on. We would really appreciate if you left it enabled. Really disable reporting? Restart Required Retrieved settings Save Save before closing? Save file Saved settings Security Risk Select Bandplan... Select Bands Select Modes Select a bandplan Service Settings Show Raw Memory Show debug log location Show extra fields Some memories are incompatible with this radio Some memories are not deletable Sort by column: Sort memories Sorting State State/Province Success The X3Plus driver is currently experimental.
There are no known issues but you should proceed with caution.
Please save an unedited copy of your first successful
download to a CHIRP Radio Images (*.img) file.
 The author of this module is not a recognized CHIRP developer. It is recommended that you not load this module as it could pose a security risk. Proceed anyway? The recommended procedure for importing memories is to open the source file and copy/paste memories from it into your target image. If you continue with this import function, CHIRP will replace all memories in your currently-open file with those in %(file)s. Would you like to open this file to copy/paste memories across, or proceed with the import? This Memory This driver has been tested with v3 of the ID-5100. If your radio is not fully updated please help by opening a bug report with a debug log so we can add support for the other revisions. This is an early stage beta driver
 This is an early stage beta driver - upload at your own risk
 This is experimental support for BJ-9900 which is still under development.
Please ensure you have a good backup with OEM software.
Also please send in bug and enhancement requests!
You have been warned. Proceed at your own risk! This memory and shift all up This memory and shift block up This radio has a tricky way of enter into program mode,
even the original software has a few tries to get inside.
I will try 8 times (most of the time ~3 will doit) and this
can take a few seconds, if don't work, try again a few times.
If you can get into it, please check the radio and cable.
 This should only be enabled if you are using modified firmware that supports wider frequency coverage. Enabling this will cause CHIRP not to enforce OEM restrictions and may lead to undefined or unregulated behavior. Use at your own risk! This will load a module from a website issue Tone Tone Mode Tone Squelch Tuning Step USB Port Finder Unable to determine port for your cable. Check your drivers and connections. Unable to edit memory before radio is loaded Unable to find stock config %r Unable to open the clipboard Unable to query Unable to read last block. This often happens when the selected model is US but the radio is a non-US one (or widebanded). Please choose the correct model and try again. Unable to reveal %s on this system Unable to set %s on this memory United States Unplug your cable (if needed) and then click OK Upload instructions Upload to radio Upload to radio... Uploaded memory %s Use fixed-width font Use larger font Value does not fit in %i bits Value must be at least %.4f Value must be at most %.4f Value must be exactly %i decimal digits Value must be zero or greater Values Vendor View WARNING! Warning Warning: %s Welcome Would you like CHIRP to install a desktop icon for you? Your cable appears to be on port: bits bytes bytes each disabled enabled {bank} is full Project-Id-Version: CHIRP
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-10-11 16:47+0300
Last-Translator: Olesya Gerasimenko <translation-team@basealt.ru>
Language-Team: Basealt Translation Team
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 23.04.3
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
  1. Выключите станцию.
 2. Подключите кабель к терминалу DATA.
 3. Нажмите и удерживайте клавишу [DISP] во время включения станции
      (на экране станции появится «CLONE»).
 4. Нажмите экранную кнопку [RECEIVE]
      (на экране станции появится «-WAIT-»).
5. Нажмите кнопку «ОК» ниже.
 %(value)s должно находиться в диапазоне от %(min)i до %(max)i Сохранение %s не было выполнено. Сохранить перед закрытием? (нет) ...и ещё %i 1. Убедитесь, что у вас микропрограмма версии 4_10 или выше
2. Выключите станцию
3. Подключите ваш интерфейсный кабель
4. Включите станцию
5. Нажмите и отпустите PTT 3 раза, удерживая клавишу MONI
6. Поддерживаемая скорость в бодах: 57600 (по умолчанию) и 19200
   (чтобы изменить, поверните ручку настройки, удерживая MONI)
7. Нажмите «OK»
 1. Выключите станцию.
2. Подключите кабель передачи данных.
3. Удерживая кнопку «A/N LOW», включите станцию.
4. <b>После нажатия «ОК»</b> нажмите «SET MHz» для отправки образа.
 1. Выключите станцию.
2. Подключите кабель передачи данных.
3. Удерживая кнопку «A/N LOW», включите станцию.
4. Нажмите «MW D/MR» для получения образа.
5. Убедитесь, что на экране написано «-WAIT-» (см. примечание ниже, если это не так)
6. Нажмите «OK» для закрытия этого диалога и начала передачи.
Примечание: если на шаге 5 на экране нет текста «-WAIT-», попробуйте выключить
      и снова включить питание, потом нажать и удерживать красную кнопку «*L» для
       разблокировки станции, а затем начните снова с шага 1.
 1. Выключите станцию.
2. Подключите кабель передачи данных.
3. Удерживая кнопки «TONE» и «REV», включите станцию.
4. <b>После нажатия «ОК»</b> нажмите «TONE» для отправки образа.
 1. Выключите станцию.
2. Подключите кабель передачи данных.
3. Удерживая кнопки «TONE» и «REV», включите станцию.
4. Нажмите «REV» для получения образа.
5. Убедитесь, что на экране написано «CLONE RX» и мигает зелёный светодиод.
6. Нажмите «ОК» для начала передачи.
 1. Выключите станцию.
2. Подключите кабель.
3. Нажмите и удерживайте на станции клавиши «MHz», «Low» и «D/MR» во время
её включения.
4. Станция находится в режиме клонирования, когда мигает TX/RX.
5. <b>После нажатия «ОК»</b> нажмите на станции клавишу «MHz» для отправки образа
    (на экране появится «TX»). 
 1. Выключите станцию.
2. Подключите кабель.
3. Нажмите и удерживайте на станции клавиши «MHz», «Low» и «D/MR» во время
её включения.
4. Станция находится в режиме клонирования, когда мигает TX/RX.
5. Нажмите на станции клавишу «Low» (на экране появится «RX»).
6. Нажмите «OK». 1. Выключите станцию.
2. Подключите кабель к разъёму ACC.
3. Нажмите и удерживайте клавиши [MODE &lt;] и [MODE &gt;] во время
    включения станции (на экране появится «CLONE MODE»).
4. <b>После нажатия «ОК»</b> в этом окне нажмите клавишу [C.S.] для
    отправки образа.
 1. Выключите станцию.
2. Подключите кабель к разъёму ACC.
3. Нажмите и удерживайте клавиши [MODE &lt;] и [MODE &gt;] во время
    включения станции (на экране появится «CLONE MODE»).
4. <b>После нажатия «ОК»</b> нажмите клавишу [A] для
    отправки образа.
 1. Выключите станцию.
2. Подключите кабель к разъёму ACC.
3. Нажмите и удерживайте клавиши [MODE &lt;] и [MODE &gt;] во время
    включения станции (на экране появится «CLONE MODE»).
4. Нажмите «ОК» в этом окне (на экране появится «Receiving»).
 1. Выключите станцию.
2. Подключите кабель к разъёму ACC.
3. Нажмите и удерживайте клавиши [MODE &lt;] и [MODE &gt;] во время
    включения станции (на экране появится «CLONE MODE»).
4. Нажмите клавишу [A](RCV) (на экране появится «receiving»).
 1. Выключите станцию.
2. Подключите кабель к разъёму ACC.
3. Нажмите и удерживайте клавиши [MODE &lt;] и [MODE &gt;] во время
    включения станции (на экране появится «CLONE MODE»).
4. Нажмите клавишу [C] (на экране появится «RX»).
 1. Выключите станцию.
2. Подключите кабель к разъёму CAT/LINEAR.
3. Нажмите и удерживайте клавиши [MODE &lt;] и [MODE &gt;] во время
    включения станции (на экране появится «CLONE MODE»).
4. <b>После нажатия «ОК»</b> нажмите клавишу [C](SEND) для
    отправки образа.
 1. Выключите станцию.
2. Подключите кабель к разъёму DATA.
3. Нажмите и удерживайте «левую» клавишу [V/M] во время
    включения станции.
4. Поверните «правую» ручку DIAL для выбора «CLONE START».
5. Нажмите клавишу [SET]. Экран на время станет пустым,
    затем появится надпись «CLONE».
6. <b>После нажатия «ОК»</b> нажмите «левую» клавишу [V/M]
    для отправки образа.
 1. Выключите станцию.
2. Подключите кабель к разъёму DATA.
3. Нажмите и удерживайте «левую» клавишу [V/M] во время
    включения станции.
4. Поверните «правую» ручку DIAL для выбора «CLONE START».
5. Нажмите клавишу [SET]. Экран на время станет пустым,
    затем появится надпись «CLONE».
6. Нажмите «левую» клавишу [LOW] (на экране появится
    «CLONE -RX-»).
 1. Выключите станцию.
2. Подключите кабель к разъёму DATA.
3. Нажмите и удерживайте клавишу [FW] во время
    включения станции (на экране появится «CLONE»).
4. <b>После нажатия «ОК»</b> нажмите клавишу [BAND]
    для отправки образа.
 1. Выключите станцию.
2. Подключите кабель к разъёму DATA.
3. Нажмите и удерживайте клавишу [FW] во время
    включения станции (на экране появится «CLONE»).
4. Нажмите клавишу [MODE] (на экране появится «-WAIT-»).
 1. Выключите станцию.
2. Подключите кабель к разъёму DATA.
3. Нажмите и удерживайте клавишу [MHz(PRI)] во время включения
     станции.
4. Поверните регулятор DIAL для выбора «F-7 CLONE».
5. Нажмите и удерживайте клавишу [BAND(SET)]. Экран на время
     станет пустым, затем появится надпись «CLONE».
6. Нажмите клавишу [LOW(ACC)] (на экране появится «--RX--»).
 1. Выключите станцию.
2. Подключите кабель к разъёму DATA.
3. Нажмите и удерживайте клавишу [MHz(PRI)] во время включения
 станции.
4. Поверните регулятор DIAL для выбора «F-7 CLONE».
5. Нажмите и удерживайте клавишу [BAND(SET)]. Экран на время
 станет пустым, затем появится надпись «CLONE».
6. <b>После нажатия «ОК»</b> нажмите клавишу [V/M(MW)] для
    отправки образа.
 1. Выключите станцию.
2. Подключите кабель к терминалу DATA.
3. Нажмите и удерживайте клавишу [DISP] во время
    включения станции (на экране появится «CLONE»).
4. <b>После нажатия «ОК» в этом окне chirp</b>
    нажмите экранную кнопку [Send].
 1. Выключите станцию.
2. Подключите кабель к терминалу DATA.
3. Нажмите и удерживайте клавишу [F] во время
    включения станции (на экране появится «CLONE»).
4. <b>После нажатия «ОК»</b> нажмите клавишу [BAND]
    для отправки образа.
 1. Выключите станцию.
2. Подключите кабель к терминалу DATA.
3. Нажмите и удерживайте клавишу [F] во время
    включения станции (на экране появится «CLONE»).
4. Нажмите клавишу [Dx] (на экране появится «-WAIT-»).
 1. Выключите станцию.
2. Подключите кабель к терминалу DATA.
3. Нажмите и удерживайте клавишу [MHz(SETUP)] во время
    включения станции (на экране появится «CLONE»).
4. <b>После нажатия «ОК»</b> нажмите клавишу [REV(DW)]
    для отправки образа.
 1. Выключите станцию.
2. Подключите кабель к терминалу DATA.
3. Нажмите и удерживайте клавишу [MHz(SETUP)] во время
    включения станции (на экране появится «CLONE»).
4. Нажмите клавишу [MHz(SETUP)] (на экране появится «-WAIT-»).
 1. Выключите станцию.
2. Подключите кабель к разъёму MIC.
3. Нажмите и удерживайте клавишу [MHz(SETUP)] во время
    включения станции (на экране появится «CLONE»).
4. <b>После нажатия «ОК»</b> нажмите клавишу [GM(AMS)]
    для отправки образа.
 1. Выключите станцию.
2. Подключите кабель к разъёму MIC.
3. Нажмите и удерживайте клавишу [MHz(SETUP)] во время
    включения станции (на экране появится «CLONE»).
4. Нажмите клавишу [MHz(SETUP)] (на экране появится «-WAIT-»).
 1. Выключите станцию.
2. Подключите кабель к разъёму MIC/EAR.
3. Нажмите и удерживайте клавишу [F/W] во время
    включения станции (на экране появится «CLONE»).
4. <b>После нажатия «ОК»</b> нажмите клавишу
      [VFO(DW)SC] для получения образа от станции.
 1. Выключите станцию.
2. Подключите кабель к разъёму MIC/EAR.
3. Нажмите и удерживайте клавишу [F/W] во время
    включения станции (на экране появится «CLONE»).
4. Нажмите клавишу [MR(SKP)SC] (на экране появится
    «CLONE WAIT»).
5. Нажмите «ОК» для отправки образа на станцию.
 1. Выключите станцию.
2. Подключите кабель к разъёму MIC/SP.
3. Нажмите и удерживайте ручку [PTT] во время включения
     станции.
4. <b>После нажатия «OK»</b> нажмите переключатель [PTT] для отправки образа.
 1. Выключите станцию.
2. Подключите кабель к разъёму MIC/SP.
3. Нажмите и удерживайте ручку [PTT] во время включения
     станции.
4. Нажмите переключатель [MONI] (на экране появится «WAIT»).
5. Нажмите «OK».
 1. Выключите станцию.
2. Подключите кабель к разъёму MIC/SP.
3. Нажмите и удерживайте клавишу [F/W] во время включения станции
     (на экране появится «CLONE»).
4. <b>После нажатия «OK»</b> нажмите клавишу [BAND] для отправки образа.
 1. Выключите станцию.
2. Подключите кабель к разъёму MIC/SP.
3. Нажмите и удерживайте клавишу [F/W] во время
     включения станции (на экране появится «CLONE»).
4. Нажмите клавишу [V/M] (на экране появится «-WAIT-»).
 1. Выключите станцию.
2. Подключите кабель к разъёму MIC/SP.
3. Нажмите и удерживайте клавишу [MON-F] во время включения станции
     (на экране появится «CLONE»).
4. <b>После нажатия «OK»</b> нажмите клавишу [BAND] для отправки образа.
 1. Выключите станцию.
2. Подключите кабель к разъёму MIC/SP.
3. Нажмите и удерживайте клавишу [MON-F] во время включения станции
     (на экране появится «CLONE»).
4. Нажмите клавишу [V/M] (на экране появится «CLONE WAIT»).
 1. Выключите станцию.
2. Подключите кабель к разъёму MIC/SP.
3. Нажмите и удерживайте переключатель [MONI] во время включения
     станции.
4. Поверните регулятор DIAL для выбора «F8 CLONE».
5. Кратко нажмите клавишу [F/W].
6. <b>После нажатия «OK»</b> удерживайте переключатель [PTT]
     в течение одной секунды для отправки образа.
 1. Выключите станцию.
2. Подключите кабель к разъёму MIC/SP.
3. Нажмите и удерживайте переключатель [MONI] во время включения
     станции.
4. Поверните регулятор DIAL для выбора «F8 CLONE».
5. Кратко нажмите клавишу [F/W].
6. Нажмите переключатель [MONI] (на экране появится «--RX--»).
 1. Выключите станцию.
2. Подключите кабель к разъёму MIC/SP.
3. Нажмите и удерживайте клавишу [moni] во время включения
     станции.
4. Выберите в меню CLONE, затем нажмите F. Станция перезагрузится
     в режиме клонирования (на экране появится «CLONE»).
5. <b>После нажатия «OK»</b> кратко удерживайте клавишу [PTT] для
     отправки образа (на экране появится «-TX-»).
 1. Выключите станцию.
2. Подключите кабель к разъёму микрофона.
3. Нажмите и удерживайте клавишу [LOW(A/N)] во время включения
     станции.
4. <b>После нажатия «OK»</b> нажмите клавишу [MHz(SET)] для
     отправки образа.
 1. Выключите станцию.
2. Подключите кабель к разъёму микрофона.
3. Нажмите и удерживайте клавишу [LOW(A/N)] во время включения
     станции.
4. Нажмите клавишу [D/MR(MW)] (на экране появится «--WAIT--»).
 1. Выключите станцию.
2. Подключите кабель к разъёму микрофона.
3. Нажмите и удерживайте клавиши [MHz], [LOW] и [D/MR]
     во время включения станции.
4. <b>После нажатия «OK»</b> нажмите клавишу [MHz(SET)]
     для отправки образа.
 1. Выключите станцию.
2. Подключите кабель к разъёму микрофона.
3. Нажмите и удерживайте клавиши [MHz], [LOW] и [D/MR]
   во время включения станции.
4. Нажмите клавишу [D/MR(MW)] (на экране появится «--WAIT--»).
 1. Выключите станцию.
2. Подключите кабель к коннектору микрофона/spkr.
3. Убедитесь, что коннектор плотно соединён.
4. Включите станцию (может потребоваться установить громкость на 100%).
5. Убедитесь, что станция настроена на неактивный канал.
6. Нажмите «ОК» для загрузки образа с устройства.
 1. Выключите станцию.
2. Подключите кабель к коннектору микрофона/spkr.
3. Убедитесь, что коннектор плотно соединён.
4. Включите станцию (может потребоваться установить громкость на 100%).
5. Убедитесь, что станция настроена на неактивный канал.
6. Нажмите «ОК» для отправки образа на устройство.
 1. Выключите станцию.
2. Подключите кабель к коннектору микрофона/spkr.
3. Убедитесь, что коннектор плотно соединён.
4. Включите станцию.
5. Убедитесь, что станция настроена на неактивный канал.
6. Нажмите «ОК» для загрузки образа с устройства.
 1. Выключите станцию.
2. Подключите кабель к коннектору микрофона/spkr.
3. Убедитесь, что коннектор плотно соединён.
4. Включите станцию.
5. Убедитесь, что станция настроена на неактивный канал.
6. Нажмите «ОК» для отправки образа на устройство.
 1. Выключите станцию.
2. Подключите кабель передачи данных.
3. Подготовьте станцию для клонирования.
4. <b>После нажатия «ОК»</b> нажмите клавишу для отправки образа.
 1. Выключите станцию.
2. Подключите кабель передачи данных.
3. Подготовьте станцию для клонирования.
4. Нажмите клавишу для получения образа.
 1. Выключите станцию.
2. Подключите микрофон и удерживайте на микрофоне [ACC]
     во время включения станции (на экране появится «CLONE»).
3. Замените микрофон на кабель программирования PC.
4. <b>После нажатия «ОК»</b> нажмите клавишу [SET] для
     отправки образа.
 1. Выключите станцию.
2. Подключите микрофон и удерживайте на микрофоне [ACC]
     во время включения станции (на экране появится «CLONE»).
3. Замените микрофон на кабель программирования PC.
4. Нажмите клавишу [DISP/SS] (в левой нижней части экрана
     появится «R»).
 1. Выключите станцию.
2. Уберите переднюю головку.
3. Подключите кабель передачи данных к станции, используя тот же коннектор,
     к которому была подключена головка, а <b>на коннектор микрофона</b>.
4. Нажмите «ОК».
 1. Выключите станцию.
2. Нажмите и удерживайте клавишу [moni] во время включения станции.
3. Выберите CLONE в меню, затем нажмите F. Станция перезагрузится
     в режиме клонирования (на экране появится «CLONE»).
4. Нажмите клавишу [moni] (на экране появится «-RX-»).
 1. Включите станцию.
2. Подключите кабель к терминалу DATA.
3. Отсоедините батарею.
4. Нажмите и удерживайте клавишу [AMS] и клавишу питания во время 
 установки батареи обратно (на экране появится «ADMS»).
5. <b>После нажатия «ОК»</b> нажмите клавишу [BAND].
 1. Включите станцию.
2. Подключите кабель к терминалу DATA.
3. Отсоедините батарею.
4. Нажмите и удерживайте клавишу [AMS] и клавишу питания во время 
 установки батареи обратно (на экране появится «ADMS»).
5. Нажмите клавишу [MODE] (на экране появится «-WAIT-»). Затем нажмите «ОК». 1. Включите станцию.
2. Подключите кабель к коннектору микрофона/spkr.
3. Убедитесь, что коннектор плотно соединён.
4. Нажмите «ОК» для загрузки образа с устройства.

Загрузка может не получиться, если вы включите станцию, когда кабель уже подключён
 1. Включите станцию.
2. Подключите кабель к коннектору микрофона/spkr.
3. Убедитесь, что коннектор плотно соединён.
4. Нажмите «ОК» для отправки образа на устройство.

Отправка может не получиться, если вы включите станцию, когда кабель уже подключён Доступная новая версия программы CHIRP. Как можно скорее загрузите её на веб-сайте! О программе О программе CHIRP Все Все файлы Все поддерживаемые форматы| Любительская радиосвязь Ошибка Применение параметров Доступные модули Частотный план Полосы частот Банки Бинарный Браузер Браузер станций Канада Для изменения этого параметра требуется обновить параметры из образа, что и будет сейчас выполнено. Каналы с эквивалентными TX и RX %s представлены видом субтона «%s». Файлы образов Chirp Необходимо сделать выбор Выберите код DTCS %s Выберите тон %s Выберите кросс-режим Выберите дуплекс Выберите модуль для загрузки из задачи %i: Город Нажмите кнопку-переключатель «Special Channels» редактора ячеек
памяти, чтобы просмотреть/задать каналы EXT. Каналы P-VFO 100-109
считаются параметрами.
В этой версии поддерживается только часть из более чем 200 доступных
параметров станции.
Игнорируйте звуковые сигналы от станции при отправке и загрузке.
 Клонирование завершено, проверка наличия ложных байтов Клонирование Клонирование из станции Клонирование на станцию Закрыть Закрыть файл Комментарий Обмен данными со станцией Завершено Подключите ваш интерфейсный кабель к PC-порту на
задней стороне модуля «TX/RX». НЕ к COM-порту на головке.
 Копировать Страна Кросс-режим Пользовательский порт Пользовательский... Вырезать DTCS Полярность
DTCS DV-память Впереди опасность Десятичный Удалить Режим разработчика Режим разработчика теперь %s. Для применения изменений необходимо перезапустить CHIRP Сравнить необработанные ячейки памяти Цифровой код Отключить отправку отчётов Отключено Расстояние Не запрашивать снова для %s Сделайте двойной щелчок, чтобы изменить имя банка Загрузить Загрузить из станции Загрузить из станции... Инструкции по загрузке Дуплекс Редактировать сведения о ячейках памяти (%i) Редактировать сведения о ячейке памяти (%i) Включить автоматическое редактирование Включено Введите частоту Введите смещение (МГц) Введите частоту TX (МГц) Введите новое имя банка %s: Введите пользовательский порт: Стёрта ячейка памяти %s Ошибка применения параметров Ошибка обмена данными со станцией Экспериментальный драйвер При экспорте можно выполнять запись только в файлы CSV Экспорт в CSV Экспорт в CSV... Дополнительно СВОБОДНАЯ база данных репитеров, содержащая самые
актуальные сведения о репитерах в Европе. Учётная запись
не требуется. Не удалось загрузить браузер станций Не удалось проанализировать результат Функции Файл не существует: %s Файлы Фильтр Фильтровать результаты с расположением, соответствующим этой строке Найти Найти далее Найти... Завершено задание станции %s Следуйте этим инструкциям, чтобы загрузить память станции:
1 - Подключите ваш интерфейсный кабель
2 - Станция > Загрузить из станции: НЕ трогайте станцию
во время загрузки!
3 - Отключите ваш интерфейсный кабель
 Следуйте этим инструкциям, чтобы загрузить память станции:
1 - Подключите ваш интерфейсный кабель
2 - Станция > Загрузить из станции: не настраивайте
радиоголовку!
3 - Отключите ваш интерфейсный кабель
 Следуйте этим инструкциям, чтобы загрузить вашу конфигурацию:
1 - Выключите вашу станцию
2 - Подключите ваш интерфейсный кабель к разъему Speaker-2
3 - Включите вашу станцию
4 - Станция > Загрузить из станции
5 - Отключите интерфейсный кабель! Иначе с правой стороны
    не будет звука!
 Следуйте этим инструкциям, чтобы загрузить вашу информацию:
1 - Выключите вашу станцию
2 - Подключите ваш интерфейсный кабель
3 - Включите вашу станцию
4 - Выполните загрузку ваших радиоданных
 Следуйте этим инструкциям, чтобы загрузить данные вашей станции:
1 - Выключите вашу станцию
2 - Подключите ваш интерфейсный кабель
3 - Включите вашу станцию (разблокируйте её, если есть защита паролем)
4 - Чтобы начать, нажмите «OK»
 Следуйте этим инструкциям, чтобы прочитать данные вашей станции:
1 - Выключите вашу станцию
2 - Подключите ваш интерфейсный кабель
3 - Включите вашу станцию
4 - Чтобы начать, нажмите «OK»
 Следуйте этим инструкциям, чтобы отправить память станции:
1 - Подключите ваш интерфейсный кабель
2 - Станция > Отправить на станцию: не трогайте
станцию во время отправки!
3 - Отключите ваш интерфейсный кабель
 Следуйте этим инструкциям, чтобы отправить память станции:
1 - Подключите ваш интерфейсный кабель
2 - Станция > Отправить на станцию: не настраивайте
радиоголовку!
3 - Отключите ваш интерфейсный кабель
 Следуйте этим инструкциям, чтобы отправить вашу конфигурацию:
1 - Выключите вашу станцию
2 - Подключите ваш интерфейсный кабель к разъему Speaker-2
3 - Включите вашу станцию
4 - Станция > Отправить на станцию
5 - Отключите интерфейсный кабель, иначе с правой стороны
    не будет звука!
6 - Выключите и снова включите питание станции, чтобы выйти
    из режима клонирования
 Следуйте этим инструкциям, чтобы отправить вашу информацию:
1 - Выключите вашу станцию
2 - Подключите ваш интерфейсный кабель
3 - Включите вашу станцию
4 - Выполните отправку ваших радиоданных
 Следуйте этим инструкциям, чтобы отправить данные вашей станции:
1 - Выключите вашу станцию
2 - Подключите ваш интерфейсный кабель
3 - Включите вашу станцию (разблокируйте её, если есть защита паролем)
4 - Чтобы начать, нажмите «ОК»
 Следуйте этим инструкциям, чтобы записать данные вашей станции:
1 - Выключите вашу станцию
2 - Подключите ваш интерфейсный кабель
3 - Включите вашу станцию
4 - Чтобы начать, нажмите «ОК»
 Следуйте этим инструкциям, чтобы загрузить вашу информацию:
1 - Выключите вашу станцию
2 - Подключите ваш интерфейсный кабель
3 - Включите вашу станцию
4 - Выполните загрузку ваших радиоданных
 Следуйте этим инструкциям, чтобы прочитать данные вашей станции:
1 - Выключите вашу станцию
2 - Подключите ваш интерфейсный кабель
3 - Включите вашу станцию
4 - Выполните загрузку ваших радиоданных
 Следуйте этим инструкциям, чтобы отправить вашу информацию:
1 - Выключите вашу станцию
2 - Подключите ваш интерфейсный кабель
3 - Включите вашу станцию
4 - Выполните отправку ваших радиоданных
 Следуйте этим инструкциям, чтобы записать данные вашей станции:
1 - Выключите вашу станцию
2 - Подключите ваш интерфейсный кабель
3 - Включите вашу станцию
4 - Выполните отправку ваших радиоданных
 Найдено значение пустого списка для %(name)s: %(value)r Частота GMRS Получение параметров Перейти к ячейке памяти Переход к ячейке памяти: Переход... Справка Помощь... Шестнадцатеричный Скрыть пустые ячейки памяти Если установлено, упорядочить результаты по расстоянию от этих координат Импорт Импорт из файла... Импорт не рекомендуется Индекс Информация Информация Вставить строку выше Создать значок на рабочем столе? Обмен данными с драйвером %(value)s — неверно (используйте десятичные градусы) Некорректная запись Неверный почтовый индекс Некорректное изменение: %s Некорректный или неподдерживаемый файл модуля Неверное значение: %r Номер задачи: В ЭФИРЕ Широта Длина должна составлять %i Ограничение полос частот Ограничение режимов Ограничение состояния Ограничить результаты этим расстоянием (км) от координат Загрузить модуль... Загрузить модуль из задачи Загрузить модуль из задачи... Загрузка модулей может представлять опасность для вашего компьютера и/или станции. НИКОГДА не загружайте модуль из источника, которому вы не доверяете, в особенности — из любого источника, отличного от основного веб-сайта CHIRP (chirp.danplanet.com). Загрузить модуль из другого источника — всё равно что предоставить прямой доступ к своему компьютеру и всему на нём! Продолжить несмотря на этот риск? Загрузка параметров Долгота Ячейки памяти Ячейку памяти %i нельзя удалить Чтобы редактировать ячейку памяти, необходимо поместить её в банк Ячейка памяти {num} отсутствует в банке {bank} Режим Модель Режимы Модуль Модуль загружен Модуль успешно загружен Найдено более одного порта: %s Переместить вниз Переместить вверх Новое окно Доступна новая версия Ниже нет пустых строк! Модули не найдены В задаче %i не найдены модули Нет результатов Нет результатов! Число Смещение Только некоторые полосы частот Только некоторые режимы Можно экспортировать только вкладки памяти Только работающие репитеры Открыть Открыть недавние Открыть предустановленную конфигурацию Открыть файл Открыть модуль Открыть журнал отладки Открыть в новом окне Открыть каталог предустановленной конфигурации Опционально: -122.0000 Опционально: 100 Опционально: 45.0000 Опционально: округ, больница и т.д. Перезаписать ячейки памяти? Каналы P-VFO 100-109 считаются параметрами.
В этой версии поддерживается только часть из
более чем 130 доступных параметров станции.
 Анализ Вставить Вставленные ячейки памяти перезапишут существующие ячейки памяти (%s) Вставленные ячейки памяти перезапишут ячейки памяти (%s) Вставленные ячейки памяти перезапишут ячейку памяти %s Вставленная ячейка памяти перезапишет ячейку памяти %s Обязательно выйдите из программы CHIRP перед установкой новой версии! Выполните следующие действия:
1 - Включите вашу станцию.
2 - Подключите интерфейсный кабель к вашей станции.
3 - Нажмите кнопку в этом окне для начала загрузки.
    (может появится другой диалог, нажмите «ОК»).
4 - Станция подаст звуковой сигнал, светодиод замигает.
5 - У вас будет 10 секунд для того, чтобы нажать «MON» перед
    началом отправки данных.
6 - Если всё пройдёт хорошо, по завершении станция подаст звуковой сигнал.
После выполнения клонирования уберите кабель и выключите и снова включите
 питание станции для перехода в обычный режим.
 Выполните следующие действия:
1 - Включите вашу станцию.
2 - Подключите интерфейсный кабель к вашей станции.
3 - Нажмите кнопку в этом окне для начала загрузки.
    (Станция подаст звуковой сигнал, светодиод замигает.)
4 - Затем нажмите кнопку «A» на вашей станции для начала клонирования.
    (По завершении станция подаст звуковой сигнал.)
 Обратите внимание, что режим разработчика предназначен для использования разработчиками проекта CHIRP или под руководством разработчика. В этом режиме включаются поведение и функции, которые следует использовать КРАЙНЕ осторожно, чтобы не навредить вашему компьютеру и вашей станции. Вы были предупреждены. Всё равно продолжить? Подождите Подключите кабель и нажмите «ОК» Порт Мощность Нажмите «Ввод» для добавления в память Предпросмотр Печать Свойства Запрос %s Запрос источника DTCS RX Станция Станция не распознала блок %i Информация о станции Станция отправила данные после последнего ожидаемого блока; это происходит, когда выбранная модель не является американской, а станция является. Выберите правильную модель и повторите попытку. RadioReference Canada запрашивает вход перед выполнением запроса Требуется обновление Обновлена ячейки памяти %s Перезагрузить драйвер Перезагрузить драйвер и файл Переименовать банк Отправка отчётов включена Отправка отчётов помогает проекту CHIRP получать информацию о том, какие модели станций и платформы ОС требуют нашего внимания. Мы будем очень признательны, если вы оставите эту функцию включённой. Действительно отключить отправку отчётов? Требуется перезапуск Полученные параметры Сохранить Сохранить перед закрытием? Сохранить файл Сохранённые параметры Угроза безопасности Выбрать частотный план... Выбор полос частот Выбор режимов Выбор частотного плана Служба Параметры Показать необработанную ячейку памяти Показать расположение журнала отладки Показать дополнительные поля Некоторые ячейки памяти несовместимы с этой станцией Некоторые ячейки памяти нельзя удалить Сортировать по столбцу: Упорядочить ячейки памяти Сортировка Область Область/регион Успешно Драйвер X3Plus является экспериментальным.
Известных ошибок нет, но следует действовать осмотрительно.
Сохраните нередактированную копию первой успешной
загрузки в файл CHIRP Radio Images (*.img).
 Автора этого модуля нет среди разработчиков CHIRP. Рекомендуется не загружать этот модуль, так как возможна угроза безопасности. Всё равно продолжить? Рекомендуемая процедура импорта ячеек памяти: открыть исходный файл и скопировать/вставить ячейки памяти из него в ваш целевой образ. Если продолжить работу с этой функцией импорта, CHIRP заменит все ячейки памяти в текущем открытом файле на ячейки из %(file)s. Открыть этот файл для копирования/вставки ячеек памяти или продолжить импорт? Эта ячейка памяти Этот драйвер был протестирован с ID-5100 v3. Если ваша станция обновлена не полностью, создайте отчёт об ошибке и приложите к нему журнал отладки, чтобы мы могли добавить соответствующую поддержку для других версий. Это ранняя бета-версия драйвера
 Это ранняя бета-версия драйвера — отправляйте на свой страх и риск
 Это экспериментальная поддержка для BJ-9900, она всё ещё в разработке.
Обязательно сделайте резервную копию с помощью OEM-программы.
Также присылайте отчёты об ошибках и предложения по улучшению.
Вы были предупреждены; продолжайте на свой страх и риск! Эта ячейка памяти и сдвинуть все вверх Эта ячейка памяти и сдвинуть блок вверх На этой станции сложно войти в режим программирования, нужно
несколько попыток даже при использовании оригинального ПО.
Попробуйте 8 раз (в большинстве случаев достаточно ~3 раз). Вход
может занять несколько секунд. Если не получается, сделайте ещё
несколько попыток. Если удалось войти, проверьте станцию и кабель.
 Эту опцию следует включать только в том случае, если вы используете модифицированную микропрограмму, которая поддерживает более широкий диапазон частот. Если включить эту опцию, CHIRP не будет применять OEM-ограничения, что может привести к неопределённому или нерегулируемому поведению. Используйте на свой страх и риск! Будет выполнена загрузка модуля из задачи на веб-сайте ТонПРД Вид субтона ТонШПД Шаг настройки Поиск USB-портов Не удалось определить порт для вашего кабеля. Проверьте драйверы и подключения. Перед редактированием ячейки памяти необходимо загрузить станцию Не удалось найти предустановленную конфигурацию %r Не удалось открыть буфер обмена Не удалось выполнить запрос Невозможно прочитать последний блок. Это часто случается, когда выбрана американская модель, но станция не является американской (или широкополосной). Выберите правильную модель и повторите попытку. Невозможно обнаружить %s в этой системе Невозможно установить %s для этой ячейки памяти Соединённые Штаты Отключите кабель (если требуется) и нажмите «ОК» Отправка инструкций Отправка на станцию Отправка на станцию... Отправлена ячейка памяти %s Использовать моноширинный шрифт Использовать более крупный шрифт Значение не помещается в %i бит Значение не должно быть меньше %.4f Значение не должно превышать %.4f Значение должно содержать десятичные цифры, ровно %i Значение должно быть больше или равно нулю Значения Изготовитель Вид ПРЕДУПРЕЖДЕНИЕ! Предупреждение Предупреждение: %s Добро пожаловать Следует ли программе CHIRP создать значок на рабочем столе? Похоже, ваш кабель вставлен в порт: бит байт байт каждый отключён включён {bank} полон 