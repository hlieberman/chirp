��    �     4  	  L      p   
  q   -   |!  9   �!     �!  ;   �!  +   6"     b"     i"    x"  �   �#  �  ,$  �   �%  �   \&    @'  �   W(    L)  �   R*  �   L+  �   D,  �   @-    0.  o  </  q  �0  �   2  �   �2  p  �3  e  A5  �   �6  �   �7  �   l8  �   C9  �   3:  �    ;  �   <  �   �<  	  �=  �   �>  �   �?  �   y@  �   XA  �   0B  �   C  #  �C    E  M  F  �   mG  �   (H  �   �H  �   �I    uJ    }K  �   �L  �   eM  �   EN  m   �N  �   4O  �   P  �   �P  �   �Q  �   �R    �S  �   �T  �   �U  ^   rV     �V     �V     �V  	   �V     �V     W     W     &W     8W     JW     SW     YW     _W     cW     kW     �W  ]   �W  J   �W     2X     DX     TX     hX     wX     �X  (   �X     �X    �X  ,   �Y     Z     Z     ,Z     =Z  
   CZ  %   NZ     tZ     |Z     �Z  k   �Z     [     [     [  
   #[     .[  	   :[     D[     H[     M[     [[  	   g[     q[     ~[     �[     �[  A   �[     �[     �[     �[     \     \     "\     +\      F\     g\     p\     �\     �\     �\  C   �\     ]     ]     ,]     G]     ^]     f]     v]     �]     �]     �]     �]     �]     �]     ^     /^     O^     ]^     n^     t^  u   }^     �^     _     '_     0_     H_     N_  1   U_     �_  	   �_     �_     �_  �   �_  �   `  �   Ka    b  �   c  �   �c  �   �d  �   e  �   �e  �   �f  8  ^g  �   �h  �   <i  �   �i  �   �j  �   3k  �   �k  �   |l  .    m  	   Om     Ym     ^m     om     {m     �m     �m  
   �m     �m     �m  7   �m     �m     �m     n     "n     (n     -n     9n     Jn     `n  '   un     �n     �n     �n  "   �n     �n     o     o     o     o     0o     <o     Ho  4   Uo     �o     �o     �o  y  �o     Dq     Uq     sq  	   �q     �q     �q  %   �q     �q     r     
r     r     r     r     +r     Fr  	   cr     mr  
   ur     �r     �r     �r     �r  
   �r     �r     �r     �r     �r     s      $s     Es     \s     as     ms     s     �s     �s     �s     �s     �s     �s     �s      t     ,t  �   @t     �t     �t  3   �t  *   	u  (   4u  &   ]u  ?   �u  �  �u  (  �w    �x     �y  $   �y     �y     �y  !   z     $z     2z  
   ;z     Fz     Oz     \z     dz     jz     �z  �   �z  ;   @{  t   |{     �{     |     |     $|     ;|  W   G|     �|  �   �|     j}     {}     �}     �}     �}  	   �}     �}  	   �}  	   �}     �}     ~     ~     "~     /~     A~     I~     R~     b~     z~  .   �~     �~     �~  3   �~  5   /     e     u     �     �     �     �     �  �   �  �   ��  ^  :�     ��  �   ��  G   `�  #   ��  =   ̃  <  
�  �   G�     ,�     I�  &  h�  �   ��  ,   ~�     ��  	   ��     ��     ǈ     ӈ  L   �  ,   0�     ]�     |�     ��  �   ��  "   S�     v�     ��  /   ��     Ԋ     �     ��     �     �     3�     C�     a�     }�  '   ��     ��     ދ     �     �     �     ��     �     �  7   �  !   N�     p�     u�  
   {�     ��     ��     ��  e  ��  &  �  4   3�  I   h�     ��  G   Ə  1   �  
   @�     K�  p  Y�  �   ʑ    ��  �   ��  ,  h�     ��  �   ��    ��    ��  �   ��  �   ��  �   ��    z�  p  ��  [  ��  �   U�  �   >�  X  �  p  i�  �   ڥ  �   ��  �   ��  �   y�  �   m�  �   M�  �   8�  �   �    	�  �   �  �   �  �   ��  �   ��  �   W�  �   E�  d  �    ��  �  ��  �   �  �   ܷ  �   ��  �   r�  :  :�  8  u�    ��    ��  �   ��  ~   \�    ۿ    ��  �   �    ��  �   ��    ��  �   ��  �   ��  d   ��  	   &�     0�     @�     G�     U�     q�     y�     ��     ��     ��     ��     ��     ��  
   ��  #   ��      �  o   �  H   w�     ��     ��     ��     ��     	�     �  )   (�     R�  ]  Y�  0   ��     ��     ��     �     !�     '�  #   6�     Z�     `�     u�  t   }�     ��     �     �     �     �     8�     A�     E�     J�     X�  	   g�     q�     ��     ��     ��  U   ��     ��     �     �  !   #�     E�     S�     Z�  2   p�     ��     ��     ��     ��     ��  [   ��     S�  '   Z�  '   ��  $   ��     ��     ��     ��     ��  !   �  #   1�     U�  "   g�     ��     ��  4   ��     ��     ��     
�     �  �   �  !   ��     ��     ��     ��     �     �  6   �     T�     X�     d�     k�  �   ��  �   X�  �   '�    ��  �   �  �   ��  �   ��  �   Z�  �   (�  �   ��  G  ��  �   �  �   ��  �   ��  �   ��  �   K�  �   �  �   ��  4   ��     ��     ��     ��  	   ��  
   ��     ��     ��     ��     �     �  E   (�     n�     z�     ��     ��     ��     ��     ��  !   ��     ��  0   �     ?�     O�     d�  -   }�     ��     ��     ��     ��     ��     ��     	�     �  6   ,�     c�     t�     ��  �  ��     x�     ��     ��     ��  	   ��     ��  ?   ��  &   )�     P�     T�     Z�     a�     h�     y�  ,   ��     ��     ��     ��     ��     �     �     2�  
   Q�     \�     h�     o�     u�     ��  0   ��  1   ��     �     �     %�     ?�     M�  !   \�     ~�  "   ��     ��     ��     ��  %    �  %   &�  �   L�     ��     ��  I   �  A   Q�  =   ��  7   ��  Q   	�  J  [�  g  ��  Z  �     i�  0   z�     ��     ��  )   ��     ��     ��     ��  
   �     �     !�     )�     0�     O�  �   ^�  M   �  z   ^�     ��     ��     	�  '   %�     M�  ^   h�     ��  	  ��     ��  -   �     6�     F�      M�     n�     ~�     ��     ��     ��     ��     ��     ��     ��     �     �     �  +   /�     [�  )   t�     ��  #   ��  A   ��  C   �     `�     w�  	   ��     ��  
   ��     ��  )   ��  �   ��  �   ��  �  p�  	   �  �   #�  S   �  &   c�  L   ��  �  ��  �   X�  $   T  #   y  7  �  I  � 7       W    [    d    r    � k   � /     %   0    V    i �   y    0    O    m A   �    �    �    �     #       ;    Z    v     � 1   � ,   � 	    
       )    1    8 
   ?    J G   W 1   �    �    �    �    �            L       \   �   0     �   T      h   �   �                   �   =  �   .  4      �         �     8           D   q    �   s   T   }   �           z       �   �   B       ^   �   \  �   <   g          w           4         s      %       [   B      �   g       -      h    t  /  {  �       |              �   K      �      x   k  �      �       �   �           *   a   U   x  &   ;  )   �   �       m  �   M   '     >   7   i  o          K       �                      �   �   6  �       �           c   S  �          j      C   M  �   �       H  �   G   �   �       u  (     N  �   f      �   }      y  �             $       C  '      *      �   �   7  �   3  �    I      ,   �             �   Y   Z   f   y   	       �           �   �   �      r   Q     {   -  c  <  /         l   o   �   8  =   r  �   m               �   
  W         �   6   H      ]  w      5  l  :   v  �       �   >  F   ~              
           �   P       V          �   �   �   �   ?          �          �   �   :  �     d  �         �      3   p   $          "   _  �       J  .   �   X      v   �      P               �   �   �       �          O     �   [  0               9      �   O   J        #  +  �     2       _           b   �   A    �      	  �   �       1   �   �   �   �     u   �  �         �   A       L  "     �   R       �       t       !   �   @   �   �     D      S   %  �       �   X       n   �       @      �   �   �      I   �       �   �   �   �   �       Z  �   R     2  �                     e            E  �   �   a  �           1      n  p  �      !  ^  j   |   ~               b      `  �          �             �   �   N   G  k   E   ,  �   5      `   q   �   �   �   �   ?      �   �           Q     d           �         �  �   e              �   �   9         )  V   �          U      i                           �   (  W  &  ]   #   F  �   ;   �   Y  z      �       �   +     1. Turn radio off.
 2. Connect cable to DATA terminal.
 3. Press and hold in [DISP] key while turning on radio
      ("CLONE" will appear on radio LCD).
 4. Press [RECEIVE] screen button
      ("-WAIT-" will appear on radio LCD).
5. Finally, press OK button below.
 %(value)s must be between %(min)i and %(max)i %i Memories and shift all up %i Memories and shift all up %i Memory %i Memories %i Memory and shift block up %i Memories and shift block up %s has not been saved. Save before closing? (none) ...and %i more 1. Ensure your firmware version is 4_10 or higher
2. Turn radio off
3. Connect your interface cable
4. Turn radio on
5. Press and release PTT 3 times while holding MONI key
6. Supported baud rates: 57600 (default) and 19200
   (rotate dial while holding MONI to change)
7. Click OK
 1. Turn Radio off.
2. Connect data cable.
3. While holding "A/N LOW" button, turn radio on.
4. <b>After clicking OK</b>, press "SET MHz" to send image.
 1. Turn Radio off.
2. Connect data cable.
3. While holding "A/N LOW" button, turn radio on.
4. Press "MW D/MR" to receive image.
5. Make sure display says "-WAIT-" (see note below if not)
6. Click OK to dismiss this dialog and start transfer.
Note: if you don't see "-WAIT-" at step 5, try cycling
      power and pressing and holding red "*L" button to unlock
      radio, then start back at step 1.
 1. Turn Radio off.
2. Connect data cable.
3. While holding "TONE" and "REV" buttons, turn radio on.
4. <b>After clicking OK</b>, press "TONE" to send image.
 1. Turn Radio off.
2. Connect data cable.
3. While holding "TONE" and "REV" buttons, turn radio on.
4. Press "REV" to receive image.
5. Make sure display says "CLONE RX" and green led is blinking
6. Click OK to start transfer.
 1. Turn radio off.
2. Connect cable
3. Press and hold in the MHz, Low, and D/MR keys on the radio while turning it on
4. Radio is in clone mode when TX/RX is flashing
5. <b>After clicking OK</b>, press the MHz key on the radio to send image.
    ("TX" will appear on the LCD). 
 1. Turn radio off.
2. Connect cable
3. Press and hold in the MHz, Low, and D/MR keys on the radio while turning it on
4. Radio is in clone mode when TX/RX is flashing
5. Press the Low key on the radio ("RX" will appear on the LCD).
6. Click OK. 1. Turn radio off.
2. Connect cable to ACC jack.
3. Press and hold in the [MODE &lt;] and [MODE &gt;] keys while
     turning the radio on ("CLONE MODE" will appear on the
     display).
4. <b>After clicking OK</b> here, press the [C.S.] key to
    send image.
 1. Turn radio off.
2. Connect cable to ACC jack.
3. Press and hold in the [MODE &lt;] and [MODE &gt;] keys while
     turning the radio on ("CLONE MODE" will appear on the
     display).
4. <b>After clicking OK</b>, press the [A] key to send image.
 1. Turn radio off.
2. Connect cable to ACC jack.
3. Press and hold in the [MODE &lt;] and [MODE &gt;] keys while
     turning the radio on ("CLONE MODE" will appear on the
     display).
4. Click OK here.
    ("Receiving" will appear on the LCD).
 1. Turn radio off.
2. Connect cable to ACC jack.
3. Press and hold in the [MODE &lt;] and [MODE &gt;] keys while
     turning the radio on ("CLONE MODE" will appear on the
     display).
4. Press the [A](RCV) key ("receiving" will appear on the LCD).
 1. Turn radio off.
2. Connect cable to ACC jack.
3. Press and hold in the [MODE &lt;] and [MODE &gt;] keys while
     turning the radio on ("CLONE MODE" will appear on the
     display).
4. Press the [C] key ("RX" will appear on the LCD).
 1. Turn radio off.
2. Connect cable to CAT/LINEAR jack.
3. Press and hold in the [MODE &lt;] and [MODE &gt;] keys while
     turning the radio on ("CLONE MODE" will appear on the
     display).
4. <b>After clicking OK</b>,
     press the [C](SEND) key to send image.
 1. Turn radio off.
2. Connect cable to DATA jack.
3. Press and hold in the "left" [V/M] key while turning the
     radio on.
4. Rotate the "right" DIAL knob to select "CLONE START".
5. Press the [SET] key. The display will disappear
     for a moment, then the "CLONE" notation will appear.
6. <b>After clicking OK</b>, press the "left" [V/M] key to
     send image.
 1. Turn radio off.
2. Connect cable to DATA jack.
3. Press and hold in the "left" [V/M] key while turning the
     radio on.
4. Rotate the "right" DIAL knob to select "CLONE START".
5. Press the [SET] key. The display will disappear
     for a moment, then the "CLONE" notation will appear.
6. Press the "left" [LOW] key ("CLONE -RX-" will appear on
     the display).
 1. Turn radio off.
2. Connect cable to DATA jack.
3. Press and hold in the [FW] key while turning the radio on
     ("CLONE" will appear on the display).
4. <b>After clicking OK</b>, press the [BAND] key to send image.
 1. Turn radio off.
2. Connect cable to DATA jack.
3. Press and hold in the [FW] key while turning the radio on
     ("CLONE" will appear on the display).
4. Press the [MODE] key ("-WAIT-" will appear on the LCD).
 1. Turn radio off.
2. Connect cable to DATA jack.
3. Press and hold in the [MHz(PRI)] key while turning the
     radio on.
4. Rotate the DIAL job to select "F-7 CLONE".
5. Press and hold in the [BAND(SET)] key. The display
     will disappear for a moment, then the "CLONE" notation
     will appear.
6. Press the [LOW(ACC)] key ("--RX--" will appear on the display).
 1. Turn radio off.
2. Connect cable to DATA jack.
3. Press and hold in the [MHz(PRI)] key while turning the
 radio on.
4. Rotate the DIAL job to select "F-7 CLONE".
5. Press and hold in the [BAND(SET)] key. The display
 will disappear for a moment, then the "CLONE" notation
 will appear.
6. <b>After clicking OK</b>, press the [V/M(MW)] key to send image.
 1. Turn radio off.
2. Connect cable to DATA terminal.
3. Press and hold [DISP] key while turning on radio
     ("CLONE" will appear on the display).
4. <b>After clicking OK here in chirp</b>,
     press the [Send] screen button.
 1. Turn radio off.
2. Connect cable to DATA terminal.
3. Press and hold in the [F] key while turning the radio on
     ("CLONE" will appear on the display).
4. <b>After clicking OK</b>, press the [BAND] key to send image.
 1. Turn radio off.
2. Connect cable to DATA terminal.
3. Press and hold in the [F] key while turning the radio on
     ("CLONE" will appear on the display).
4. Press the [Dx] key ("-WAIT-" will appear on the LCD).
 1. Turn radio off.
2. Connect cable to DATA terminal.
3. Press and hold in the [MHz(SETUP)] key while turning the radio
     on ("CLONE" will appear on the display).
4. <b>After clicking OK</b>, press the [REV(DW)] key
     to send image.
 1. Turn radio off.
2. Connect cable to DATA terminal.
3. Press and hold in the [MHz(SETUP)] key while turning the radio
     on ("CLONE" will appear on the display).
4. Press the [MHz(SETUP)] key
     ("-WAIT-" will appear on the LCD).
 1. Turn radio off.
2. Connect cable to MIC Jack.
3. Press and hold in the [MHz(SETUP)] key while turning the radio
     on ("CLONE" will appear on the display).
4. <b>After clicking OK</b>, press the [GM(AMS)] key
     to send image.
 1. Turn radio off.
2. Connect cable to MIC Jack.
3. Press and hold in the [MHz(SETUP)] key while turning the radio
     on ("CLONE" will appear on the display).
4. Press the [MHz(SETUP)] key
     ("-WAIT-" will appear on the LCD).
 1. Turn radio off.
2. Connect cable to MIC/EAR jack.
3. Press and hold in the [F/W] key while turning the radio on
    ("CLONE" will appear on the display).
4. <b>After clicking OK</b>, press the [VFO(DW)SC] key to receive
    the image from the radio.
 1. Turn radio off.
2. Connect cable to MIC/EAR jack.
3. Press and hold in the [F/W] key while turning the radio on
    ("CLONE" will appear on the display).
4. Press the [MR(SKP)SC] key ("CLONE WAIT" will appear
    on the LCD).
5. Click OK to send image to radio.
 1. Turn radio off.
2. Connect cable to MIC/SP jack.
3. Press and hold [PTT] &amp; Knob while turning the
     radio on.
4. <b>After clicking OK</b>, press the [PTT] switch to send image.
 1. Turn radio off.
2. Connect cable to MIC/SP jack.
3. Press and hold [PTT] &amp; Knob while turning the
     radio on.
4. Press the [MONI] switch ("WAIT" will appear on the LCD).
5. Press OK.
 1. Turn radio off.
2. Connect cable to MIC/SP jack.
3. Press and hold in the [F/W] key while turning the radio on
     ("CLONE" will appear on the display).
4. <b>After clicking OK</b>, press the [BAND] key to send image.
 1. Turn radio off.
2. Connect cable to MIC/SP jack.
3. Press and hold in the [F/W] key while turning the radio on
     ("CLONE" will appear on the display).
4. Press the [V/M] key ("-WAIT-" will appear on the LCD).
 1. Turn radio off.
2. Connect cable to MIC/SP jack.
3. Press and hold in the [MON-F] key while turning the radio on
     ("CLONE" will appear on the display).
4. <b>After clicking OK</b>, press the [BAND] key to send image.
 1. Turn radio off.
2. Connect cable to MIC/SP jack.
3. Press and hold in the [MON-F] key while turning the radio on
     ("CLONE" will appear on the display).
4. Press the [V/M] key ("CLONE WAIT" will appear on the LCD).
 1. Turn radio off.
2. Connect cable to MIC/SP jack.
3. Press and hold in the [MONI] switch while turning the
     radio on.
4. Rotate the DIAL job to select "F8 CLONE".
5. Press the [F/W] key momentarily.
6. <b>After clicking OK</b>, hold the [PTT] switch
     for one second to send image.
 1. Turn radio off.
2. Connect cable to MIC/SP jack.
3. Press and hold in the [MONI] switch while turning the
     radio on.
4. Rotate the DIAL job to select "F8 CLONE".
5. Press the [F/W] key momentarily.
6. Press the [MONI] switch ("--RX--" will appear on the LCD).
 1. Turn radio off.
2. Connect cable to MIC/SP jack.
3. Press and hold in the [moni] key while turning the radio on.
4. Select CLONE in menu, then press F. Radio restarts in clone mode.
     ("CLONE" will appear on the display).
5. <b>After clicking OK</b>, briefly hold [PTT] key to send image.
    ("-TX-" will appear on the LCD). 
 1. Turn radio off.
2. Connect cable to mic jack.
3. Press and hold in the [LOW(A/N)] key while turning the radio on.
4. <b>After clicking OK</b>, press the [MHz(SET)] key to send image.
 1. Turn radio off.
2. Connect cable to mic jack.
3. Press and hold in the [LOW(A/N)] key while turning the radio on.
4. Press the [D/MR(MW)] key ("--WAIT--" will appear on the LCD).
 1. Turn radio off.
2. Connect cable to mic jack.
3. Press and hold in the [MHz], [LOW], and [D/MR] keys
   while turning the radio on.
4. <b>After clicking OK</b>, press the [MHz(SET)] key to send image.
 1. Turn radio off.
2. Connect cable to mic jack.
3. Press and hold in the [MHz], [LOW], and [D/MR] keys
   while turning the radio on.
4. Press the [D/MR(MW)] key ("--WAIT--" will appear on the LCD).
 1. Turn radio off.
2. Connect cable to mic/spkr connector.
3. Make sure connector is firmly connected.
4. Turn radio on (volume may need to be set at 100%).
5. Ensure that the radio is tuned to channel with no activity.
6. Click OK to download image from device.
 1. Turn radio off.
2. Connect cable to mic/spkr connector.
3. Make sure connector is firmly connected.
4. Turn radio on (volume may need to be set at 100%).
5. Ensure that the radio is tuned to channel with no activity.
6. Click OK to upload image to device.
 1. Turn radio off.
2. Connect cable to mic/spkr connector.
3. Make sure connector is firmly connected.
4. Turn radio on.
5. Ensure that the radio is tuned to channel with no activity.
6. Click OK to download image from device.
 1. Turn radio off.
2. Connect cable to mic/spkr connector.
3. Make sure connector is firmly connected.
4. Turn radio on.
5. Ensure that the radio is tuned to channel with no activity.
6. Click OK to upload image to device.
 1. Turn radio off.
2. Connect data cable.
3. Prepare radio for clone.
4. <b>After clicking OK</b>, press the key to send image.
 1. Turn radio off.
2. Connect data cable.
3. Prepare radio for clone.
4. Press the key to receive the image.
 1. Turn radio off.
2. Connect mic and hold [ACC] on mic while powering on.
    ("CLONE" will appear on the display)
3. Replace mic with PC programming cable.
4. <b>After clicking OK</b>, press the [SET] key to send image.
 1. Turn radio off.
2. Connect mic and hold [ACC] on mic while powering on.
    ("CLONE" will appear on the display)
3. Replace mic with PC programming cable.
4. Press the [DISP/SS] key
    ("R" will appear on the lower left of LCD).
 1. Turn radio off.
2. Remove front head.
3. Connect data cable to radio, use the same connector where
     head was connected to, <b>not the mic connector</b>.
4. Click OK.
 1. Turn radio off.
3. Press and hold in the [moni] key while turning the radio on.
4. Select CLONE in menu, then press F. Radio restarts in clone mode.
     ("CLONE" will appear on the display).
5. Press the [moni] key ("-RX-" will appear on the LCD).
 1. Turn radio on.
2. Connect cable to DATA terminal.
3. Unclip battery.
4. Press and hold in the [AMS] key and power key while clipping 
 in back battery the("ADMS" will appear on the display).
5. <b>After clicking OK</b>, press the [BAND] key.
 1. Turn radio on.
2. Connect cable to DATA terminal.
3. Unclip battery.
4. Press and hold in the [AMS] key and power key while clipping 
 in back battery the("ADMS" will appear on the display).
5. Press the [MODE] key ("-WAIT-" will appear on the LCD). OK</b>
 click <b>Then 1. Turn radio on.
2. Connect cable to mic/spkr connector.
3. Make sure connector is firmly connected.
4. Click OK to download image from device.

It will may not work if you turn on the radio with the cable already attached
 1. Turn radio on.
2. Connect cable to mic/spkr connector.
3. Make sure connector is firmly connected.
4. Click OK to upload the image to device.

It will may not work if you turn on the radio with the cable already attached A new CHIRP version is available. Please visit the website as soon as possible to download it! About About CHIRP All All Files All supported formats| Amateur An error has occurred Applying settings Available modules Bandplan Bands Banks Bin Browser Building Radio Browser Canada Changing this setting requires refreshing the settings from the image, which will happen now. Channels with equivalent TX and RX %s are represented by tone mode of "%s" Chirp Image Files Choice Required Choose %s DTCS Code Choose %s Tone Choose Cross Mode Choose duplex Choose the module to load from issue %i: City Click on the "Special Channels" toggle-button of the memory
editor to see/set the EXT channels. P-VFO channels 100-109
are considered Settings.
Only a subset of the over 200 available radio settings
are supported in this release.
Ignore the beeps from the radio on upload and download.
 Clone completed, checking for spurious bytes Cloning Cloning from radio Cloning to radio Close Close file Cluster %i memory Cluster %i memories Comment Communicate with radio Complete Connect your interface cable to the PC Port on the
back of the 'TX/RX' unit. NOT the Com Port on the head.
 Convert to FM Copy Country Cross mode Custom Port Custom... Cut DTCS DTCS
Polarity DTMF decode DV Memory Danger Ahead Dec Delete Developer Mode Developer state is now %s. CHIRP must be restarted to take effect Diff Raw Memories Digital Code Digital Modes Disable reporting Disabled Distance Do not prompt again for %s Double-click to change bank name Download Download from radio Download from radio... Download instructions Driver information Dual-mode digital repeaters that support analog will be shown as FM Duplex Edit details for %i memories Edit details for memory %i Enable Automatic Edits Enabled Enter Frequency Enter Offset (MHz) Enter TX Frequency (MHz) Enter a new name for bank %s: Enter custom port: Erased memory %s Error applying settings Error communicating with radio Experimental driver Export can only write CSV files Export to CSV Export to CSV... Extra FM Radio FREE repeater database, which provides most up-to-date
information about repeaters in Europe. No account is
required. Failed to load radio browser Failed to parse result Features File does not exist: %s Files Filter Filter results with location matching this string Find Find Next Find... Finished radio job %s Follow these instructions to download the radio memory:
1 - Connect your interface cable
2 - Radio > Download from radio: DO NOT mess with the radio
during download!
3 - Disconnect your interface cable
 Follow these instructions to download the radio memory:
1 - Connect your interface cable
2 - Radio > Download from radio: Don't adjust any settings
on the radio head!
3 - Disconnect your interface cable
 Follow these instructions to download the radio memory:
1 - Turn off your radio
2 - Connect your interface cable
3 - Turn on your radio, volume @ 50%
4 - CHIRP Menu - Radio - Download from radio
 Follow these instructions to download your config:
1 - Turn off your radio
2 - Connect your interface cable to the Speaker-2 jack
3 - Turn on your radio
4 - Radio > Download from radio
5 - Disconnect the interface cable! Otherwise there will be
    no right-side audio!
 Follow these instructions to download your info:
1 - Turn off your radio
2 - Connect your interface cable
3 - Turn on your radio
4 - Do the download of your radio data
 Follow these instructions to download your radio:
1 - Turn off your radio
2 - Connect your interface cable
3 - Turn on your radio (unlock it if password protected)
4 - Click OK to start
 Follow these instructions to read your radio:
1 - Turn off your radio
2 - Connect your interface cable
3 - Turn on your radio
4 - Click OK to start
 Follow these instructions to upload the radio memory:
1 - Connect your interface cable
2 - Radio > Upload to radio: DO NOT mess with the radio
during upload!
3 - Disconnect your interface cable
 Follow these instructions to upload the radio memory:
1 - Connect your interface cable
2 - Radio > Upload to radio: Don't adjust any settings
on the radio head!
3 - Disconnect your interface cable
 Follow these instructions to upload the radio memory:
1 - Turn off your radio
2 - Connect your interface cable
3 - Turn on your radio, volume @ 50%
4 - CHIRP Menu - Radio - Upload to radio
 Follow these instructions to upload your config:
1 - Turn off your radio
2 - Connect your interface cable to the Speaker-2 jack
3 - Turn on your radio
4 - Radio > Upload to radio
5 - Disconnect the interface cable, otherwise there will be
    no right-side audio!
6 - Cycle power on the radio to exit clone mode
 Follow these instructions to upload your info:
1 - Turn off your radio
2 - Connect your interface cable
3 - Turn on your radio
4 - Do the upload of your radio data
 Follow these instructions to upload your radio:
1 - Turn off your radio
2 - Connect your interface cable
3 - Turn on your radio (unlock it if password protected)
4 - Click OK to start
 Follow these instructions to write your radio:
1 - Turn off your radio
2 - Connect your interface cable
3 - Turn on your radio
4 - Click OK to start
 Follow this instructions to download your info:
1 - Turn off your radio
2 - Connect your interface cable
3 - Turn on your radio
4 - Do the download of your radio data
 Follow this instructions to read your radio:
1 - Turn off your radio
2 - Connect your interface cable
3 - Turn on your radio
4 - Do the download of your radio data
 Follow this instructions to upload your info:
1 - Turn off your radio
2 - Connect your interface cable
3 - Turn on your radio
4 - Do the upload of your radio data
 Follow this instructions to write your radio:
1 - Turn off your radio
2 - Connect your interface cable
3 - Turn on your radio
4 - Do the upload of your radio data
 Found empty list value for %(name)s: %(value)r Frequency GMRS Getting settings Goto Memory Goto Memory: Goto... Help Help Me... Hex Hide empty memories If set, sort results by distance from these coordinates Import Import from file... Import not recommended Index Info Information Insert Row Above Install desktop icon? Interact with driver Invalid %(value)s (use decimal degrees) Invalid Entry Invalid ZIP code Invalid edit: %s Invalid or unsupported module file Invalid value: %r Issue number: LIVE Latitude Length must be %i Limit Bands Limit Modes Limit Status Limit results to this distance (km) from coordinates Load Module... Load module from issue Load module from issue... Loading modules can be extremely dangerous, leading to damage to your computer, radio, or both. NEVER load a module from a source you do not trust, and especially not from anywhere other than the main CHIRP website (chirp.danplanet.com). Loading a module from another source is akin to giving them direct access to your computer and everything on it! Proceed despite this risk? Loading settings Logo string 1 (12 characters) Logo string 2 (12 characters) Longitude Memories Memory %i is not deletable Memory must be in a bank to be edited Memory {num} not in bank {bank} Mode Model Modes Module Module Loaded Module loaded successfully More than one port found: %s Move Down Move Up New Window New version available No empty rows below! No modules found No modules found in issue %i No results No results! Number Offset Only certain bands Only certain modes Only memory tabs may be exported Only working repeaters Open Open Recent Open Stock Config Open a file Open a module Open debug log Open in new window Open stock config directory Optional: -122.0000 Optional: 100 Optional: 45.0000 Optional: County, Hospital, etc. Overwrite memories? P-VFO channels 100-109 are considered Settings.
Only a subset of the over 130 available radio settings
are supported in this release.
 Parsing Paste Pasted memories will overwrite %s existing memories Pasted memories will overwrite memories %s Pasted memories will overwrite memory %s Pasted memory will overwrite memory %s Please be sure to quit CHIRP before installing the new version! Please follow this steps carefully:
1 - Turn on your radio
2 - Connect the interface cable to your radio
3 - Click the button on this window to start download
    (you may see another dialog, click ok)
4 - Radio will beep and led will flash
5 - You will get a 10 seconds timeout to press "MON" before
    data upload start
6 - If all goes right radio will beep at end.
After cloning remove the cable and power cycle your radio to
get into normal mode.
 Please follow this steps carefully:
1 - Turn on your radio
2 - Connect the interface cable to your radio.
3 - Click the button on this window to start download
    (Radio will beep and led will flash)
4 - Then press the "A" button in your radio to start cloning.
    (At the end radio will beep)
 Please note that developer mode is intended for use by developers of the CHIRP project, or under the direction of a developer. It enables behaviors and functions that can damage your computer and your radio if not used with EXTREME care. You have been warned! Proceed anyway? Please wait Plug in your cable and then click OK Port Power Press enter to set this in memory Print Preview Printing Properties Query %s Query Source RX DTCS Radio Radio did not ack block %i Radio information Radio sent data after the last awaited block, this happens when the selected model is a non-US but the radio is a US one. Please choose the correct model and try again. RadioReference Canada requires a login before you can query RadioReference.com is the world's largest
radio communications data provider
<small>Premium account required</small> Refresh required Refreshed memory %s Reload Driver Reload Driver and File Rename bank RepeaterBook is Amateur Radio's most comprehensive,
worldwide, FREE repeater directory. Reporting enabled Reporting helps the CHIRP project know which radio models and OS platforms to spend our limited efforts on. We would really appreciate if you left it enabled. Really disable reporting? Restart Required Restore %i tab Restore %i tabs Retrieved settings Save Save before closing? Save file Saved settings Scanlists Scrambler Security Risk Select Bandplan... Select Bands Select Modes Select a bandplan Service Settings Show Raw Memory Show debug log location Show extra fields Some memories are incompatible with this radio Some memories are not deletable Sort %i memory Sort %i memories Sort %i memory ascending Sort %i memories ascending Sort %i memory descending Sort %i memories descending Sort by column: Sort memories Sorting State State/Province Success The DMR-MARC Worldwide Network The X3Plus driver is currently experimental.
There are no known issues but you should proceed with caution.
Please save an unedited copy of your first successful
download to a CHIRP Radio Images (*.img) file.
 The author of this module is not a recognized CHIRP developer. It is recommended that you not load this module as it could pose a security risk. Proceed anyway? The recommended procedure for importing memories is to open the source file and copy/paste memories from it into your target image. If you continue with this import function, CHIRP will replace all memories in your currently-open file with those in %(file)s. Would you like to open this file to copy/paste memories across, or proceed with the import? This Memory This driver has been tested with v3 of the ID-5100. If your radio is not fully updated please help by opening a bug report with a debug log so we can add support for the other revisions. This driver is in development and should be considered as experimental. This is an early stage beta driver
 This is an early stage beta driver - upload at your own risk
 This is an experimental driver for the Quansheng UV-K5. It may harm your radio, or worse. Use at your own risk.

Before attempting to do any changes please download the memory image from the radio with chirp and keep it. This can be later used to recover the original settings. 

some details are not yet implemented This is experimental support for BJ-9900 which is still under development.
Please ensure you have a good backup with OEM software.
Also please send in bug and enhancement requests!
You have been warned. Proceed at your own risk! This memory and shift all up This memory and shift block up This radio has a tricky way of enter into program mode,
even the original software has a few tries to get inside.
I will try 8 times (most of the time ~3 will doit) and this
can take a few seconds, if don't work, try again a few times.
If you can get into it, please check the radio and cable.
 This should only be enabled if you are using modified firmware that supports wider frequency coverage. Enabling this will cause CHIRP not to enforce OEM restrictions and may lead to undefined or unregulated behavior. Use at your own risk! This will load a module from a website issue Tone Tone Mode Tone Squelch Tuning Step USB Port Finder Unable to determine port for your cable. Check your drivers and connections. Unable to edit memory before radio is loaded Unable to find stock config %r Unable to open the clipboard Unable to query Unable to read last block. This often happens when the selected model is US but the radio is a non-US one (or widebanded). Please choose the correct model and try again. Unable to reveal %s on this system Unable to set %s on this memory United States Unplug your cable (if needed) and then click OK Upload instructions Upload to radio Upload to radio... Uploaded memory %s Use fixed-width font Use larger font Value does not fit in %i bits Value must be at least %.4f Value must be at most %.4f Value must be exactly %i decimal digits Value must be zero or greater Values Vendor View WARNING! Warning Warning: %s Welcome Would you like CHIRP to install a desktop icon for you? Your cable appears to be on port: bits bytes bytes each disabled enabled {bank} is full Project-Id-Version: CHIRP
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-01-20 18:51+0300
Last-Translator: Abdullah YILMAZ (TA1AUB) <h.abdullahyilmaz@hotmail.com>
Language-Team: TURKISH
Language: tr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 3.4.2
  1. Telsizi kapatın.
  2. Kabloyu DATA terminaline bağlayın.
  3. Telsizi açarken [DISP] tuşunu basılı tutun
  (telsiz LCD'sinde "CLONE" görünecektir).
  4. [RECEIVE] ekran tuşuna basın
       (telsiz LCD'sinde "-WAIT-" görünecektir).
5. Son olarak aşağıdaki OK tuşuna basın.
 %(value)s, %(min)i ile %(max)i arasında olmalıdır %i Kaydı ve hepsini yukarı kaydır %i Kaydı ve hepsini yukarı kaydır %i Kaydı %i Kaydı %i Kaydı ve bloğu yukarı kaydır %i Kaydı ve bloğu yukarı kaydır %s kaydedilmedi. Kapanmadan önce kaydedilsin mi? (hiçbiri) ...ve %i daha 1. Donanım yazılımı sürümünüzün 4_10 veya üzeri olduğundan emin olun
2. Telsizi kapatın
3. Arayüz kablonuzu bağlayın
4. Telsizi açın
5. MONI tuşunu basılı tutarken PTT'ye 3 kez basın ve bırakın
6. Desteklenen baud hızları: 57600 (varsayılan) ve 19200
   (değiştirmek için MONI'ye basılı tutarken kadranı çevirin)
7. TAMAM'a tıklayın
 1. Telsizi kapatın.
2. Veri kablosunu bağlayın.
3. "A/N LOW" tuşunu basılı tutarken telsizi açın.
4. <b>TAMAM'ı tıkladıktan sonra</b>, imajı göndermek için "SET MHz"e basın.
 1. Telsizi kapatın.
2. Veri kablosunu bağlayın.
3. "A/N LOW" tuşunu basılı tutarken telsizi açın.
4. İmajı almak için "MW D/MR" tuşuna basın.
5. Ekranda "-WAIT-" yazdığından emin olun (değilse aşağıdaki nota bakın)
6. Bu iletişim kutusunu kapatmak ve aktarımı başlatmak için TAMAM'a tıklayın.
Not: 5. adımda "-WAIT-" öğesini görmüyorsanız, gücü kapatıp açmayı
      ve telsizin kilidini açmak için kırmızı "*L" tuşunu basılı tutmayı deneyin,
      ardından 1. adımdan başlayın.
 1. Telsizi kapatın.
2. Veri kablosunu bağlayın.
3. "TONE" ve "REV" tuşlarını basılı tutarken telsizi açın.
4. <b>TAMAM'ı tıkladıktan sonra</b>, imajı göndermek için "TONE" tuşuna basın.
 1. Telsizi kapatın.
2. Veri kablosunu bağlayın.
3. "TONE" ve "REV" tuşlarını basılı tutarken telsizi açın.
4. İmajı almak için "REV" tuşuna basın.
5. Ekranda "CLONE RX" yazdığından ve yeşil ledin yanıp söndüğünden emin olun.
6. Aktarımı başlatmak için TAMAM'a tıklayın.
 1. Telsizi kapatın.
2. Kabloyu bağlayın.
3. Telsizi açarken MHz, LOW ve D/MR tuşlarını basılı tutun.
4. TX/RX yanıp sönerken radyo klon modundadır
5. <b>TAMAM'a tıkladıktan sonra</b>, imajı göndermek için radyodaki MHz tuşuna basın.
     ("TX" LCD'de görünecektir). 
 1. Telsizi kapatın.
2. Kabloyu bağlayın.
3. Telsizi açarken MHz, LOW ve D/MR tuşlarını basılı tutun.
4. TX/RX yanıp sönerken radyo klon modundadır
5. Telsizdeki Low tuşuna basın ("RX" LCD'de görünecektir).
6. Tamam'a tıklayın. 1. Telsizi kapatın.
2. Kabloyu ACC jakına bağlayın.
3. Telsizi açarken [MODE &lt;] ve [MODE &gt;] tuşlarını basılı tutun 
     (ekranda "KLON MODU" görünecektir).
4. <b>TAMAM'ı tıkladıktan sonra</b>, imajı göndermek için
     [C.S.] tuşuna basın.
 1. Telsizi kapatın.
2. Kabloyu ACC jakına bağlayın.
3. Telsizi açarken [MODE &lt;] ve [MODE &gt;] tuşlarını
    basılı tutun ("ekranda KLON MODU" görünecektir).
4. <b>TAMAM'ı tıkladıktan sonra</b>, imajı göndermek için
    [A] tuşuna basın.
 1. Telsizi kapatın.
2. Kabloyu ACC jakına bağlayın.
3. Telsizi açarken [MODE &lt;] ve [MODE &gt;] tuşlarını basılı tutun ("ekranda KLON MODU" görünecektir).
4. Burada TAMAM'ı tıklayın.
     (ekranda "Alınıyor" görünecektir).
 1. Telsizi kapatın.
2. Kabloyu ACC jakına bağlayın.
3. Telsizi açarken [MODE &lt;] ve [MODE &gt;] tuşlarını basılı tutun (ekranda "KLON MODU" görünecektir).
4. [A](RCV) tuşuna basın (ekranda "alıyor" görünecektir).
 1. Telsizi kapatın.
2. Kabloyu ACC jakına bağlayın.
3. Telsizi açarken [MODE &lt;] ve [MODE &gt;] tuşlarını basılı tutun ("ekranda KLON MODU" görünecektir).
4. [C] tuşuna basın (ekranda "RX" görünecektir).
 1. Telsizi kapatın.
2. Kabloyu CAT/LINEAR jakına bağlayın.
3. Telsizi açarken [MODE &lt;] ve [MODE &gt;] tuşlarını basılı tutun (ekranda "KLON MODU" görünecektir).
4. <b>TAMAM'ı tıkladıktan sonra</b>, imajı göndermek için [C](GÖNDER) tuşuna basın.
 1. Telsizi kapatın.
2. Kabloyu DATA jakına bağlayın.
3. Telsizi açarken "sol" [V/M] tuşuna basılı tutun.
4. "KLONLAMA BAŞLAT"ı seçmek için  DIAL düğmesini "sağa" çevirin.
5. [SET] tuşuna basın. Ekran bir an kaybolacak, ardından "CLONE" notu görünecektir.
6. <b>Tamam'a tıkladıktan sonra</b>, imajı  göndermek için "sol" [V/M] tuşuna basın.
 1. Telsizi kapatın.
2. Kabloyu DATA jakına bağlayın.
3. Telsizi açarken "sol" [V/M] tuşuna basılı tutun.
4. "KLONLAMA BAŞLAT"ı seçmek için  DIAL düğmesini 
"sağa" çevirin.
5. [SET] tuşuna basın. Ekran bir an kaybolacak, ardından 
"CLONE" notu görünecektir.
6. "Sol" [LOW] tuşuna basın ("CLONE -RX-" ekranda görünecektir).
 1. Telsizi kapatın.
2. Kabloyu DATA jakına bağlayın.
3. Telsizi açarken [FW] tuşunu basılı tutun
      (ekranda "CLONE" görünecektir).
4. <b>TAMAM'ı tıkladıktan sonra</b>, imajı göndermek için [BAND] tuşuna basın.
 1. Telsizi kapatın.
2. Kabloyu DATA jakına bağlayın.
3. Telsizi açarken [FW] tuşunu basılı tutun
      (ekranda "CLONE" görünecektir).
4. [MODE] tuşuna basın (ekranda "CLONE WAIT" görünecektir).
 1. Telsizi kapatın.
2. Kabloyu DATA jakına bağlayın.
3. Telsizi açarken [MHz(PRI)] tuşunu basılı tutun.
4. "F-7 CLONE" öğesini seçmek için KADRANI çevirin.
5. [BAND(SET)] tuşunu basılı tutun. Ekran bir an için kaybolacak, ardından "CLONE" yazısı belirecektir.
6. [LOW(ACC)] tuşuna basın (ekranda "--RX--" görünecektir).
 1. Telsizi kapatın.
2. Kabloyu DATA jakına bağlayın.
3. Telsizi açarken [MHz(PRI)] tuşunu basılı tutun.
4. "F-7 CLONE" öğesini seçmek için KADRANI çevirin.
5. [BAND(SET)] tuşunu basılı tutun. Ekran bir an için kaybolacak, ardından "CLONE" yazısı belirecektir.
6. <b>TAMAM'ı tıkladıktan sonra</b>, imaj göndermek için [V/M(MW)] tuşuna basın.
 1. Telsizi kapatın.
2. Kabloyu DATA terminaline bağlayın.
3. Telsizi açarken [DISP] tuşunu basılı tutun
      ("CLONE" ekranda görünecektir).
4. <b>TAMAM'ı tıkladıktan sonra</b>,
     [Gönder] ekran tuşuna basın.
 1. Telsizi kapatın.
2. Kabloyu DATA terminaline bağlayın.
3. Telsizi açarken [F] tuşunu basılı tutun
      ("CLONE" ekranda görünecektir).
4. <b>TAMAM'ı tıkladıktan sonra</b>, imajı göndermek için [BAND] tuşuna basın.
 1. Telsizi kapatın.
2. Kabloyu DATA terminaline bağlayın.
3. Telsizi açarken [F] tuşunu basılı tutun
     (ekranda "CLONE" görünecektir).
4. [Dx] tuşuna basın (ekranda "-WAIT-" görünecektir).
 1. Telsizi kapatın.
2. Kabloyu DATA terminaline bağlayın.
3. Telsizi açarken [MHz(SETUP)] tuşunu basılı tutun ("ekranda CLONE" görünecektir).
4. <b>TAMAM'ı tıkladıktan sonra</b>, imajı göndermek için [GERİ(DW)] tuşuna basın.
 1. Telsizi kapatın.
2. Kabloyu DATA terminaline bağlayın.
3. Telsizi açarken [MHz(SETUP)] tuşunu basılı tutun ("ekranda CLONE" görünecektir).
4. [MHz(SETUP)] tuşuna basın
      (ekranda "-WAIT-" görünecektir).
 1. Telsizi kapatın.
2. Kabloyu MIC Jakına bağlayın.
3. Telsizi açarken [MHz(SETUP)] tuşunu basılı tutun ("ekranda CLONE" görünecektir).
4. <b>TAMAM'ı tıkladıktan sonra</b>, imaj göndermek için [GM(AMS)] tuşuna basın.
 1. Telsizi kapatın.
2. Kabloyu MIC Jakına bağlayın.
3. Telsizi açarken [MHz(SETUP)] tuşunu basılı tutun ("ekranda CLONE" görünecektir).
4. [MHz(SETUP)] tuşuna basın
      (ekranda "-WAIT-" görünecektir).
 1. Telsizi kapatın.
2. Kabloyu MIC/EAR jakına bağlayın.
3. Telsizi açarken [F/W] tuşunu basılı tutun
     (ekranda "CLONE" görünecektir).
4. <b>TAMAM'ı tıkladıktan sonra</b>, imajı telsizden almak için [VFO(DW)SC] tuşuna basın.
 1. Telsizi kapatın.
2. Kabloyu MIC/EAR jakına bağlayın.
3. Telsizi açarken [F/W] tuşunu basılı tutun
     (ekranda "CLONE" görünecektir).
4. [MR(SKP)SC] tuşuna basın (ekranda "CLONE WAIT" görünecektir).
5. İmajı telsize göndermek için TAMAM'a tıklayın.
 1. Telsizi kapatın.
2. Kabloyu MIC/SP jakına bağlayın.
3. Telsizi açarken [PTT] &amp;  tuşuna basın.
4. <b>TAMAM'ı tıkladıktan sonra</b>, imaj göndermek
     için [PTT] tuşuna basın.
 1. Telsizi kapatın.
2. Kabloyu MIC/SP jakına bağlayın.
3. Telsizi açarken [PTT] &amp;  tuşuna basın.
4. [MONI] tuşuna basın (ekranda "WAIT" görünecektir).
5. TAMAM'a basın.
 1. Telsizi kapatın.
2. Kabloyu MIC/SP jakına bağlayın.
3. Telsizi açarken [F/W] tuşunu basılı tutun
      (ekranda "CLONE" görünecektir).
4. <b>TAMAM'ı tıkladıktan sonra</b>, imajı göndermek için [BAND] tuşuna basın.
 1. Telsizi kapatın.
2. Kabloyu MIC/SP jakına bağlayın.
3. Telsizi açarken [F/W] tuşunu basılı tutun
      (ekranda "CLONE" görünecektir).
4. [V/M] tuşuna basın (ekranda "-WAIT-" görünecektir).
 1. Telsizi kapatın.
2. Kabloyu MIC/SP jakına bağlayın.
3. Telsizi açarken [MON-F] tuşunu basılı tutun
      (ekranda "CLONE" görünecektir).
4. <b>TAMAM'ı tıkladıktan sonra</b>, imajı göndermek için [BAND] tuşuna basın.
 1. Telsizi kapatın.
2. Kabloyu MIC/SP jakına bağlayın.
3. Telsizi açarken [MON-F] tuşunu basılı tutun
      (ekranda "CLONE" görünecektir).
4. [V/M] tuşuna basın (ekranda "CLONE WAIT" görünecektir).
 1. Telsizi kapatın.
2. Kabloyu MIC/SP jakına bağlayın.
3. Telsizi açarken [PTT] &amp;  tuşuna basın.
3. Telsizi açarken [MONI] tuşunu basılı tutun.
4. "F8 CLONE" öğesini seçmek için KADRANI çevirin.
5. Bir an için [F/W] tuşuna basın.
6. <b>TAMAM'ı tıkladıktan sonra</b>, imaj göndermek için [PTT] tuşunu bir saniye basılı tutun.
 1. Telsizi kapatın.
2. Kabloyu MIC/SP jakına bağlayın.
3. Telsizi açarken [MONI] tuşunu basılı tutun.
4. "F8 CLONE" öğesini seçmek için KADRANI çevirin.
5. Bir an için [F/W] tuşuna basın.
6. [MONI] tuşuna basın (ekranda "--RX--" görünecektir).
 1. Telsizi kapatın.
2. Kabloyu MIC/SP jakına bağlayın.
3. Telsizi açarken [moni] tuşunu basılı tutun.
4. Menüden KLON'u seçin, ardından F'ye basın. Telsiz klon modunda yeniden başlar.
      (ekranda "CLONE" görünecektir).
5. <b>TAMAM'ı tıkladıktan sonra</b>, imajı göndermek için [PTT] tuşunu kısa bir süre basılı tutun.
     (ekranda "-TX-" görünecektir). 
 1. Telsizi kapatın.
2. Kabloyu mikrofon jakına bağlayın.
3. Telsizi açarken [LOW(A/N)] tuşunu basılı tutun.
4. <b>TAMAM'ı tıkladıktan sonra</b>, imajı göndermek için [MHz(SET)] tuşuna basın.
 1. Telsizi kapatın.
2. Kabloyu mikrofon jakına bağlayın.
3. Telsizi açarken [LOW(A/N)] tuşunu basılı tutun.
4. [D/MR(MW)] tuşuna basın (ekranda "--WAIT--" görünecektir).
 1. Telsizi kapatın.
2. Kabloyu mikrofon jakına bağlayın.
3. Telsizi açarken [MHz], [LOW] ve [D/MR] tuşlarına basılı tutun
4. <b>TAMAM'ı tıkladıktan sonra</b>, imajı göndermek için [MHz(SET)] tuşuna basın.
 1. Telsizi kapatın.
2. Kabloyu mikrofon jakına bağlayın.
3. Telsizi açarken [MHz], [LOW] ve [D/MR] tuşlarını basılı tutun.
4. [D/MR(MW)] tuşuna basın (ekranda "--WAIT--" görünecektir).
 1. Telsizi kapatın.
2. Kabloyu mik/hprlr konektörüne bağlayın.
3. Konektörün sıkıca bağlandığından emin olun.
4. Telsizi açın (ses düzeyinin %100 olarak ayarlanması gerekebilir).
5. Telsizin etkin olmayan kanala ayarlandığından emin olun.
6. İmajı cihazdan indirmek için TAMAM'a tıklayın.
 1. Telsizi kapatın.
2. Kabloyu mic/spkr konektörüne bağlayın.
3. Konektörün sıkıca bağlandığından emin olun.
4. Telsizi açın (ses düzeyinin %100 olarak ayarlanması gerekebilir).
5. Telsizin etkin olmayan kanala ayarlandığından emin olun.
6. İmajı cihaza yüklemek için TAMAM'a tıklayın.
 1. Telsizi kapatın.
2. Kabloyu mik/hprlr konektörüne bağlayın.
3. Konektörün sıkıca bağlandığından emin olun.
4. Telsizi açın.
5. Telsizin etkin olmayan kanala ayarlandığından emin olun.
6. İmajı cihazdan indirmek için TAMAM'a tıklayın.
 1. Telsizi kapatın.
2. Kabloyu mic/spkr konektörüne bağlayın.
3. Konektörün sıkıca bağlandığından emin olun.
4. Telsizi açın.
5. Telsizin etkin olmayan kanala ayarlandığından emin olun.
6. İmajı cihaza yüklemek için TAMAM'a tıklayın.
 1. Telsizi kapatın.
2. Veri kablosunu bağlayın.
3. Telsizi klon için hazırlayın.
4. <b>TAMAM'ı tıkladıktan sonra</b>, imaj göndermek için tuşuna basın.
 1. Telsizi kapatın.
2. Veri kablosunu bağlayın.
3. Telsizi klon için hazırlayın.
4. İmajı almak için tuşuna basın.
 1. Telsizi kapatın.
2. Mikrofonu bağlayın ve açarken mikrofondaki [ACC] tuşunu basılı tutun.
     ("CLONE" ekranda görünecektir)
3. Mikrofonu PC programlama kablosuyla değiştirin.
4. <b>TAMAM'ı tıkladıktan sonra</b>, imajı göndermek için [SET] tuşuna basın.
 1. Telsizi kapatın.
2. Mikrofonu bağlayın ve açarken mikrofondaki [ACC] tuşunu basılı tutun.
     ("CLONE" ekranda görünecektir)
3. Mikrofonu PC programlama kablosuyla değiştirin.
4. [DISP/SS] tuşuna basın
     (LCD'nin sol alt köşesinde "R" görünecektir).
 1. Telsizi kapatın.
2. Ön başlığı çıkarın.
3. Veri kablosunu telsize bağlayın, 
     <b>mikrofon konektörüne değil</b> kafanın bağlı olduğu konektörü kullanın.
4. TAMAM'a tıklayın.
 1. Telsizi kapatın.
3. Telsizi açarken [moni] tuşunu basılı tutun.
4. Menüden KLON'u seçin, ardından F'ye basın. Telsiz klon modunda yeniden başlar.
      (ekranda "CLONE" görünecektir).
5. [moni] tuşuna basın (ekranda "-RX-" görünecektir).
 1. Radyoyu açın.
2. Kabloyu DATA terminaline bağlayın.
3. Bataryayı çıkartın.
4. Bataryayı takarken [AMS] tuşuna ve güç tuşuna basılı tutun ("ADMS" ekranda görünecektir).
5. <b>Tamam'a tıkladıktan sonra</b>, [BAND] tuşuna basın.
 1. Radyoyu açın.
2. Kabloyu DATA terminaline bağlayın.
3. Bataryayı çıkartın.
4. Bataryayı takarken [AMS] tuşuna ve güç tuşuna basılı tutun ("ADMS" ekranda görünecektir).
5. [MODE] tuşuna basın (LCD'de "-WAIT-" görünecektir). Sonra </b> TAMAM <b> tıklayın 1. Telsizi kapatın.
2. Kabloyu mic/spkr konektörüne bağlayın.
3. Konektörün sıkıca bağlandığınızdan emin olun.
4. İmajı cihazdan indirmek için Tamam'a tıklayın.

Kablo takılıyken telsizi açarsanız çalışmayabilir
 1. Telsizi kapatın.
2. Kabloyu mic/spkr konektörüne bağlayın.
3. Konektörün sıkıca bağlandığınızdan emin olun.
4. İmajı cihaza yüklemek için Tamam'a tıklayın.

Kablo takılıyken telsizi açarsanız çalışmayabilir Yeni bir CHIRP sürümü mevcut. İndirmek için lütfen en kısa zamanda web sitesini ziyaret edin! Hakkında CHIRP Hakkında Tümü Tüm Dosyalar Desteklenen tüm biçimler| Amatör Bir hata oluştu Ayarlar uygulanıyor Mevcut modüller Bant planı Bantlar Bankalar Bin Tarayıcı Telsiz Tarayıcısı oluşturuluyor Kanada Bu ayarın değiştirilmesi, imajdaki ayarların yenilenmesini gerektirir, bu işlem şimdi gerçekleşecektir. Eşdeğer TX ve RX %s'ye sahip kanallar, "%s" ton modu ile temsil edilir Chirp İmaj Dosyaları Seçim Gerekli %s DTCS Kodunu Seç %s Tonunu Seç Çapraz Modu Seç Dubleks seç Sorun %i'den yüklenecek modülü seçin: Şehir EXT kanallarını görmek/ayarlamak için kayıt düzenleyicisinin
"Özel Kanallar" geçiş düğmesine tıklayın. P-VFO kanalları
100-109 olarak ayarlanabilir. Bu sürümde 200'den fazla
kullanılabilir telsiz ayarının yalnızca bir alt kümesi
desteklenmektedir. Yükleme ve indirme sırasında
telsizden gelen bip seslerini dikkate almayın.
 Klon tamamlandı, sahte baytlar kontrol ediliyor Klonlanıyor Telsizden klonlanıyor Telsize klonlanıyor Kapat Dosyayı kapat %i Kaydı sırala %i kaydı sırala Yorum Telsiz ile iletişim Tamamla Arayüz kablonuzu 'TX/RX' ünitesinin arkasındaki
PC Bağlantı Noktasına bağlayın. Kafadaki Com Portu DEĞİL.
 FM'e dönüştür Kopyala Ülke Çapraz mod Özel Bağlantı Noktası Özel... Kes DTCS DTCS
Polarite DTMF kod çöz DV Kaydı İleride Tehlike Dec Sil Geliştirici Modu Geliştirici durumu artık %s. Etkili olması için CHIRP yeniden başlatılmalıdır Farklı Ham Kayıtlar Dijital Kod Dijital Modlar Raporlamayı devre dışı bırak Devre dışı Mesafe %s için tekrar sorma Banka adını değiştirmek için çift tıklayın İndir Telsizden indir Telsizden indir... İndirme talimatları Sürücü bilgileri Analogu destekleyen çift modlu dijital tekrarlayıcılar(röleler) FM olarak gösterilecek Duplex %i kaydı için ayrıntıları düzenle %i kaydı için ayrıntıları düzenle Otomatik Düzenlemeleri Etkinleştir Etkin Frekans Gir Ofset girin (MHz) TX Frekansını girin (MHz) Banka %s için yeni bir ad girin: Özel bağlantı noktasını girin: %s kaydı silindi Ayarlar uygulanırken hata oluştu Telsizle iletişim hatası Deneysel sürücü Dışa aktarma yalnızca CSV dosyalarını yazabilir CSV'ye aktar CSV'ye aktar... Ekstra FM Radyo Avrupa'daki tekrarlayıcılar (röleler) hakkında en güncel
bilgileri sağlayan ÜCRETSİZ tekrarlayıcı (röle) veritabanı.
Hesap gerekmez. Telsiz tarayıcısı yüklenemedi Sonuç ayrıştırılamadı Özellikler Dosya mevcut değil: %s Dosyalar Filtre Bu dizeyle eşleşen konuma sahip sonuçları filtrele Bul Sonraki Bul Bul... %s telsiz işleri tamamlandı Telsiz hafızasını indirmek için şu talimatları izleyin:
1 - Arayüz kablonuzu bağlayın
2 - Telsiz > Telsizden İndir: İndirme sırasında telsiz ile uğraşmayın!
3 - Arayüz kablonuzu çıkarın
 Telsiz hafızasını indirmek için şu talimatları izleyin:
1 - Arayüz kablonuzu bağlayın
2 - Telsiz > Telsizden İndir: Telsiz kafasında herhangi bir ayar yapmayın!
3 - Arayüz kablonuzu çıkarın
 Telsiz hafızasını indirmek için şu talimatları izleyin:
1 - Telsizinizi kapatın
2 - Arayüz kablonuzu bağlayın
3 - Telsizinizi açın, ses seviyesi @ %50
4 - 4 - CHIRP Menüsü - Telsiz - Telsizden indir
 Yapılandırmanızı indirmek için şu talimatları izleyin:
1 - Telsizinizi kapatın
2 - Arayüz kablonuzu Speaker-2 jakına bağlayın
3 - Telsizinizi açın
4 - Telsiz > Telsizden İndir
5 - Arayüz kablosunu çıkarın! Aksi takdirde, sağ taraf sesi olmayacaktır!
 Bilgilerinizi indirmek için şu talimatları izleyin:
1 - Telsizinizi kapatın
2 - Arayüz kablonuzu bağlayın
3 - Telsizinizi açın
4 - Telsiz verilerinizin indirilmesini gerçekleştirin
 Bilgilerinizi indirmek için bu talimatları izleyin:
1 - Telsizinizi kapatın
2 - Arayüz kablonuzu bağlayın
3 - Telsizinizi açın (şifre korumalıysa engelini kaldırın)
4 - Başlatmak için TAMAM'a tıklayın.
 Bilgilerinizi indirmek için bu talimatları izleyin:
1 - Telsizinizi kapatın
2 - Arayüz kablonuzu bağlayın
3 - Telsizinizi açın
4 - Başlatmak için TAMAM'a tıklayın.
 Telsiz hafızasını yüklemek için şu talimatları izleyin:
1 - Arayüz kablonuzu bağlayın
2 - Telsiz > Telsize Yükle: Yükleme sırasında telsiz ile uğraşmayın!
3 - Arayüz kablonuzu çıkarın
 Telsiz hafızasını yüklemek için şu talimatları izleyin:
1 - Arayüz kablonuzu bağlayın
2 - Telsiz > Telsize yükle: Telsiz kafasında herhangi bir ayar yapmayın!
3 - Arayüz kablonuzu çıkarın
 Telsiz hafızasını yüklemek için şu talimatları izleyin:
1 - Telsizinizi kapatın
2 - Arayüz kablonuzu bağlayın
3 - Telsizinizi açın, ses seviyesi @ %50
4 - 4 - CHIRP Menüsü - Telsiz - Telsize yükle
 Yapılandırmanızı yüklemek için şu talimatları izleyin:
1 - Telsizinizi kapatın
2 - Arayüz kablonuzu Speaker-2 jakına bağlayın
3 - Telsizinizi açın
4 - Telsiz > Telsize yükle
5 - Arayüz kablosunu çıkarın, aksi takdirde sağ tarafta ses olmayacaktır!
6 - Klon modundan çıkmak için telsizi kapatıp açın
 Bilgilerinizi yüklemek için şu talimatları izleyin:
1 - Telsizinizi kapatın
2 - Arayüz kablonuzu bağlayın
3 - Telsizinizi açın
4 - Telsiz verilerinizin yüklemesini gerçekleştirin
 Bilgilerinizi yüklemek için bu talimatları izleyin:
1 - Telsizinizi kapatın
2 - Arayüz kablonuzu bağlayın
3 - Telsizinizi açın (şifre korumalıysa engelini kaldırın)
4 - Başlatmak için TAMAM'a tıklayın.
 Bilgilerinizi yazmak için bu talimatları izleyin:
1 - Telsizinizi kapatın
2 - Arayüz kablonuzu bağlayın
3 - Telsizinizi açın (şifre korumalıysa engelini kaldırın)
4 - Başlatmak için TAMAM'a tıklayın.
 Bilgilerinizi indirmek için şu talimatları izleyin:
1 - Telsizinizi kapatın
2 - Arayüz kablonuzu bağlayın
3 - Telsizinizi açın
4 - Telsiz verilerinizin indirilmesini gerçekleştirin
 Telsizinizi okumak için bu talimatları izleyin:
1 - Telsizinizi kapatın
2 - Arayüz kablonuzu bağlayın
3 - Telsizinizi açın
4 - Telsiz verilerinizin indirmesini gerçekleştirin
 Bilgilerinizi yüklemek için şu talimatları izleyin:
1 - Telsizinizi kapatın
2 - Arayüz kablonuzu bağlayın
3 - Telsizinizi açın
4 - Telsiz verilerinizin yüklemesini gerçekleştirin
 Telsizinizi yazmak için bu talimatları izleyin:
1 - Telsizinizi kapatın
2 - Arayüz kablonuzu bağlayın
3 - Telsizinizi açın
4 - Telsiz verilerinizin yüklemesini gerçekleştirin
 %(name)s için boş liste değeri bulundu: %(value)r Frekans GMRS Ayarlar alınıyor Kayda Git Kayda Git: Git... Yardım Bana Yardım Et... Hex Boş kayıtları gizle Ayarlanırsa, sonuçları bu koordinatlardan uzaklığa göre sırala İçe Aktar Dosyadan İçe Aktar... İçe aktarma önerilmez Dizin Bilgi Bilgi Yukarıya Satır Ekle Masaüstü simgesi yüklensin mi? Sürücü ile etkileşim Geçersiz %(value)s (ondalık basamak kullanın) Geçersiz Girdi Geçersiz Posta kodu Geçersiz düzenleme: %s Geçersiz veya desteklenmeyen modül dosyası Geçersiz değer: %r Kayıt numarası: CANLI Enlem Uzunluk %i olmalıdır Bantları Sınırla Modları Sınırla Durum Sınırla Sonuçları koordinatlardan bu mesafeye (km) sınırla Modül Yükle... Kayıttan modül yükle Kayıttan modül yükle... Modülleri yüklemek son derece tehlikeli olabilir ve bilgisayarınıza, telsizinize veya her ikisine birden zarar verebilir. ASLA güvenmediğiniz bir kaynaktan ve özellikle ana CHIRP web sitesi (chirp.danplanet.com) dışında herhangi bir yerden modül yüklemeyin. Başka bir kaynaktan bir modül yüklemek, kaynak sahiplerinin bilgisayarınıza ve üzerindeki her şeye doğrudan erişim sağlamasına olanak sağlayabilir! Bu riske rağmen devam edilsin mi? Ayarlar yükleniyor Logo dizesi 1 (12 karakter) Logo dizesi 2 (12 karakter) Boylam Kayıtlar Kayıt %i silinemez Hafızanın düzenlenebilmesi için bir bankada olması gerekir {num} kaydı {bank} bankasında değil Mod Model Modlar Modül Modül Yüklendi Modül başarıyla yüklendi Birden fazla bağlantı noktası bulundu: %s Aşağı Taşı Yukarı Taşı Yeni Pencere Yeni sürüm mevcut Aşağıda boş satır yok! Modül bulunamadı %i kadında modül bulunamadı Sonuç yok Sonuç yok! Numara Ofset Sadece belirli gruplar Yalnızca belirli modlar Yalnızca kayıt sekmeleri dışa aktarılabilir Yalnızca çalışan tekrarlayıcılar (röleler) Aç Son Kullanılanları Aç Hazır Yapılandırma Aç Bir dosya aç Bir modül aç Hata ayıklama günlüğünü aç Yeni pencerede aç Hazır yapılandırma dizinini aç İsteğe bağlı: -122.0000 İsteğe bağlı: 100 İsteğe bağlı: 45.0000 İsteğe bağlı: bölge, Hastane vb. Kayıtların üzerine yazılsın mı? P-VFO kanalları 100-109  olarak ayarlanabilir.
Bu sürümde 130'dan fazla kullanılabilir telsiz ayarının yalnızca bir alt kümesi desteklenmektedir.
 Ayrıştırılıyor Yapıştır Yapıştırılan kayıtlar, mevcut %s kayıtlarının üzerine yazılacak Yapıştırılan kayıtlar %s kayıtlarının üzerine yazılacak Yapıştırılan kayıtlar, %s kaydının üzerine yazılacak Yapıştırılan kayıt, %s kaydının üzerine yazacak Lütfen yeni sürümü yüklemeden önce CHIRP'den çıktığınızdan emin olun! Lütfen bu adımları dikkatlice izleyin:
1 - Telsizinizi açın
2 - Arayüz kablosunu telsizinize bağlayın
3 - İndirmeyi başlatmak için bu penceredeki tuşa tıklayın (başka bir iletişim kutusu görebilirsiniz, tamam'a tıklayın)
4 - Telsiz bip sesi çıkaracak ve led yanıp sönecektir
5 - Veri yükleme başlamadan önce "MON" tuşuna basmanız için 10 saniyelik bir zaman aşımı süresi alacaksınız.
6 - Her şey yolunda giderse sonunda telsiz bip sesi çıkaracaktır.
Klonladıktan sonra kabloyu çıkarın ve normal moda geçmek için telsizinizi kapatıp açın.
 Lütfen bu adımları dikkatlice izleyin:
1 - Telsizinizi açın
2 - Arayüz kablosunu telsizinize bağlayın.
3 - İndirmeyi başlatmak için bu penceredeki tuşa tıklayın
     (Telsiz bip sesi çıkaracak ve led yanıp sönecektir)
4 - Ardından klonlamaya başlamak için telsizinizdeki "A" tuşuna basın.
     (Sonunda telsiz bip sesi çıkaracaktır)
 Lütfen geliştirici modunun CHIRP projesinin geliştiricileri tarafından veya bir geliştiricinin yönetimi altında kullanılması için tasarlandığını unutmayın. ÇOK DİKKATLİ kullanılmadığı takdirde bilgisayarınıza ve telsizinize zarar verebilecek davranış ve işlevleri mümkün kılar. Uyarıldın! Yine de devam edilsin mi? Lütfen bekleyin Kablonuzu takın ve ardından Tamam'a tıklayın Port Güç Bu kayda almak için enter tuşuna basın Baskı Önizleme Baskı Özellikler %s sorgusu Kaynaktan Sorgula RX DTCS Telsiz Telsiz %i bloğunu onaylamadı Telsiz bilgisi Telsiz, beklenen son bloktan sonra veri gönderdi, bu, seçilen model ABD dışındaysa gerçekleşir ama telsiz bir ABD telsizi. Lütfen doğru modeli seçin ve tekrar deneyin. RadioReference Kanada, sorgulayabilmeniz için önce oturum açmanız gerekir RadioReference.com dünyanın en büyük
telsiz iletişim veri sağlayıcısıdır
<small>Premium hesap gereklidir</small> Yeniden Başlatma Gerekli Yenilenmiş %s kaydı Sürücüyü Yeniden Yükle Sürücüyü ve Dosyayı Yeniden Yükle Bankayı yeniden adlandır RepeaterBook, Amatör Telsiz için dünya çapında en kapsamlı 
ÜCRETSİZ röle rehberidir. Raporlama etkinleştirildi Raporlama, CHIRP projesinin sınırlı çabalarımızı hangi telsiz modellerine ve işletim sistemi platformlarına harcayacağını bilmesine yardımcı olur. Etkin bırakırsanız gerçekten minnettar oluruz. Raporlama gerçekten devre dışı bırakılsın mı? Yeniden Başlatma Gerekiyor %i sekmeyi geri yükle %i sekmeyi geri yükle Alınan ayarlar Kaydet Kapanmadan önce kaydedilsin mi? Dosyayı kaydet Kaydedilmiş ayarlar Tarama listeleri Karıştırıcı Güvenlik Riski Bant Planı Seç... Bantları Seç Modları Seç Bir bant planı seçin Servis Ayarlar Ham Kaydı Göster Hata ayıklama günlüğü konumunu göster Ekstra alanları göster Bazı kayıtlar bu telsizle uyumlu değil Bazı anılar silinemez %i kaydı sırala %i kaydı sırala %i kaydı artan şekilde sırala %i kaydı artan şekilde sırala %i kaydı azalan şekilde sırala %i kaydı azalan şekilde sırala Sütuna göre sırala: Kayıtları sırala Sıralama Devlet Eyalet/İl Başarılı DMR-MARC Dünya Çapında iletişim Ağı X3Plus sürücüsü şu anda deneyseldir.
Bilinen bir sorun yok, ancak dikkatli bir şekilde ilerlemelisiniz.
Lütfen ilk başarılı indirmenizin düzenlenmemiş bir kopyasını
bir CHIRP Radio Images (*.img) dosyasına kaydedin.
 Bu modülün yazarı tanınmış bir CHIRP geliştiricisi değildir. Güvenlik riski oluşturabileceğinden bu modülü yüklememeniz önerilir. Yine de devam edilsin mi? Kayıtları içe aktarmak için önerilen prosedür, kaynak dosyayı açmak ve kayıtları bu dosyadan hedef görüntünüze kopyalamak/yapıştırmaktır. Bu içe aktarma işlevine devam ederseniz CHIRP, şu anda açık olan dosyanızdaki tüm kayıtları %(file)s içindekilerle değiştirecektir. Kayıtları kopyalamak/yapıştırmak için bu dosyayı açmak mı yoksa içe aktarma işlemine devam etmek mi istiyorsunuz? Bu kaydı Bu sürücü, ID-5100'ün v3'ü ile test edilmiştir. Telsiziniz tam olarak güncellenmemişse, lütfen hata ayıklama günlüğü içeren bir hata raporu açarak bize yardım edin, böylece diğer revizyonlar için destek verebilelim. Bu sürücü geliştirme aşamasındadır ve deneysel olarak değerlendirilmelidir. Bu erken aşama beta sürücüsüdür
 Bu, erken aşamadaki bir beta sürücüsüdür - yükleme riski size aittir
 Bu Quansheng UV-K5 için deneysel bir sürücüdür. Telsizinize zarar verebilir veya daha kötüsü olabilir. Riski size ait olmak üzere kullanın.

Herhangi bir değişiklik yapmadan önce lütfen hafıza imajını telsizden chirp ile indirin ve saklayın. (Yedekleme yapın) Bu daha sonra orijinal ayarları kurtarmak için kullanılabilir.

bazı ayrıntılar henüz uygulanmadı Bu, halen geliştirilmekte olan BJ-9900 için deneysel destektir.
Lütfen OEM yazılımıyla iyi bir yedeklemeniz olduğundan emin olun.
Ayrıca lütfen hata raporu ve geliştirme istekleri gönderin!
Uyarıldınız. Kendi sorumluluğunuzda ilerleyin! Bu kaydı ve hepsini yukarı kaydır Bu kaydı ve bloğu yukarı kaydır Bu telsizde program moduna girmenin zor bir yolu var,
orijinal yazılım ile bile girmek için birkaç deneme yapmak gerekir.
8 kez deneyeceğim (çoğu zaman ~3 de işe yarar) ve bu
birkaç saniye sürebilir, işe yaramazsa birkaç kez tekrar deneyin.
İşe yaramazsa, lütfen telsizi ve kabloyu kontrol edin.
 Bu yalnızca daha geniş frekans kapsamını destekleyen değiştirilmiş ürün yazılımı kullanıyorsanız etkinleştirilmelidir. Bunun etkinleştirilmesi, CHIRP'in OEM kısıtlamalarını uygulamamasına neden olur ve tanımlanmamış veya düzenlenmemiş davranışlara yol açabilir. Riski size ait olmak üzere kullanın! Bu, bir web sitesi kaydından bir modül yükleyecektir Ton Ton Modu Ton Susturucu Ayarlama Adımı USB Portu Bulucu Kablonuz için bağlantı noktası belirlenemiyor. Sürücülerinizi ve bağlantılarınızı kontrol edin. Radyo yüklenmeden önce kayıt düzenlenemiyor %r stok yapılandırması bulunamadı Pano açılamıyor Sorgulanamıyor Son blok okunamıyor. Bu genellikle seçilen model ABD olduğunda olur ama telsiz ABD dışı bir telsizdir (veya geniş bantlıdır). Lütfen doğru modeli seçin ve tekrar deneyin. %s bu sistemde gösterilemiyor Bu kayıtta %s ayarlanamıyor Amerika Birleşik Devletleri (gerekirse) Kablonuzu çıkarın ve ardından Tamam'ı tıklayın Yükleme talimatları Telsize yükle Telsize yükle... Yüklenen kayıt %s Sabit genişlikte yazı tipi kullan Daha büyük yazı tipi kullan Değer %i bit'e sığmıyor Değer en az %.4f olmalıdır Değer en fazla %.4f olmalıdır Değer tam olarak %i ondalık basamak olmalıdır Değer sıfır veya daha büyük olmalıdır Değerler Tedarikçi Göster UYARI! Uyarı Uyarı: %s Hoşgeldiniz CHIRP'in sizin için bir masaüstü simgesi yüklemesini ister misiniz? Kablonuz şu bağlantı noktasında görünüyor: bitler bytes her biri bayt devre dışı bırakıldı etkinleştirildi {bank} dolu 